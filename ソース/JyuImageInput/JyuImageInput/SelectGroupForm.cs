﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class SelectGroupForm : Form
    {
        public SelectGroupForm()
        {
            InitializeComponent();

            listChange();

            dataGridView1.Columns[0].Width = 60;
            dataGridView1.Columns[1].Width = 60;
            dataGridView1.Columns[2].Width = 70;
            dataGridView1.Columns[3].Width = 70;
            dataGridView1.Columns[4].Width = 70;

            dataGridView1.Columns[0].HeaderText = "審査月";
            dataGridView1.Columns[1].HeaderText = "スキャンID";
            dataGridView1.Columns[2].HeaderText = "グループID";
            dataGridView1.Columns[3].HeaderText = "画像数";
            dataGridView1.Columns[4].HeaderText = "入力済数";

            dataGridView1.Columns[5].Visible = false;
            dataGridView1.Columns[6].Visible = false;
            dataGridView1.Columns[7].Visible = false;
            dataGridView1.Columns[8].Visible = false;
            dataGridView1.Columns[9].Visible = false;
            dataGridView1.Columns[10].Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell == null) return;
            int groupid = (int)dataGridView1[2, dataGridView1.CurrentRow.Index].Value;
            Settings.GroupID = groupid;
            Settings.GroupStartDate =(int)dataGridView1[4, dataGridView1.CurrentRow.Index].Value;

            using(var cmd =SQLClass.CreateCommand("SELECT rid FROM receipts WHERE gid = :gid"))
            {
                cmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = groupid;
                using (var dr = cmd.ExecuteReader())
                {
                    Settings.RIDList.Clear();

                    while (dr.Read())
                    {
                        Settings.RIDList.Add(dr.GetInt64(0));
                    }
                }
            }

            Settings.RIDList.Sort();
            Settings.RIDListIndex = 0;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listChange()
        {
            DataTable dt = new DataTable();
            string daStr = "SELECT scans.billmonth, groups.sid, groups.gid, groups.imagecount, groups.inputcount, groups.startdate, groups.enddate, groups.parts1, groups.parts2, groups.days1, groups.days2 " +
                "FROM groups, scans ";

            if (radioButton1.Checked)
            {
                //未入力分
                daStr += "WHERE scans.sid = groups.sid AND groups.enddate <= 0 ORDER BY gid";
            }
            else if (radioButton2.Checked)
            {
                //部位２未入力
                daStr += "WHERE scans.sid = groups.sid AND groups.enddate > 0 AND (groups.parts1 <> 0 AND groups.parts1 > groups.parts2) ORDER BY gid";
            }
            else if (radioButton3.Checked)
            {
                //開始日未入力
                daStr += "WHERE scans.sid = groups.sid AND groups.enddate > 0 AND (groups.parts1 <> 0 AND groups.parts1 > groups.days1) ORDER BY gid";
            }
            else if (radioButton4.Checked)
            {
                //開始日２未入力
                daStr += "WHERE scans.sid = groups.sid AND groups.enddate > 0 AND (groups.parts1 <> 0 AND groups.parts1 > groups.days2) ORDER BY gid";
            }
            else
            {
                //すべて
                daStr += "WHERE scans.sid = groups.sid ORDER BY gid";
            }


            using (var da = SQLClass.CreateDataAdapter(daStr))
            {
                da.Fill(dt);
            }

            dt.Columns.Add("開始日");
            dt.Columns.Add("終了日");
            dt.Columns.Add("部位１");
            dt.Columns.Add("部位２");
            dt.Columns.Add("日付１");
            dt.Columns.Add("日付２");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if ((int)dt.Rows[i][5] != 0)
                {
                    dt.Rows[i][11] = ((int)dt.Rows[i][5]).ToString("0000/00/00");
                }
                else
                {
                    dt.Rows[i][11] = "";
                }

                if ((int)dt.Rows[i][6] != 0)
                {
                    dt.Rows[i][12] = ((int)dt.Rows[i][6]).ToString("0000/00/00");
                }
                else
                {
                    dt.Rows[i][12] = "";
                }

                if ((int)dt.Rows[i][7] != 0)
                {
                    dt.Rows[i][13] = dt.Rows[i][7];
                }
                else
                {
                    dt.Rows[i][13] = "";
                }

                if ((int)dt.Rows[i][8] != 0)
                {
                    dt.Rows[i][14] = dt.Rows[i][8];
                }
                else
                {
                    dt.Rows[i][14] = "";
                }
                if ((int)dt.Rows[i][9] != 0)
                {
                    dt.Rows[i][15] = dt.Rows[i][9];
                }
                else
                {
                    dt.Rows[i][15] = "";
                }
                if ((int)dt.Rows[i][10] != 0)
                {
                    dt.Rows[i][16] = dt.Rows[i][10];
                }
                else
                {
                    dt.Rows[i][16] = "";
                }

            }

            dataGridView1.DataSource = dt;
            dataGridView1.Columns[11].Width = 70;
            dataGridView1.Columns[12].Width = 70;
            dataGridView1.Columns[13].Width = 70;
            dataGridView1.Columns[14].Width = 70;
            dataGridView1.Columns[15].Width = 70;
            dataGridView1.Columns[16].Width = 70;

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell == null)
            {
                label1.Text = "";
                label2.Text = "";
                return;
            }

            int cr = dataGridView1.CurrentRow.Index;

            int gid = (int)dataGridView1[2, cr].Value;

            //なぜかpostgresのMAX関数が遅すぎるので自力MAX ついでにカウント
            int inputDate = 0;
            using (var cmd = SQLClass.CreateCommand("SELECT inputdate FROM receipts WHERE gid = :gid"))
            {

                cmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["gid"].Value = gid;

                List<int> lst = new List<int>();
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read()) lst.Add(dr.GetInt32(0));
                }

                inputDate = lst.Count == 0 ? 0 : lst.Max();
                label2.Text = "現在入力数：　" + lst.Count(item => item > 0);
            }

            using (var cmd = SQLClass.CreateCommand("SELECT users.uname FROM receipts, users WHERE users.uid = receipts.uid AND receipts.gid = :gid AND inputdate = @inputdate"))
            {
                cmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add("inputdate", NpgsqlTypes.NpgsqlDbType.Integer);

                cmd.Parameters[0].Value = gid;
                cmd.Parameters["inputdate"].Value = inputDate;

                var userName = cmd.ExecuteScalar() ?? "";
                label1.Text = "最終入力者：　" + userName.ToString();
            }

            //using (var cmd = SQLClass.CreateCommand("SELECT COUNT(gid) FROM receipts WHERE gid = :gid AND inputdate > 0"))
            //{
            //    cmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);
            //    cmd.Parameters[0].Value = gid;

            //    var gcount = cmd.ExecuteScalar() ?? 0;
            //    label2.Text = "現在入力数：　" + gcount.ToString();
            //}
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            listChange();
        }
    }
}
