﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class PartsCheckForm : Form
    {
        public PartsCheckForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //チェック開始

            int year;
            int month;
            int.TryParse(textBox1.Text, out year);
            int.TryParse(textBox2.Text, out month);

            if (year < 1 || year > 99)
            {
                MessageBox.Show("年の指定に誤りがあります");
                return;
            }

            if (month < 0 || month > 12)
            {
                MessageBox.Show("月の指定に誤りがあります");
                return;
            }

            month = year * 100 + month;
            var lst = checkPartsData(month);

            errorListView.DataSource = lst;

            errorListView.Columns[0].HeaderText = "レセプトID";
            errorListView.Columns[1].HeaderText = "部位番号";
            errorListView.Columns[2].HeaderText = "1回目入力";
            errorListView.Columns[3].HeaderText = "2回目入力";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private List<PartsErrorData> checkPartsData(int targetMonth)
        {
            DataTable dt = new DataTable();
            List<PartsErrorData> partsErrorList = new List<PartsErrorData>();

            using (var cmd = SQLClass.CreateCommand("SELECT parts.rid, parts.name1, parts.name2, parts.name3, parts.name4, parts.name5, parts.name6, parts.name7, parts.name8, parts.name9, parts.name10, parts.s1, parts.s2, parts.e1, parts.e2 " +
                "FROM (parts INNER JOIN receipts ON parts.rid = receipts.rid) INNER JOIN groups ON receipts.gid = groups.gid WHERE groups.billmonth = :billmonth"))
            {
                cmd.Parameters.Add("billmonth", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["billmonth"].Value = targetMonth;

                using (var da = SQLClass.CreateDataAdapter(cmd))
                {
                    try
                    {
                        da.Fill(dt);
                    }
                    catch(Exception e)
                    {
                        MessageBox.Show("データの取得に失敗しました\r\n" + e.Message);
                        return partsErrorList;
                    }
                }
            }


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var row = dt.Rows[i];
                if ((string)row["name1"] != (string)row["name6"]) partsErrorList.Add(new PartsErrorData((long)row["rid"], 1, (string)row["name1"], (string)row["name6"]));
                if ((string)row["name2"] != (string)row["name7"]) partsErrorList.Add(new PartsErrorData((long)row["rid"], 2, (string)row["name2"], (string)row["name7"]));
                if ((string)row["name3"] != (string)row["name8"]) partsErrorList.Add(new PartsErrorData((long)row["rid"], 3, (string)row["name3"], (string)row["name8"]));
                if ((string)row["name4"] != (string)row["name9"]) partsErrorList.Add(new PartsErrorData((long)row["rid"], 4, (string)row["name4"], (string)row["name9"]));
                if ((string)row["name5"] != (string)row["name10"]) partsErrorList.Add(new PartsErrorData((long)row["rid"], 5, (string)row["name5"], (string)row["name10"]));
                if ((int)row["s1"] != (int)row["s2"]) partsErrorList.Add(new PartsErrorData((long)row["rid"], 10, row["s1"].ToString(), row["s2"].ToString()));
                if ((int)row["e1"] != (int)row["e2"]) partsErrorList.Add(new PartsErrorData((long)row["rid"], 20, row["e1"].ToString(), row["e2"].ToString()));
            }

            partsErrorList.Sort((x, y) => x.rid == y.rid ? 0 : x.rid > y.rid ? 1 : -1);

            return partsErrorList;
        }


        public class PartsErrorData
        {
            public long rid { get; internal set; }
            public int order { get; internal set; }
            public string input1 { get; internal set; }
            public string input2 { get; internal set; }

            internal PartsErrorData(long rid, int order, string input1, string input2)
            {
                this.rid = rid;
                this.order = order;
                this.input1 = input1;
                this.input2 = input2;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var lst = ((List<PartsErrorData>)errorListView.DataSource);

            if (lst == null || lst.Count == 0)
            {
                MessageBox.Show("修正対象のデータがありません");
                return;
            }

            using (var f = new PartsRevisionForm(lst)) f.ShowDialog();
        }
    }
}
