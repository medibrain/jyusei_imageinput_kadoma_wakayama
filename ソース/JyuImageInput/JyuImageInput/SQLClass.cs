﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;

namespace JyuImageInput
{
    class SQLClass
    {
        static NpgsqlConnection con;

        static SQLClass()
        {
            Settings.load();
            conOpen();
        }

        ~SQLClass()
        {
            ConClose();
        }

        /// <summary>
        /// コマンドを作成します 接続がない場合NULLを返します
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>
        public static NpgsqlCommand CreateCommand(string commandText)
        {
            if(!ConState())return null;

            NpgsqlCommand cmd = new NpgsqlCommand(commandText,con);
            return cmd;
        }

        public static NpgsqlDataAdapter CreateDataAdapter(string commandText)
        {
            if (!ConState()) return null;

            NpgsqlDataAdapter da = new NpgsqlDataAdapter(commandText, con);
            return da;
        }

        public static NpgsqlDataAdapter CreateDataAdapter(NpgsqlCommand command)
        {
            if (!ConState()) return null;

            NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
            return da;
        }

        public static void conOpen()
        {
            string conStr = string.Format("Server={0};Port=5432;User Id={1};Password={2};Database=recescan;Encoding=UNICODE;Preload Reader=true;",
                Settings.DBName,
                Settings.DBuserID,
                Settings.DBuserPassword);
            con = new NpgsqlConnection(conStr);
            try
            {
                con.Open();
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        public static void ConClose()
        {
            con.Close();
        }

        public static bool ConState()
        {
            return (con.State == System.Data.ConnectionState.Open);
        }

        public static void settingSave()
        {
            string fileName = System.Windows.Forms.Application.StartupPath + "setting.txt";

        }

        /// <summary>
        /// トランザクションを開始します
        /// </summary>
        /// <returns></returns>
        public NpgsqlTransaction CreateTran()
        {
            return con.BeginTransaction();
        }
    }
}
