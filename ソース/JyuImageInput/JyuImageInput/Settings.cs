﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing;
using System.Runtime.InteropServices;


namespace JyuImageInput
{
    static class Settings
    {

        public static string DBuserID { get; set; }
        public static string DBuserPassword { get; set; }
        public static string DBName { get; set; }
        public static int numberLength { get; set; }


        /// <summary>
        /// レセプトチェック時のエラーコード
        /// </summary>
        public enum ERROR_CODE
        {
            エラーなし = 0,
            未入力 = 1, 未入力無視 = 2, 未入力保留 = 3,
            同一複数データ = 11, 同一複数データ無視 = 12, 同一複数データ保留 = 13,
            先頭続紙 = 21, 先頭続紙無視 = 22, 先頭続紙保留 = 23,
            エラー指定 = 31, エラー無視 = 32, エラー保留 = 33,
            一致データなし = 41, 一致データなし無視 = 42, 一致データなし保留 = 43,
            画像重複 = 51, 画像重複無視 = 52, 画像重複保留 = 53,
            エラーレセ続紙 = 61
        }

        public static SortedList<int, string> errorMessage = new SortedList<int,string>
        {
            {0,"エラーなし"},
            {1,"未入力 - エラーの扱いが指定されていません"},
            {2,"未入力 - 無視に指定されています"},
            {3,"未入力 - 保留に指定されています"},
            {11,"同一複数データ - エラーの扱いが指定されていません"},
            {12,"同一複数データ - 無視に指定されています"},
            {13,"同一複数データ - 保留に指定されています"},
            {21,"グループの先頭にある続紙等 - エラーの扱いが指定されていません"},
            {22,"グループの先頭にある続紙等 - 無視に指定されています"},
            {23,"グループの先頭にある続紙等 - 保留に指定されています"},
            {31,"入力時エラー指定画像 - エラーの扱いが指定されていません"},
            {32,"入力時エラー指定画像 - 無視に指定されています"},
            {33,"入力時エラー指定画像 - 保留に指定されています"},
            {41,"一致データなし - エラーの扱いが指定されていません"},
            {42,"一致データなし - 無視に指定されています"},
            {43,"一致データなし - 保留に指定されています"},
            {51,"画像データ重複 - エラーの扱いが指定されていません"},
            {52,"画像データ重複 - 無視に指定されています"},
            {53,"画像データ重複 - 保留に指定されています"},
        };
                        

        /// <summary>
        /// scanデータが格納されている親フォルダ
        /// </summary>
        public static string imagePath { get; set; }

        /// <summary>
        /// 現在の入力者名
        /// </summary>
        public static string InputUserName { get; set; }

        /// <summary>
        /// 現在の入力者ID
        /// </summary>
        public static int InputUserID { get; set; }

        /// <summary>
        /// 現在の入力グループID
        /// </summary>
        public static int GroupID { get; set; }

        /// <summary>
        /// 現在入力中グループ内レセプトID一覧
        /// </summary>
        public static List<long> RIDList = new List<long>();

        /// <summary>
        /// 現在入力中レセプトのリスト内インデックス
        /// </summary>
        public static int RIDListIndex { get; set; }

        /// <summary>
        /// 現在入力中グループの審査月
        /// </summary>
        public static int GroupBillMonth { get; set; }

        /// <summary>
        /// 現在入力中のレセプト画像ID
        /// </summary>
        public static long ReceiptID { get; set; }

        /// <summary>
        /// グループの入力開始日
        /// </summary>
        public static int GroupStartDate { get; set; }

        /// <summary>
        /// グループ内入力済み枚数
        /// </summary>
        public static int GroupInputCount { get; set; }

        /// <summary>
        /// 起動してからの入力枚数
        /// </summary>
        public static int TodayInputCount { get; set; }


        //20190624211543 furukawa st ////////////////////////
        //gifフラグ、gifサイズ追加
        
        public static bool flgConvGif { get; set; }

        public static int intGifConvSize { get; set; }
        //20190624211543 furukawa ed ////////////////////////


        static Settings()
        {
            load();
            InputUserName = null;
            InputUserID = -1;
        }

        /// <summary>
        /// セーブするためのクラス
        /// </summary>
        [Serializable]
        class SaveData
        {
            internal string saveUserID;
            internal string saveUserPassword;
            internal string saveDatagbaseName;
            internal string saveimagePath;
            internal int numberLength;

            //20190624212403 furukawa st ////////////////////////
            //gifフラグ、gifサイズ追加
            
            internal bool saveflgConvGif;
            internal int saveGifConvSize;
            //20190624212403 furukawa ed ////////////////////////

        }

        /// <summary>
        /// 設定をセーブします
        /// </summary>
        public static void save()
        {
            string fileName = System.Windows.Forms.Application.StartupPath + "\\setting.dat";
            SaveData sd = new SaveData();

            //デフォルト設定
            //sd.saveDatagbaseName = "localhost";
            //sd.saveUserID = "postgres";
            //sd.saveUserPassword = "pass";
            //sd.saveimagePath = "C:\\scan";
            //sd.numberLength = 6;
            
            sd.saveDatagbaseName = DBName;
            sd.saveUserID = DBuserID;
            sd.saveUserPassword = DBuserPassword;
            sd.saveimagePath = imagePath;
            sd.numberLength = numberLength;

            //20190624212454 furukawa st ////////////////////////
            //gifフラグ、gifサイズ追加
            
            sd.saveflgConvGif = flgConvGif;
            sd.saveGifConvSize = intGifConvSize;
            //20190624212454 furukawa ed ////////////////////////

            using (FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, sd);
            }
        }

        /// <summary>
        /// 設定をロードします
        /// </summary>
        /// <returns></returns>
        public static bool load()
        {
            SaveData sd;

            try
            {
                string fileName = System.Windows.Forms.Application.StartupPath + "\\setting.dat";
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    sd = (SaveData)bf.Deserialize(fs);
                }
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }

            DBName = sd.saveDatagbaseName;
            DBuserID = sd.saveUserID;
            DBuserPassword = sd.saveUserPassword;
            imagePath = sd.saveimagePath;
            numberLength = sd.numberLength;

            //20190624212549 furukawa st ////////////////////////
            //gifフラグ、gifサイズ追加
            
            flgConvGif = sd.saveflgConvGif;
            intGifConvSize = sd.saveGifConvSize;
            //20190624212549 furukawa ed ////////////////////////


            return true;

        }

        /// <summary>
        /// 画像処理を施し、gifに変換して保存されます
        /// </summary>
        /// <param name="imgName">変換元ファイルパス</param>
        /// <param name="saveName">変換後gifファイル名</param>
        public static bool ImageChange(string imgName, string saveName)
        {

#if NOT_GIFCONVERT  // GIF変換をやめる
            //↑やめると書いてあるのに、やるルーチンに行っている

            try
            {
                // 一旦、変換後のファイルを削除
                if (File.Exists(saveName)) File.Delete(saveName);

                //ファイルのサイズを取得
                var filesize = new System.IO.FileInfo(imgName).Length;             // ファイルサイズ（バイト）


                //和歌山広域は圧縮しない、門真は圧縮必要
                //設定ファイルにしておいたほうがあとあと楽できそう

                //20190624220544 furukawa フラグでGIF圧縮制御                
                if (!flgConvGif)                
                {

                    //20190624220256 furukawa st ////////////////////////
                    //復帰

                            //20190530152522 furukawa st ////////////////////////
                            //スキャンサイズが大きくなり圧縮処理が重いので、圧縮しない                
                            File.Copy(imgName, saveName);
                            return true;
                            //20190530152522 furukawa ed ////////////////////////

                    //20190624220256 furukawa ed ////////////////////////


                }
                else
                {
                    //20190624220341 furukawa st ////////////////////////
                    //圧縮サイズを設定から取得
                    
                    if (filesize < intGifConvSize * 1000)

                            //20190530134429 furukawa st ////////////////////////
                            //スキャンサイズが大きくなり圧縮処理が重いので、圧縮基準サイズ変更                                                                                   


                            //20190624和歌山広域再調整

                            //if (filesize < 500000)
                            //if (filesize < 900000)
                            //if (filesize < 200000)
                            //20190530134429 furukawa ed ////////////////////////
                    //20190624220341 furukawa ed ////////////////////////

                    {


                        // 設定KB未満 → 画像変換しない。ファイルコピーするだけ。
                        File.Copy(imgName, saveName);
                    }
                    else
                    {
                        // 設定KB以上 → GIF圧縮する。
                        using (var img = Image.FromFile(imgName)) img.Save(saveName, System.Drawing.Imaging.ImageFormat.Gif);
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            
#else
            using (Bitmap bmp = (Bitmap)Bitmap.FromFile(imgName, false))
            {
                Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
                var bitData = bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, bmp.PixelFormat);

                // 画像のメモリ上の開始位置
                IntPtr bmpPtr = bitData.Scan0;

                // 取得した画像データを代入する配列を確保
                byte[] scans = new byte[bmp.Height * bmp.Width];

                Marshal.Copy(bmpPtr, scans, 0, scans.Length);

                for (int i = 0; i < scans.Length; i++)
                {
                    if (scans[i] > 220) scans[i] = 255;
                }

                //元に戻す
                Marshal.Copy(scans, 0, bmpPtr, scans.Length);
                bmp.UnlockBits(bitData);
                string imgPath = System.IO.Path.GetDirectoryName(imgName);
                string originalName = System.IO.Path.GetFileNameWithoutExtension(imgName);

                bool resBool = true;

                //セーブ
                try
                {
                    bmp.Save(saveName, System.Drawing.Imaging.ImageFormat.Gif);
                }
                catch
                {
                    resBool = false;
                }
                return resBool;
            }
#endif
        }


        /// <summary>
        /// 明るさの上限のみをを調整します
        /// </summary>
        /// <param name="imgName">ビットマップデータ</param>
        /// <param name="saveName">上限値　これ以上はすべて白に変換されます</param>
        public static void ImageBrightness(Bitmap bmp,int maxBright)
        {
            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            var bitData = bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, bmp.PixelFormat);

            // 画像のメモリ上の開始位置
            IntPtr bmpPtr = bitData.Scan0;

            // 取得した画像データを代入する配列を確保
            byte[] scans = new byte[bmp.Height * bmp.Width];

            Marshal.Copy(bmpPtr, scans, 0, scans.Length);
            System.Threading.Tasks.Parallel.For(0,scans.Length, i =>
                {
                    if (scans[i] > maxBright) scans[i] = 255;
                });

            //元に戻す
            Marshal.Copy(scans, 0, bmpPtr, scans.Length);
            bmp.UnlockBits(bitData);
        }


    }
}
