﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JyuImageInput
{
    static class DateTimeEx
    {
        /// <summary>
        /// 年号番号なし年月和暦4桁(平成固定）を西暦年月に変換します
        /// </summary>
        /// <param name="jyyMM"></param>
        /// <returns>西暦yyyyMM</returns>
        public static int YearMonthJtoAD_H(int jyyMM)
        {
            if (jyyMM == 0) return 0;

            //20190505211549 furukawa st ////////////////////////
            //令和対応
            //3105以上　1912以下は令和

            //20200412081327 furukawa st ////////////////////////
            //令和年月の和暦表現が3桁(112、201等）になっていることで、頭2桁の比較が無理(令和2年は2xxになって条件ではじかれる）になったので4桁表現にした
            
            if (jyyMM>=3105 || (int.Parse(jyyMM.ToString("0000").Substring(0, 2)) <20))
            //if (jyyMM >= 3105 || (int.Parse(jyyMM.ToString().Substring(0, 2)) < 20))
            //20200412081327 furukawa ed ////////////////////////
            {
                return jyyMM + 201800;
            }
            else if (jyyMM<3105)
            {//3105未満は平成
                return jyyMM + 198800;
            }
            else
            {
                return jyyMM + 198800;
            }
            //return jyyMM + 198800;
            //20190505211549 furukawa ed ////////////////////////
        }

        /// <summary>
        /// 西暦年月を年号番号なし年月和暦4桁(平成固定）に変換します
        /// </summary>
        /// <param name="adyyyyMM">西暦yyyymm</param>
        /// <returns>eemm</returns>
        public static int YearMonthADtoJ_H(int adyyyyMM)
        {
            //20190505212853 furukawa st ////////////////////////
            //令和対応

            if (adyyyyMM >= 201905)
            {
                return adyyyyMM - 201800;
            }
            else
            {
                return adyyyyMM - 198800;
            }
            //return adyyyyMM - 198800;

            //20190505212853 furukawa ed ////////////////////////            
        }


        /// <summary>
        /// 現在の日時をint型8桁で返します
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static int NowDateInt()
        {
            int nowDateInt = DateTime.Now.Year * 10000 + DateTime.Now.Month * 100 + DateTime.Now.Day;
            return nowDateInt;
        }


        /// <summary>
        /// DateTime型の日付をint型8桁に変換します
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int DateTimeToInt(DateTime dt)
        {
            int nowDateInt = dt.Year * 10000 + dt.Month * 100 + dt.Day;
            return nowDateInt;
        }

        /// <summary>
        /// int型8桁の日付をDateTime型に変換します
        /// </summary>
        /// <param name="intDate"></param>
        /// <returns></returns>
        public static DateTime IntToDateTime(int intDate)
        {
            int year = intDate / 10000;
            int month = (intDate % 10000) / 100;
            int day = intDate % 100;
            DateTime dt = new DateTime(year, month, day);
            return dt;
        }
    }
}
