﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class VersionForm : Form
    {
        public VersionForm()
        {
            InitializeComponent();
            label3.Text = "Version: " + Application.ProductVersion;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
