﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class OutputForm : Form
    {
        public OutputForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            selectScans();
        }

        private void selectScans()
        {
            int byear;
            int bmonth;

            if (!int.TryParse(textBox1.Text, out byear) || !int.TryParse(textBox2.Text, out bmonth) || bmonth > 12)
            {
                MessageBox.Show("審査月の指定に誤りがあります。");
                return;
            }

            int billmonth = byear * 100 + bmonth;
            DataTable dt = new DataTable();
            dataGridView1.DataSource = null;

            using (var cmd = SQLClass.CreateCommand("SELECT sid, billmonth, scancount, checkdate, enddate, outdate FROM scans WHERE billmonth = :billmonth"))
            {
                cmd.Parameters.Add("billmonth", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = billmonth;
                using(var da =SQLClass.CreateDataAdapter(cmd))
                {
                    da.Fill(dt);
                }
            }

            dt.Columns.Add("outTarget", typeof(bool));

            dataGridView1.DataSource = dt;

            dataGridView1.Columns[0].HeaderText = "ID";
            dataGridView1.Columns[1].HeaderText = "請求月";
            dataGridView1.Columns[2].HeaderText = "枚数";
            dataGridView1.Columns[3].HeaderText = "チェック";
            dataGridView1.Columns[4].HeaderText = "処理終了";
            dataGridView1.Columns[5].HeaderText = "出力日";
            dataGridView1.Columns[6].HeaderText = "出力";

            DataGridViewCellStyle dcs = new DataGridViewCellStyle();
            dcs.Format = "00/00/00";

            dataGridView1.Columns[3].DefaultCellStyle = dcs;
            dataGridView1.Columns[4].DefaultCellStyle = dcs;
            dataGridView1.Columns[5].DefaultCellStyle = dcs;

            dataGridView1.Columns[0].Width = 50;
            dataGridView1.Columns[1].Width = 70;
            dataGridView1.Columns[2].Width = 50;
            dataGridView1.Columns[3].Width = 70;
            dataGridView1.Columns[4].Width = 70;
            dataGridView1.Columns[5].Width = 70;
            dataGridView1.Columns[6].Width = 50;

            dataGridView1.Columns[0].ReadOnly = true;
            dataGridView1.Columns[1].ReadOnly = true;
            dataGridView1.Columns[2].ReadOnly = true;
            dataGridView1.Columns[3].ReadOnly = true;
            dataGridView1.Columns[4].ReadOnly = true;
            dataGridView1.Columns[5].ReadOnly = true;


            DataGridViewCellStyle dcs0 = new DataGridViewCellStyle();
            dcs0.Format = "0";

            //未出力すべてを出力対象に
            //日付が０を表示工夫
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if ((int)dt.Rows[i][3] == 0) dataGridView1[3, i].Style = dcs0;
                if ((int)dt.Rows[i][4] == 0) dataGridView1[4, i].Style = dcs0;
                if ((int)dt.Rows[i][5] == 0) dataGridView1[5, i].Style = dcs0;
                if ((int)dt.Rows[i][5] > 0) dt.Rows[i][6] = false;
                else dt.Rows[i][6] = true;
            }

        }

        private void selectReceipts(int scanid)
        {
            DataTable dt = new DataTable();
            dataGridView2.DataSource = null;

            using (var cmd = SQLClass.CreateCommand("SELECT receipts.rid, receipts.medimonth, receipts.insuredno, receipts.totalpoint, groups.billmonth, receipts.outdata "+
                "FROM receipts, groups WHERE receipts.gid = groups.gid AND groups.sid =:sid ORDER BY receipts.rid"))
            {
                cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = scanid;

                using (var da = SQLClass.CreateDataAdapter(cmd))
                {
                    da.Fill(dt);
                }
            }

            dataGridView2.DataSource = dt;
            int count = dt.Rows.Count;
            label3.Text = "件数：  " + count.ToString();

            dataGridView2.Columns[0].HeaderText = "レセプトID";
            dataGridView2.Columns[1].HeaderText = "診療年月";
            dataGridView2.Columns[2].HeaderText = "被保番";
            dataGridView2.Columns[3].HeaderText = "合計額";
            dataGridView2.Columns[4].HeaderText = "請求月";
            dataGridView2.Columns[5].HeaderText = "電算管理番号";

            dataGridView2.Columns[0].Width = 100;
            dataGridView2.Columns[1].Width = 70;
            dataGridView2.Columns[2].Width = 70;
            dataGridView2.Columns[3].Width = 70;
            dataGridView2.Columns[4].Width = 70;
            dataGridView2.Columns[5].Width = 140;
        }

        private void btnOutPut_Click(object sender, EventArgs e)
        {
            List<int> sids = new List<int>();
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (!(bool)dataGridView1[6, i].Value) continue;
                if ((int)dataGridView1[4, i].Value == 0)
                {
                    MessageBox.Show("処理が終了していないものが出力対象になっています。選択しなおしてください。",
                        "出力エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                sids.Add((int)dataGridView1[0, i].Value);
            }

            if (sids.Count == 0)
            {
                MessageBox.Show("出力対象がありません");
                return;
            }

            string sidsStr = sids[0].ToString();
            for (int i = 1; i < sids.Count; i++)
            {
                sidsStr = sidsStr + ", " + sids[i].ToString();
            }

            //重複チェック
            checkBox1.Checked = false;
            if (!overTest(sids)) return;


            DataTable dt = new DataTable();
            using (var cmd = SQLClass.CreateCommand("SELECT receipts.rid, receipts.outdata FROM receipts, groups " +
                "WHERE receipts.gid = groups.gid AND receipts.errorcode = 0 AND groups.sid IN (" + sidsStr + ") ORDER BY receipts.rid"))
            using (var da = SQLClass.CreateDataAdapter(cmd))
            {
                da.Fill(dt);
            }

            bool newTypeCSV = true;

            var res = MessageBox.Show("出力対象レセプト[" + dt.Rows.Count.ToString() + "件]を、新データ形式で出力します。よろしいですか？",
                "出力確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res != System.Windows.Forms.DialogResult.Yes)
            {
                res = MessageBox.Show("出力対象レセプト[" + dt.Rows.Count.ToString() + "件]を、旧データ形式で出力しますか？",
                    "出力確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res != System.Windows.Forms.DialogResult.Yes)
                {
                    MessageBox.Show("出力を中止します。");
                    return;
                }
                else
                {
                    newTypeCSV = false;
                }
            }


            string path = string.Empty;
            using (var f = new FolderBrowserDialog())
            {
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
                path = f.SelectedPath;
            }

            List<OutData> outDatas = new List<OutData>();

            string outRid;
            long rid;
            // OutData od = null;
            int errorcount = 0;
            //続紙ではないサーチID
            //本紙がエラーではじかれているにもかかわらず、続紙が正規版として残っている時のチェック用
            string nowOutRid = "";

            using (var cmd = SQLClass.CreateCommand("SELECT id, sex, birth, insuredno, hoscode, " +
                "medimonth, billmonth, typecode, totalcost, statecode FROM tdata WHERE id = :id"))
            {
                cmd.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Text);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    outRid = (string)dt.Rows[i][1];
                    rid = (long)dt.Rows[i][0];
                    if (outRid.Contains('-'))
                    {
                        //続紙等の場合

                        //直前のidと一致しない場合　スルー
                        string checkSerchRid = outRid.Remove(outRid.IndexOf('-'));
                        if (nowOutRid != checkSerchRid)
                        {
                            errorcount++;
                            continue;
                        }

                        // 続紙の場合、直前のレセプトの枚数を増やす
                        if (outDatas.Count > 0) outDatas.Last().imageCount++;
                        //od.imageCount++;
                        OutData subOD = new OutData();
                        subOD.subReceipt = true;
                        subOD.rid = rid;
                        subOD.id = outRid;
                        outDatas.Add(subOD);
                    }
                    else
                    {
                        cmd.Parameters[0].Value = outRid;
                        nowOutRid = outRid;
                        using (var dr = cmd.ExecuteReader())
                        {
                            //新しいレセプトの場合、今のレセプトを登録
                            //if (od != null) outDatas.Add(od);
                            //od = null;

                            //新しいレセプトの情報を取得
                            if (!dr.Read()) continue;
                            object[] obj = new object[10];
                            dr.GetValues(obj);

                            //新しいレコードを作成
                            var od = new OutData();
                            outDatas.Add(od);

                            //
                            od.rid = rid;

                            //id, sex, birth, insuredno, hoscode, medimonth, billmonth, typecode, totalcost, statecodde
                            od.id = (string)obj[0];
                            od.sex = (int)obj[1];
                            od.birth = (int)obj[2];
                            od.insuredno = (string)obj[3];
                            od.hoscode = (long)obj[4];
                            od.medimonth = (int)obj[5];
                            od.billmonth = (int)obj[6];
                            od.typecode = (int)obj[7];
                            od.totalcost = (int)obj[8];
                            od.statecode = (int)obj[9];
                            od.imageCount = 1;
                        }
                    }
                }
            }

            outDatas.Sort((x, y) => x.id.CompareTo(y.id));
            int cc = outDatas.Count(item => item.imageCount > 1);

            //1枚目セーブフォルダ作成、指定
            string diskid = DateTime.Now.ToString("yyyyMMddhhmmss");
            string saveDir = path + "\\" + diskid + "-Disk1";
            System.IO.Directory.CreateDirectory(saveDir);
            System.IO.Directory.CreateDirectory(saveDir + "\\image");
            string imageFolder = saveDir + "\\image\\";

            int discCount = 1;


            StringBuilder sb = new StringBuilder();

            //csvの作成
            if (newTypeCSV)
            {
                foreach (var item in outDatas)
                {
                    //続紙等の場合、記録しない
                    if (item.subReceipt) continue;

                    sb.Append(item.id + ",");
                    sb.Append(item.id + ".gif,");
                    sb.Append(item.imageCount + "\r\n");
                }
            }
            else
            {
                foreach (var item in outDatas)
                {
                    //続紙等の場合、記録しない
                    if (item.subReceipt) continue;

                    sb.Append(item.id + ",");
                    sb.Append(item.id + ".gif,");
                    sb.Append(item.imageCount + ",");
                    sb.Append(item.sex + ",");
                    sb.Append(item.birth + ",");
                    sb.Append(item.insuredno + ",");
                    sb.Append(item.hoscode + ",");

                    //20190614142219 furukawa st ////////////////////////
                    //medimonth、billmonthは前ゼロつける
                    
                    sb.Append(item.medimonth.ToString("0000") + ",");
                    sb.Append(item.billmonth.ToString("0000") + ",");
                            //sb.Append(item.medimonth + ",");
                            //sb.Append(item.billmonth + ",");
                    //20190614142219 furukawa ed ////////////////////////

                    sb.Append(item.typecode + ",");
                    sb.Append(item.totalcost + ",");
                    sb.Append(item.statecode + "\r\n");
                }
            }


            string CsvText = sb.ToString();
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(saveDir + "\\receipt.csv", false, Encoding.GetEncoding("Shift_JIS")))
            {
                sw.Write(CsvText);
            }

            //画像ファイルのコピー

            //現在のバイト数
            long sumByte = 0;

            //画像ファイルが見つからないなどのエラーがある場合のリスト
            List<string> errorMessageList = new List<string>();

            for (int i = 0; i < outDatas.Count; i++)
            {
                if (sumByte > 4000000000)
                {
                    //4.0Gごとにあらたなディレクトリ(ディスク)を作る
                    discCount++;

                    string subSaveDir = path + "\\" + diskid + "-Disk" + discCount.ToString();
                    System.IO.Directory.CreateDirectory(subSaveDir);
                    System.IO.Directory.CreateDirectory(subSaveDir + "\\image");
                    imageFolder = subSaveDir + "\\image\\";

                    //合計バイト数を戻す
                    sumByte = 0;
                }

                long receiptID = outDatas[i].rid;

                //20190614134646 furukawa st ////////////////////////
                //billstrの桁数を変更（令和1年は前ゼロなくなるので）
                
                string billstr = (receiptID / 1000000).ToString();
                        //string billstr = (receiptID / 1000000).ToString("0000");
                //20190614134646 furukawa ed ////////////////////////


                string floderNo = ((receiptID / 10000) % 100).ToString("00");
                string ridStr = (receiptID % 1000000).ToString("000000");

                string imgstr = Settings.imagePath + "\\" + billstr + floderNo + "\\" + receiptID.ToString();

                //画像があるか確認 gif
                if (System.IO.File.Exists(imgstr + ".gif"))
                {
                    var fileInfo = new System.IO.FileInfo(imgstr + ".gif");
                    sumByte += fileInfo.Length;
                    imgstr += ".gif";
                    System.IO.File.Copy(imgstr, imageFolder + outDatas[i].id + ".gif", true);
                }
                else
                {
                    //ログに書き込み
                    string errorMessage = "画像が見つかりません。   内部レセプトID:[" + receiptID.ToString();
                    errorMessage += "]  電算管理番号:[" + outDatas[i].id.ToString() + "]";
                    errorMessageList.Add(errorMessage);
                }
            }

            //diskinfoの作成

            string targetMonth = textBox1.Text.PadLeft(2, '0') + textBox2.Text.PadLeft(2, '0');
            for (int i = 1; i <= discCount; i++)
            {
                StringBuilder infoSB = new StringBuilder();
                infoSB.Append("dataid:" + diskid + "\r\n");
                infoSB.Append("date:" + DateTime.Today.ToString("yyyyMMdd") + "\r\n");
                infoSB.Append("disccount:" + discCount.ToString() + "\r\n");
                infoSB.Append("discno:" + i.ToString() + "\r\n");
                infoSB.Append("targetmonth:" + targetMonth + "\r\n");

                string infoTextName = path;//System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                infoTextName += "\\" + diskid + "-Disk" + i.ToString() + "\\datainfo.txt";
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(infoTextName, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.Write(infoSB.ToString());
                }
            }

            //出力済みとしてデータベースへ登録
            using (var cmd = SQLClass.CreateCommand("UPDATE scans SET outdate = :outdate WHERE sid IN (" + sidsStr + ")"))
            {
                cmd.Parameters.Add("outdate", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = DateTimeEx.NowDateInt();
                cmd.ExecuteNonQuery();
            }

            //エラーメッセージがある場合、ログとして出す
            if (errorMessageList.Count > 0)
            {
                string errorLogFileName = path;
                errorLogFileName += "\\" + diskid + "-Disk1\\errorlog.txt";

                using (var sw = new System.IO.StreamWriter(errorLogFileName,false,Encoding.GetEncoding("Shift-JIS")))
                {
                    foreach (var item in errorMessageList)
                    {
                        sw.WriteLine(item);
                    }
                }
                MessageBox.Show("画像ファイルの処理でエラーが発生しています。Disk1フォルダの中のエラーログを確認してください。");
            }
            MessageBox.Show("出力が終わりました");
        }

        class OutData
        {
            public long rid;
            public string id;
            public int sex;
            public int birth;
            public string insuredno;
            public long hoscode;
            public int medimonth;
            public int billmonth;
            public int typecode;
            public int totalcost;
            public int statecode;
            public int imageCount;
            public bool subReceipt = false;
        }


        private void dataGridView1_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell == null) return;
            int sid = (int)dataGridView1[0, dataGridView1.CurrentRow.Index].Value;
            selectReceipts(sid);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<int> sids = new List<int>();
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if ((bool)dataGridView1[6, i].Value == false) continue;
                if ((int)dataGridView1[4, i].Value == 0)
                {
                    MessageBox.Show("処理が終了していないものがテスト対象になっています。選択しなおしてください。",
                        "出力エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                sids.Add((int)dataGridView1[0, i].Value);
            }

            if (overTest(sids))
            {
                MessageBox.Show("重複チェックが終わりました");
            }
        }

        private bool overTest(List<int> sids)
        {
            if (sids.Count == 0)
            {
                MessageBox.Show("出力対象がありません");
                return false;
            }

            string sidsStr = sids[0].ToString();
            for (int i = 1; i < sids.Count; i++)
            {
                sidsStr = sidsStr + ", " + sids[i].ToString();
            }

            //重複チェック
            DataTable dt = new DataTable();
            bool fullCheck = checkBox1.Checked;
            checkBox1.Checked = false;
            string targetErrorCode;

            if (fullCheck)
            {
                targetErrorCode = "(0, 1, 2, 51, 52, 53)";
            }
            else
            {
                targetErrorCode = "(0, 51)";
            }

            using (var cmd = SQLClass.CreateCommand("SELECT rid, outdata, errorcode FROM receipts "+
                "WHERE outdata IN (SELECT outdata FROM receipts, groups WHERE receipts.gid = groups.gid "+
                "AND sid IN (" + sidsStr + ") AND outdata <> '' AND outdata <> 'error' "+
                "AND errorcode IN " + targetErrorCode + 
                " GROUP BY outdata, billmonth HAVING count(rid) > 1) ORDER BY outdata"))
            using (var da = SQLClass.CreateDataAdapter(cmd))
            {
                da.Fill(dt);
            }

            if (dt.Rows.Count != 0)
            {
                MessageBox.Show("重複データが [ " + dt.Rows.Count.ToString() + " ] 件見つかりました。修正を行なってください。");

                //重複データをエラーリストへ
                DataCheckClass.ErrorList.Clear();
                StringBuilder oversb = new StringBuilder();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    long overRid = (long)dt.Rows[i][0];
                    if (i != 0) oversb.Append(", ");
                    oversb.Append(overRid.ToString());
                    DataCheckClass.ErrorList.Add(new DataCheckClass.ErrorData(overRid, Settings.ERROR_CODE.画像重複));
                }

                //SQLに反映
                string cmdStr;
                if (fullCheck)
                {
                    cmdStr = "UPDATE receipts SET errorcode = 51 WHERE rid IN (";
                }
                else
                {
                    cmdStr = "UPDATE receipts SET errorcode = 51 WHERE errorcode IN (0, 1, 2) AND rid IN (";
                }
                using (var cmd = SQLClass.CreateCommand(cmdStr + oversb.ToString() + ")"))
                {
                    int ci = cmd.ExecuteNonQuery();
                }

                using (ImageCheckForm f = new ImageCheckForm())
                {
                    f.ShowDialog();
                }

                return false;
            }

            //複数データで対象がない場合チェック
            DataTable dt2 = new DataTable();
            using (var cmd = SQLClass.CreateCommand("SELECT outdata, rid, errorcode "+
                "FROM receipts WHERE outdata IN (SELECT outdata FROM receipts, groups "+
                "WHERE receipts.gid = groups.gid AND sid IN (" + sidsStr + ") "+
                "AND outdata <> '' AND outdata <> 'error' "+
                "GROUP BY outdata, billmonth HAVING count(rid) > 1) ORDER BY outdata"))
            using (var da = SQLClass.CreateDataAdapter(cmd))
            {
                da.Fill(dt2);
            }

            Dictionary<string, List<long>> checkdic = new Dictionary<string, List<long>>();
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                string outdata = (string)dt2.Rows[i][0];
                long rid = (long)dt2.Rows[i][1];
                int errorcode = (int)dt2.Rows[i][2];
                if (!checkdic.ContainsKey(outdata)) checkdic.Add(outdata, new List<long>());

                if (errorcode == 0)
                {
                    //エラーコードが0　対象データがある場合OK. RIDにゼロを追加
                    checkdic[outdata].Add(0);
                }
                else
                {
                    checkdic[outdata].Add(rid);
                }
            }

            //ひとつでもゼロがあれば、すべて消す
            foreach (var outdataItem in checkdic)
            {
                if (outdataItem.Value.Any(item => item == 0)) outdataItem.Value.Clear();
            }

            List<long> outList = new List<long>();
            foreach (var item in checkdic.Values)
            {
                outList.AddRange(item);
            }

            if (outList.Count > 1)
            {
                MessageBox.Show("重複画像において対象が一つも設定されていないデータがあります。");

                //エラーリストへ
                DataCheckClass.ErrorList.Clear();
                StringBuilder oversb = new StringBuilder();
                oversb.Clear();
                for (int i = 0; i < outList.Count; i++)
                {
                    if (i != 0) oversb.Append(", ");
                    oversb.Append(outList[i].ToString());
                    DataCheckClass.ErrorList.Add(new DataCheckClass.ErrorData(outList[i], Settings.ERROR_CODE.画像重複));
                }

                //SQLへ
                using (var cmd = SQLClass.CreateCommand("UPDATE receipts SET errorcode = 51 WHERE rid IN (" + oversb.ToString() + ")"))
                {
                    cmd.ExecuteNonQuery();
                }

                using (ImageCheckForm f = new ImageCheckForm())
                {
                    f.ShowDialog();
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// 出力対象外になった次のレセプトが続紙の場合をチェックします
        /// </summary>
        private void voidNextCheck()
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (OmissionCheckForm f = new OmissionCheckForm())
            {
                f.ShowDialog();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("未実装です");
        }

        private void checAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                dataGridView1[6, i].Value = checkAll.Checked;
            }
        }
    }
}
