﻿namespace JyuImageInput
{
    partial class ImageCheckForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAfterCheck = new System.Windows.Forms.Button();
            this.btnBeforeCheck = new System.Windows.Forms.Button();
            this.textBoxInsuredNo = new System.Windows.Forms.TextBox();
            this.textBoxMediMonth = new System.Windows.Forms.TextBox();
            this.textBoxTotalCost = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxGroupID = new System.Windows.Forms.TextBox();
            this.textBoxScanID = new System.Windows.Forms.TextBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button11 = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBoxOutdata = new System.Windows.Forms.TextBox();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.直前レセプトの続紙にするToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.直接入力ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAfterReze = new System.Windows.Forms.Button();
            this.btnBeforeReze = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAfter10 = new System.Windows.Forms.Button();
            this.btnBefore10 = new System.Windows.Forms.Button();
            this.textBoxReceiptID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.この電算管理番号を指定するToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(299, 175);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnAfterCheck
            // 
            this.btnAfterCheck.Location = new System.Drawing.Point(136, 274);
            this.btnAfterCheck.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAfterCheck.Name = "btnAfterCheck";
            this.btnAfterCheck.Size = new System.Drawing.Size(100, 29);
            this.btnAfterCheck.TabIndex = 3;
            this.btnAfterCheck.Text = "次チェック";
            this.btnAfterCheck.UseVisualStyleBackColor = true;
            this.btnAfterCheck.Click += new System.EventHandler(this.btnAfterCheck_Click);
            // 
            // btnBeforeCheck
            // 
            this.btnBeforeCheck.Location = new System.Drawing.Point(28, 274);
            this.btnBeforeCheck.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBeforeCheck.Name = "btnBeforeCheck";
            this.btnBeforeCheck.Size = new System.Drawing.Size(100, 29);
            this.btnBeforeCheck.TabIndex = 4;
            this.btnBeforeCheck.Text = "前チェック";
            this.btnBeforeCheck.UseVisualStyleBackColor = true;
            this.btnBeforeCheck.Click += new System.EventHandler(this.btnBeforeCheck_Click);
            // 
            // textBoxInsuredNo
            // 
            this.textBoxInsuredNo.Location = new System.Drawing.Point(105, 176);
            this.textBoxInsuredNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxInsuredNo.Name = "textBoxInsuredNo";
            this.textBoxInsuredNo.Size = new System.Drawing.Size(132, 22);
            this.textBoxInsuredNo.TabIndex = 1;
            this.textBoxInsuredNo.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            this.textBoxInsuredNo.Enter += new System.EventHandler(this.inputTextBox_Enter);
            this.textBoxInsuredNo.Validated += new System.EventHandler(this.textBoxInsuredNo_Validated);
            // 
            // textBoxMediMonth
            // 
            this.textBoxMediMonth.Location = new System.Drawing.Point(105, 124);
            this.textBoxMediMonth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxMediMonth.Name = "textBoxMediMonth";
            this.textBoxMediMonth.Size = new System.Drawing.Size(132, 22);
            this.textBoxMediMonth.TabIndex = 0;
            this.textBoxMediMonth.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            this.textBoxMediMonth.Enter += new System.EventHandler(this.inputTextBox_Enter);
            // 
            // textBoxTotalCost
            // 
            this.textBoxTotalCost.Location = new System.Drawing.Point(105, 229);
            this.textBoxTotalCost.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxTotalCost.Name = "textBoxTotalCost";
            this.textBoxTotalCost.Size = new System.Drawing.Size(132, 22);
            this.textBoxTotalCost.TabIndex = 2;
            this.textBoxTotalCost.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            this.textBoxTotalCost.Enter += new System.EventHandler(this.inputTextBox_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 180);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "被保番";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 128);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "診療年月";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 232);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "合計金額";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxGroupID);
            this.panel1.Controls.Add(this.textBoxScanID);
            this.panel1.Controls.Add(this.dataGridView2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnAfterReze);
            this.panel1.Controls.Add(this.btnBeforeReze);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.btnAfter10);
            this.panel1.Controls.Add(this.btnBefore10);
            this.panel1.Controls.Add(this.textBoxReceiptID);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnAfterCheck);
            this.panel1.Controls.Add(this.btnBeforeCheck);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxInsuredNo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxMediMonth);
            this.panel1.Controls.Add(this.textBoxTotalCost);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(894, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(265, 748);
            this.panel1.TabIndex = 12;
            // 
            // textBoxGroupID
            // 
            this.textBoxGroupID.Location = new System.Drawing.Point(105, 89);
            this.textBoxGroupID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxGroupID.Name = "textBoxGroupID";
            this.textBoxGroupID.ReadOnly = true;
            this.textBoxGroupID.Size = new System.Drawing.Size(132, 22);
            this.textBoxGroupID.TabIndex = 38;
            this.textBoxGroupID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxScanID
            // 
            this.textBoxScanID.Location = new System.Drawing.Point(29, 89);
            this.textBoxScanID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxScanID.Name = "textBoxScanID";
            this.textBoxScanID.ReadOnly = true;
            this.textBoxScanID.Size = new System.Drawing.Size(67, 22);
            this.textBoxScanID.TabIndex = 37;
            this.textBoxScanID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.ColumnHeadersVisible = false;
            this.dataGridView2.Location = new System.Drawing.Point(15, 794);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowTemplate.Height = 21;
            this.dataGridView2.Size = new System.Drawing.Size(232, 116);
            this.dataGridView2.TabIndex = 35;
            this.dataGridView2.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellDoubleClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 775);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 15);
            this.label5.TabIndex = 34;
            this.label5.Text = "重複情報";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button11);
            this.groupBox3.Controls.Add(this.numericUpDown1);
            this.groupBox3.Location = new System.Drawing.Point(15, 694);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(232, 60);
            this.groupBox3.TabIndex = 32;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "バックしきい値";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(148, 19);
            this.button11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(69, 29);
            this.button11.TabIndex = 34;
            this.button11.Text = "確定";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown1.Location = new System.Drawing.Point(43, 22);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            220,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(85, 22);
            this.numericUpDown1.TabIndex = 32;
            this.numericUpDown1.Value = new decimal(new int[] {
            220,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button10);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(15, 619);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(232, 68);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "画像調整";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(120, 25);
            this.button10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(97, 29);
            this.button10.TabIndex = 30;
            this.button10.Text = "差し替え";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(15, 25);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(97, 29);
            this.button4.TabIndex = 29;
            this.button4.Text = "回転";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBoxOutdata);
            this.groupBox4.Location = new System.Drawing.Point(15, 552);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(232, 59);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "関連付けられた電算管理番号";
            // 
            // textBoxOutdata
            // 
            this.textBoxOutdata.ContextMenuStrip = this.contextMenuStrip2;
            this.textBoxOutdata.Location = new System.Drawing.Point(24, 22);
            this.textBoxOutdata.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxOutdata.Name = "textBoxOutdata";
            this.textBoxOutdata.ReadOnly = true;
            this.textBoxOutdata.Size = new System.Drawing.Size(183, 22);
            this.textBoxOutdata.TabIndex = 27;
            this.textBoxOutdata.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxOutdata.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.直前レセプトの続紙にするToolStripMenuItem,
            this.直接入力ToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(197, 48);
            // 
            // 直前レセプトの続紙にするToolStripMenuItem
            // 
            this.直前レセプトの続紙にするToolStripMenuItem.Name = "直前レセプトの続紙にするToolStripMenuItem";
            this.直前レセプトの続紙にするToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.直前レセプトの続紙にするToolStripMenuItem.Text = "直前レセプトの続紙にする";
            this.直前レセプトの続紙にするToolStripMenuItem.Click += new System.EventHandler(this.直前レセプトの続紙にするToolStripMenuItem_Click);
            // 
            // 直接入力ToolStripMenuItem
            // 
            this.直接入力ToolStripMenuItem.Name = "直接入力ToolStripMenuItem";
            this.直接入力ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.直接入力ToolStripMenuItem.Text = "直接入力";
            this.直接入力ToolStripMenuItem.Click += new System.EventHandler(this.直接入力ToolStripMenuItem_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton4);
            this.groupBox2.Controls.Add(this.radioButton3);
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Controls.Add(this.radioButton1);
            this.groupBox2.Location = new System.Drawing.Point(15, 392);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(232, 152);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "エラー内容";
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(37, 121);
            this.radioButton4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(145, 19);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "修正済み/問題なし";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.errorRadioButton_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(37, 80);
            this.radioButton3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(55, 19);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "保留";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.errorRadioButton_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(37, 52);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(129, 19);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "無視・出力しない";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.errorRadioButton_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(37, 25);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(103, 19);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "未確認エラー";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.errorRadioButton_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 60);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 15);
            this.label6.TabIndex = 25;
            this.label6.Text = "label6";
            // 
            // btnAfterReze
            // 
            this.btnAfterReze.Location = new System.Drawing.Point(136, 356);
            this.btnAfterReze.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAfterReze.Name = "btnAfterReze";
            this.btnAfterReze.Size = new System.Drawing.Size(100, 29);
            this.btnAfterReze.TabIndex = 24;
            this.btnAfterReze.Text = "後レセプト";
            this.btnAfterReze.UseVisualStyleBackColor = true;
            this.btnAfterReze.Click += new System.EventHandler(this.btnAfterReze_Click);
            // 
            // btnBeforeReze
            // 
            this.btnBeforeReze.Location = new System.Drawing.Point(28, 356);
            this.btnBeforeReze.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBeforeReze.Name = "btnBeforeReze";
            this.btnBeforeReze.Size = new System.Drawing.Size(100, 29);
            this.btnBeforeReze.TabIndex = 23;
            this.btnBeforeReze.Text = "前レセプト";
            this.btnBeforeReze.UseVisualStyleBackColor = true;
            this.btnBeforeReze.Click += new System.EventHandler(this.button3_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(15, 11);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(235, 41);
            this.label7.TabIndex = 22;
            this.label7.Text = "label7";
            // 
            // btnAfter10
            // 
            this.btnAfter10.Location = new System.Drawing.Point(136, 306);
            this.btnAfter10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAfter10.Name = "btnAfter10";
            this.btnAfter10.Size = new System.Drawing.Size(100, 29);
            this.btnAfter10.TabIndex = 20;
            this.btnAfter10.Tag = "";
            this.btnAfter10.Text = "check 10>";
            this.btnAfter10.UseVisualStyleBackColor = true;
            this.btnAfter10.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnBefore10
            // 
            this.btnBefore10.Location = new System.Drawing.Point(28, 306);
            this.btnBefore10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBefore10.Name = "btnBefore10";
            this.btnBefore10.Size = new System.Drawing.Size(100, 29);
            this.btnBefore10.TabIndex = 19;
            this.btnBefore10.Text = "<10 check";
            this.btnBefore10.UseVisualStyleBackColor = true;
            this.btnBefore10.Click += new System.EventHandler(this.button6_Click);
            // 
            // textBoxReceiptID
            // 
            this.textBoxReceiptID.Location = new System.Drawing.Point(105, 56);
            this.textBoxReceiptID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxReceiptID.Name = "textBoxReceiptID";
            this.textBoxReceiptID.ReadOnly = true;
            this.textBoxReceiptID.Size = new System.Drawing.Size(132, 22);
            this.textBoxReceiptID.TabIndex = 17;
            this.textBoxReceiptID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 151);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(184, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "続紙[ --- ]　エラー[ ***** ]";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(894, 748);
            this.panel2.TabIndex = 13;
            this.panel2.SizeChanged += new System.EventHandler(this.panel2_SizeChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 752);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1159, 176);
            this.panel3.TabIndex = 14;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.Size = new System.Drawing.Size(995, 176);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.この電算管理番号を指定するToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(192, 26);
            // 
            // この電算管理番号を指定するToolStripMenuItem
            // 
            this.この電算管理番号を指定するToolStripMenuItem.Name = "この電算管理番号を指定するToolStripMenuItem";
            this.この電算管理番号を指定するToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.この電算管理番号を指定するToolStripMenuItem.Text = "このレセプトに関連付ける";
            this.この電算管理番号を指定するToolStripMenuItem.Click += new System.EventHandler(this.このレセプトに関連付けるToolStripMenuItem_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button8);
            this.panel4.Controls.Add(this.radioButton6);
            this.panel4.Controls.Add(this.radioButton5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(995, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(164, 176);
            this.panel4.TabIndex = 1;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(32, 116);
            this.button8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(100, 29);
            this.button8.TabIndex = 4;
            this.button8.Text = "更新";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(27, 76);
            this.radioButton6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(100, 19);
            this.radioButton6.TabIndex = 3;
            this.radioButton6.Text = "同一被保番";
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.dataSelecttRadioButton_CheckedChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Checked = true;
            this.radioButton5.Location = new System.Drawing.Point(27, 49);
            this.radioButton5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(70, 19);
            this.radioButton5.TabIndex = 2;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "同一点";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.dataSelecttRadioButton_CheckedChanged);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 748);
            this.splitter1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1159, 4);
            this.splitter1.TabIndex = 15;
            this.splitter1.TabStop = false;
            // 
            // ImageCheckForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1159, 928);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ImageCheckForm";
            this.Text = "画像チェック";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ImageCheckForm_FormClosing);
            this.Load += new System.EventHandler(this.ImageCheckForm_Load);
            this.Shown += new System.EventHandler(this.ImageCheckForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.contextMenuStrip2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAfterCheck;
        private System.Windows.Forms.Button btnBeforeCheck;
        private System.Windows.Forms.TextBox textBoxInsuredNo;
        private System.Windows.Forms.TextBox textBoxMediMonth;
        private System.Windows.Forms.TextBox textBoxTotalCost;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxReceiptID;
        private System.Windows.Forms.Button btnAfter10;
        private System.Windows.Forms.Button btnBefore10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAfterReze;
        private System.Windows.Forms.Button btnBeforeReze;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxOutdata;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem この電算管理番号を指定するToolStripMenuItem;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TextBox textBoxGroupID;
        private System.Windows.Forms.TextBox textBoxScanID;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem 直前レセプトの続紙にするToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 直接入力ToolStripMenuItem;
    }
}

