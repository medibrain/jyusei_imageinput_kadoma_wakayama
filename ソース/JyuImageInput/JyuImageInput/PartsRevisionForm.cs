﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class PartsRevisionForm : Form
    {
        List<PartsCheckForm.PartsErrorData> errorList;

        bool changeFlug = false;

        int nowIndex = 0;

        /// <summary>
        /// 表示モード
        /// </summary>
        private enum ImageMode { WidthHeightBase, OriginalBase, PanelBase, FreeRatio };
        private ImageMode imageMode = ImageMode.PanelBase;


        public PartsRevisionForm(List<PartsCheckForm.PartsErrorData> errorList)
        {
            InitializeComponent();
            this.errorList = errorList;
        }

        private void partsSelect(int errorIndex)
        {
            textBox1.ReadOnly = true;
            textBox2.ReadOnly = true;
            textBox3.ReadOnly = true;
            textBox4.ReadOnly = true;
            textBox5.ReadOnly = true;
            textBox6.ReadOnly = true;
            textBox7.ReadOnly = true;
            textBox8.ReadOnly = true;
            textBox9.ReadOnly = true;
            textBox10.ReadOnly = true;
            textBox13.ReadOnly = true;
            textBox14.ReadOnly = true;
            textBox15.ReadOnly = true;
            textBox16.ReadOnly = true;


            if (errorList[errorIndex].order == 1)
            {
                textBox1.ReadOnly = false;
                textBox2.ReadOnly = false;
            }
            if (errorList[errorIndex].order == 2)
            {
                textBox3.ReadOnly = false;
                textBox4.ReadOnly = false;
            }
            if (errorList[errorIndex].order == 3)
            {
                textBox5.ReadOnly = false;
                textBox6.ReadOnly = false;
            }
            if (errorList[errorIndex].order == 4)
            {
                textBox7.ReadOnly = false;
                textBox8.ReadOnly = false;
            }
            if (errorList[errorIndex].order == 5)
            {
                textBox9.ReadOnly = false;
                textBox10.ReadOnly = false;
            }
            if (errorList[errorIndex].order == 10)
            {
                textBox13.ReadOnly = false;
                textBox14.ReadOnly = false;
            }
            if (errorList[errorIndex].order == 20)
            {
                textBox15.ReadOnly = false;
                textBox16.ReadOnly = false;
            }

            long rid = errorList[errorIndex].rid;

            //データベースから読み出し
            using (var cmd = SQLClass.CreateCommand("SELECT rid, name1, name2, name3, name4, name5, name6, name7, name8, name9, name10, s1, s2, e1, e2 FROM parts WHERE rid = :rid"))
            {
                cmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);
                cmd.Parameters["rid"].Value = rid;

                using (var dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        dr.Read();
                        var os = new object[dr.FieldCount];
                        dr.GetValues(os);
                        textBox1.Text = (string)os[1];
                        textBox2.Text = (string)os[6];
                        textBox3.Text = (string)os[2];
                        textBox4.Text = (string)os[7];
                        textBox5.Text = (string)os[3];
                        textBox6.Text = (string)os[8];
                        textBox7.Text = (string)os[4];
                        textBox8.Text = (string)os[9];
                        textBox9.Text = (string)os[5];
                        textBox10.Text = (string)os[10];
                        textBox11.Text = os[0].ToString();
                        textBox12.Text = (nowIndex + 1).ToString() + " / " + errorList.Count;
                        textBox13.Text = os[11].ToString();
                        textBox14.Text = os[12].ToString();
                        textBox15.Text = os[13].ToString();
                        textBox16.Text = os[14].ToString();
                    }
                    else
                    {
                        textBox1.Text = string.Empty;
                        textBox2.Text = string.Empty;
                        textBox3.Text = string.Empty;
                        textBox4.Text = string.Empty;
                        textBox5.Text = string.Empty;
                        textBox6.Text = string.Empty;
                        textBox7.Text = string.Empty;
                        textBox8.Text = string.Empty;
                        textBox9.Text = string.Empty;
                        textBox10.Text = string.Empty;
                        textBox11.Text = "エラー";
                        textBox13.Text = string.Empty;
                        textBox14.Text = string.Empty;
                        textBox15.Text = string.Empty;
                        textBox16.Text = string.Empty;
                    }
                }
            }

            //画像表示
            //20190614135841 furukawa st ////////////////////////
            //billstrの桁数を変更（令和1年は前ゼロなくなるので）
            
            string billstr = (rid / 1000000).ToString();
                        //string billstr = (rid / 1000000).ToString("0000");
            //20190614135841 furukawa ed ////////////////////////

            string floderNo = ((rid / 10000) % 100).ToString("00");
            string ridStr = (rid % 1000000).ToString("000000");

            string imgstr = Settings.imagePath + "\\" + billstr + floderNo + "\\" + rid.ToString();

            //画像があるか確認 gif jpg 両対応
            if (System.IO.File.Exists(imgstr + ".gif")) imgstr += ".gif";
            else if (System.IO.File.Exists(imgstr + ".jpg")) imgstr += ".jpg";
            else imgstr = "def.gif";

            pictureBox1.Image = Image.FromFile(imgstr);

            changeFlug = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //現在表示中のデータを登録
            if (changeFlug)
            {
                //登録前に確認
                if (partsUpdateCheck())
                {
                    var res = MessageBox.Show("入力が一致していません 修正せずに別のレセプトを表示しますか？", "不一致確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (res != System.Windows.Forms.DialogResult.Yes) return;
                }
                else
                {
                    partsUpdate(errorList[nowIndex].rid);
                }
            }

            //次のレセプトを表示する
            if (nowIndex < errorList.Count - 1)
            {
                nowIndex++;
                partsSelect(nowIndex);
            }
        }

        /// <summary>
        /// 1回目と2回目の入力が完全に一致しているか、登録前に確かめます
        /// </summary>
        /// <returns></returns>
        private bool partsUpdateCheck()
        {
            bool errorFlug = false;

            if (!textBox1.ReadOnly && textBox1.Text.Trim() != textBox2.Text.Trim()) errorFlug = true;
            if (!textBox3.ReadOnly && textBox3.Text.Trim() != textBox4.Text.Trim()) errorFlug = true;
            if (!textBox5.ReadOnly && textBox5.Text.Trim() != textBox6.Text.Trim()) errorFlug = true;
            if (!textBox7.ReadOnly && textBox7.Text.Trim() != textBox8.Text.Trim()) errorFlug = true;
            if (!textBox9.ReadOnly && textBox9.Text.Trim() != textBox10.Text.Trim()) errorFlug = true;
            if (!textBox13.ReadOnly && textBox13.Text.Trim() != textBox14.Text.Trim()) errorFlug = true;
            if (!textBox15.ReadOnly && textBox15.Text.Trim() != textBox16.Text.Trim()) errorFlug = true;

            int temp;
            if (!int.TryParse(textBox13.Text, out temp)) errorFlug = true;
            if (!int.TryParse(textBox14.Text, out temp)) errorFlug = true;
            if (!int.TryParse(textBox15.Text, out temp)) errorFlug = true;
            if (!int.TryParse(textBox16.Text, out temp)) errorFlug = true;

            return errorFlug;
        }

        private bool partsUpdate(long rid)
        {
            using(var cmd = SQLClass.CreateCommand("UPDATE parts SET name1 = :name1, name2 = :name2, name3 = :name3, name4 = :name4, name5 = :name5, " +
                "name6 = :name6, name7 = :name7, name8 = :name8, name9 = :name9, name10 = :name10, " +
                "s1 = :s1, s2 = :s2, e1 = :e1, e2 = :e2 WHERE rid = :rid"))
            {
                cmd.Parameters.Add("name1", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters.Add("name2", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters.Add("name3", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters.Add("name4", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters.Add("name5", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters.Add("name6", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters.Add("name7", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters.Add("name8", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters.Add("name9", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters.Add("name10", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters.Add("s1", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add("s2", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add("e1", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add("e2", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);

                cmd.Parameters["name1"].Value = partsNameAdjust(textBox1.Text);
                cmd.Parameters["name2"].Value = partsNameAdjust(textBox3.Text);
                cmd.Parameters["name3"].Value = partsNameAdjust(textBox5.Text);
                cmd.Parameters["name4"].Value = partsNameAdjust(textBox7.Text);
                cmd.Parameters["name5"].Value = partsNameAdjust(textBox9.Text);
                cmd.Parameters["name6"].Value = partsNameAdjust(textBox2.Text);
                cmd.Parameters["name7"].Value = partsNameAdjust(textBox4.Text);
                cmd.Parameters["name8"].Value = partsNameAdjust(textBox6.Text);
                cmd.Parameters["name9"].Value = partsNameAdjust(textBox8.Text);
                cmd.Parameters["name10"].Value = partsNameAdjust(textBox10.Text);
                cmd.Parameters["s1"].Value = int.Parse(textBox13.Text);
                cmd.Parameters["s2"].Value = int.Parse(textBox14.Text);
                cmd.Parameters["e1"].Value = int.Parse(textBox15.Text);
                cmd.Parameters["e2"].Value = int.Parse(textBox16.Text);
                cmd.Parameters["rid"].Value = rid;

                int res;

                try
                {
                    res = cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    MessageBox.Show("部位の修正に失敗しました\r\n" + e.Message);
                    return false;
                }

                if (res != 1)
                {
                    MessageBox.Show("部位の修正に失敗しました\r\n");
                    return false;
                }
            }
            return true;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ImageShowSettingChange();
            ImageReset();
        }

        private void ImageReset()
        {
            if (pictureBox1.Image == null) return;
            if (imageMode == ImageMode.WidthHeightBase)
            {
                //幅いっぱい、もしくは高さいっぱいにする
                float wf = (float)(panel2.Width - SystemInformation.HorizontalScrollBarHeight) / (float)pictureBox1.Image.Width;
                float wh = (float)(panel2.Height - SystemInformation.VerticalScrollBarWidth) / (float)pictureBox1.Image.Height;

                float ratio;
                ratio = wf < wh ? wh : wf;

                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox1.Dock = DockStyle.None;
                pictureBox1.Height = (int)((float)pictureBox1.Image.Height * ratio);
                pictureBox1.Width = (int)((float)pictureBox1.Image.Width * ratio);
            }
            else if (imageMode == ImageMode.PanelBase)
            {
                //全面表示にする
                pictureBox1.Dock = DockStyle.None;
                pictureBox1.Location = new Point(0, 0);
                pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;

                imageMode = ImageMode.PanelBase;
            }
            else if (imageMode == ImageMode.OriginalBase)
            {
                //等倍表示にする
                pictureBox1.Dock = DockStyle.Fill;
                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            }
            else
            {
                //任意の倍率
            }
        }

        private void ImageShowSettingChange()
        {
            if (imageMode == ImageMode.OriginalBase)
            {
                //幅いっぱい、もしくは高さいっぱいにする
                imageMode = ImageMode.WidthHeightBase;
            }
            else if (imageMode == ImageMode.WidthHeightBase)
            {
                //全面表示にする
                imageMode = ImageMode.PanelBase;
            }
            else
            {
                //等倍表示にする
                imageMode = ImageMode.OriginalBase;
            }
        }

        private void panel2_SizeChanged(object sender, EventArgs e)
        {
            ImageReset();
        }

        private void partsTextBox_TextChanged(object sender, EventArgs e)
        {
            changeFlug = true;
        }

        private void PartsRevisionForm_Shown(object sender, EventArgs e)
        {
            if (errorList.Count < 1)
            {
                MessageBox.Show("修正できるエラーが存在しません");
                this.Close();
                return;
            }

            partsSelect(0);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //現在表示中のデータを登録
            if (changeFlug)
            {
                //登録前に確認
                if (partsUpdateCheck())
                {
                    var res = MessageBox.Show("入力が一致していません 修正せずに別のレセプトを表示しますか？", "不一致確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (res != System.Windows.Forms.DialogResult.Yes) return;
                }
                else
                {
                    partsUpdate(errorList[nowIndex].rid);
                }
            }

            //次のレセプトを表示する
            if (nowIndex > 0)
            {
                nowIndex--;
                partsSelect(nowIndex);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string partsNameAdjust(string str)
        {
            return str.Trim();
        }
    }
}
