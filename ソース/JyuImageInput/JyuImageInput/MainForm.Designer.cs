﻿namespace JyuImageInput
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.txtHihoNum = new System.Windows.Forms.TextBox();
            this.txtSinryoYM = new System.Windows.Forms.TextBox();
            this.txtSum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnNext50 = new System.Windows.Forms.Button();
            this.btnNext10 = new System.Windows.Forms.Button();
            this.btnPrev10 = new System.Windows.Forms.Button();
            this.btnPrev50 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnGroupChange = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.btnUserChange = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ファイルFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.終了XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.データ出力OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ツールTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.スキャンデータ管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.入力情報検索ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ユーザー情報ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.漏れチェックToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.部位チェックToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.オプションOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.モードToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.通常入力ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.部位入力1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.部位入力2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.日付入力１回目FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.日付入力２回目SToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ヘルプHToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.バージョン情報AToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(299, 175);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnNext.Location = new System.Drawing.Point(118, 586);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = "次へ";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            this.btnNext.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.btnNext_PreviewKeyDown);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPrevious.Location = new System.Drawing.Point(37, 586);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(75, 23);
            this.btnPrevious.TabIndex = 4;
            this.btnPrevious.TabStop = false;
            this.btnPrevious.Text = "前へ";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // txtHihoNum
            // 
            this.txtHihoNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtHihoNum.Location = new System.Drawing.Point(77, 85);
            this.txtHihoNum.Name = "txtHihoNum";
            this.txtHihoNum.Size = new System.Drawing.Size(100, 22);
            this.txtHihoNum.TabIndex = 1;
            this.txtHihoNum.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            this.txtHihoNum.Enter += new System.EventHandler(this.inputTextBox_Enter);
            // 
            // txtSinryoYM
            // 
            this.txtSinryoYM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSinryoYM.Location = new System.Drawing.Point(77, 26);
            this.txtSinryoYM.Name = "txtSinryoYM";
            this.txtSinryoYM.Size = new System.Drawing.Size(100, 22);
            this.txtSinryoYM.TabIndex = 0;
            this.txtSinryoYM.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            this.txtSinryoYM.Enter += new System.EventHandler(this.inputTextBox_Enter);
            // 
            // txtSum
            // 
            this.txtSum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSum.Location = new System.Drawing.Point(77, 129);
            this.txtSum.MaxLength = 15;
            this.txtSum.Name = "txtSum";
            this.txtSum.Size = new System.Drawing.Size(100, 22);
            this.txtSum.TabIndex = 2;
            this.txtSum.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            this.txtSum.Enter += new System.EventHandler(this.inputTextBox_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "被保番";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "診療年月";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "合計金額";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox6);
            this.panel1.Controls.Add(this.textBox13);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.btnNext50);
            this.panel1.Controls.Add(this.btnNext10);
            this.panel1.Controls.Add(this.btnPrev10);
            this.panel1.Controls.Add(this.btnPrev50);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btnNext);
            this.panel1.Controls.Add(this.btnPrevious);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(535, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(229, 817);
            this.panel1.TabIndex = 12;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBox15);
            this.groupBox6.Controls.Add(this.textBox14);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Enabled = false;
            this.groupBox6.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox6.Location = new System.Drawing.Point(16, 519);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(202, 46);
            this.groupBox6.TabIndex = 26;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "日付";
            // 
            // textBox15
            // 
            this.textBox15.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox15.Location = new System.Drawing.Point(160, 19);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(31, 22);
            this.textBox15.TabIndex = 1;
            this.textBox15.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            // 
            // textBox14
            // 
            this.textBox14.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox14.Location = new System.Drawing.Point(61, 19);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(31, 22);
            this.textBox14.TabIndex = 0;
            this.textBox14.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(103, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 15);
            this.label16.TabIndex = 1;
            this.label16.Text = "終了日";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 15);
            this.label15.TabIndex = 0;
            this.label15.Text = "開始日";
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox13.Location = new System.Drawing.Point(81, 113);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(135, 22);
            this.textBox13.TabIndex = 25;
            this.textBox13.TabStop = false;
            this.textBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label14.Location = new System.Drawing.Point(6, 116);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 15);
            this.label14.TabIndex = 24;
            this.label14.Text = "入力モード";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.textBox12);
            this.groupBox5.Controls.Add(this.textBox11);
            this.groupBox5.Controls.Add(this.textBox10);
            this.groupBox5.Controls.Add(this.textBox9);
            this.groupBox5.Controls.Add(this.textBox8);
            this.groupBox5.Enabled = false;
            this.groupBox5.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox5.Location = new System.Drawing.Point(16, 337);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 153);
            this.groupBox5.TabIndex = 23;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "部位入力";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 120);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 15);
            this.label13.TabIndex = 11;
            this.label13.Text = "部位5";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 97);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 15);
            this.label12.TabIndex = 10;
            this.label12.Text = "部位4";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 15);
            this.label11.TabIndex = 9;
            this.label11.Text = "部位3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 51);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 15);
            this.label10.TabIndex = 8;
            this.label10.Text = "部位2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 15);
            this.label9.TabIndex = 7;
            this.label9.Text = "部位1";
            // 
            // textBox12
            // 
            this.textBox12.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox12.Location = new System.Drawing.Point(76, 117);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(100, 22);
            this.textBox12.TabIndex = 4;
            this.textBox12.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            this.textBox12.Enter += new System.EventHandler(this.inputTextBox_Enter);
            // 
            // textBox11
            // 
            this.textBox11.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox11.Location = new System.Drawing.Point(76, 94);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 22);
            this.textBox11.TabIndex = 3;
            this.textBox11.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            this.textBox11.Enter += new System.EventHandler(this.inputTextBox_Enter);
            // 
            // textBox10
            // 
            this.textBox10.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox10.Location = new System.Drawing.Point(76, 71);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 22);
            this.textBox10.TabIndex = 2;
            this.textBox10.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            this.textBox10.Enter += new System.EventHandler(this.inputTextBox_Enter);
            // 
            // textBox9
            // 
            this.textBox9.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox9.Location = new System.Drawing.Point(76, 48);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 22);
            this.textBox9.TabIndex = 1;
            this.textBox9.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            this.textBox9.Enter += new System.EventHandler(this.inputTextBox_Enter);
            // 
            // textBox8
            // 
            this.textBox8.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox8.Location = new System.Drawing.Point(76, 25);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 22);
            this.textBox8.TabIndex = 0;
            this.textBox8.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            this.textBox8.Enter += new System.EventHandler(this.inputTextBox_Enter);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtSinryoYM);
            this.groupBox4.Controls.Add(this.txtSum);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.txtHihoNum);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox4.Location = new System.Drawing.Point(16, 159);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 163);
            this.groupBox4.TabIndex = 22;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "基本入力";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(184, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "続紙[ --- ]　エラー[ ***** ]";
            // 
            // btnNext50
            // 
            this.btnNext50.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnNext50.Location = new System.Drawing.Point(161, 612);
            this.btnNext50.Name = "btnNext50";
            this.btnNext50.Size = new System.Drawing.Size(47, 23);
            this.btnNext50.TabIndex = 21;
            this.btnNext50.TabStop = false;
            this.btnNext50.Text = "50>";
            this.btnNext50.UseVisualStyleBackColor = true;
            this.btnNext50.Click += new System.EventHandler(this.button8_Click);
            // 
            // btnNext10
            // 
            this.btnNext10.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnNext10.Location = new System.Drawing.Point(114, 612);
            this.btnNext10.Name = "btnNext10";
            this.btnNext10.Size = new System.Drawing.Size(47, 23);
            this.btnNext10.TabIndex = 20;
            this.btnNext10.TabStop = false;
            this.btnNext10.Tag = "";
            this.btnNext10.Text = "10>";
            this.btnNext10.UseVisualStyleBackColor = true;
            this.btnNext10.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnPrev10
            // 
            this.btnPrev10.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPrev10.Location = new System.Drawing.Point(67, 612);
            this.btnPrev10.Name = "btnPrev10";
            this.btnPrev10.Size = new System.Drawing.Size(47, 23);
            this.btnPrev10.TabIndex = 19;
            this.btnPrev10.TabStop = false;
            this.btnPrev10.Text = "<10";
            this.btnPrev10.UseVisualStyleBackColor = true;
            this.btnPrev10.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnPrev50
            // 
            this.btnPrev50.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPrev50.Location = new System.Drawing.Point(20, 612);
            this.btnPrev50.Name = "btnPrev50";
            this.btnPrev50.Size = new System.Drawing.Size(47, 23);
            this.btnPrev50.TabIndex = 18;
            this.btnPrev50.TabStop = false;
            this.btnPrev50.Text = "<50";
            this.btnPrev50.UseVisualStyleBackColor = true;
            this.btnPrev50.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox7);
            this.groupBox3.Controls.Add(this.textBox6);
            this.groupBox3.Location = new System.Drawing.Point(8, 775);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(211, 50);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "管理ID / 入力日時";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(104, 18);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(94, 19);
            this.textBox7.TabIndex = 3;
            this.textBox7.TabStop = false;
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(12, 18);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(88, 19);
            this.textBox6.TabIndex = 17;
            this.textBox6.TabStop = false;
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.btnGroupChange);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox2.Location = new System.Drawing.Point(9, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(209, 95);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "現在の入力グループ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 15);
            this.label8.TabIndex = 16;
            this.label8.Text = "枚数：";
            // 
            // btnGroupChange
            // 
            this.btnGroupChange.Location = new System.Drawing.Point(126, 66);
            this.btnGroupChange.Name = "btnGroupChange";
            this.btnGroupChange.Size = new System.Drawing.Size(75, 23);
            this.btnGroupChange.TabIndex = 0;
            this.btnGroupChange.TabStop = false;
            this.btnGroupChange.Text = "グループ変更";
            this.btnGroupChange.UseVisualStyleBackColor = true;
            this.btnGroupChange.Click += new System.EventHandler(this.button3_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 15);
            this.label7.TabIndex = 15;
            this.label7.Text = "グループID：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.btnUserChange);
            this.groupBox1.Location = new System.Drawing.Point(8, 664);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(211, 83);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "現在の入力者情報";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 13;
            this.label6.Text = "入力枚数";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(62, 56);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(57, 19);
            this.textBox5.TabIndex = 12;
            this.textBox5.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "入力者名";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(62, 28);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(138, 19);
            this.textBox4.TabIndex = 12;
            this.textBox4.TabStop = false;
            // 
            // btnUserChange
            // 
            this.btnUserChange.Location = new System.Drawing.Point(125, 54);
            this.btnUserChange.Name = "btnUserChange";
            this.btnUserChange.Size = new System.Drawing.Size(75, 23);
            this.btnUserChange.TabIndex = 11;
            this.btnUserChange.TabStop = false;
            this.btnUserChange.Text = "入力者変更";
            this.btnUserChange.UseVisualStyleBackColor = true;
            this.btnUserChange.Click += new System.EventHandler(this.button4_Click);
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(535, 817);
            this.panel2.TabIndex = 13;
            this.panel2.SizeChanged += new System.EventHandler(this.panel2_SizeChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Yu Gothic UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ファイルFToolStripMenuItem,
            this.ツールTToolStripMenuItem,
            this.モードToolStripMenuItem,
            this.ヘルプHToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(764, 28);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ファイルFToolStripMenuItem
            // 
            this.ファイルFToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.終了XToolStripMenuItem,
            this.データ出力OToolStripMenuItem});
            this.ファイルFToolStripMenuItem.Name = "ファイルFToolStripMenuItem";
            this.ファイルFToolStripMenuItem.Size = new System.Drawing.Size(80, 24);
            this.ファイルFToolStripMenuItem.Text = "ファイル(&F)";
            // 
            // 終了XToolStripMenuItem
            // 
            this.終了XToolStripMenuItem.Name = "終了XToolStripMenuItem";
            this.終了XToolStripMenuItem.Size = new System.Drawing.Size(162, 24);
            this.終了XToolStripMenuItem.Text = "終了(&X)";
            this.終了XToolStripMenuItem.Click += new System.EventHandler(this.終了XToolStripMenuItem_Click);
            // 
            // データ出力OToolStripMenuItem
            // 
            this.データ出力OToolStripMenuItem.Name = "データ出力OToolStripMenuItem";
            this.データ出力OToolStripMenuItem.Size = new System.Drawing.Size(162, 24);
            this.データ出力OToolStripMenuItem.Text = "データ出力(&O)";
            this.データ出力OToolStripMenuItem.Click += new System.EventHandler(this.データ出力OToolStripMenuItem_Click);
            // 
            // ツールTToolStripMenuItem
            // 
            this.ツールTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.スキャンデータ管理ToolStripMenuItem,
            this.入力情報検索ToolStripMenuItem,
            this.ユーザー情報ToolStripMenuItem,
            this.toolStripSeparator1,
            this.漏れチェックToolStripMenuItem,
            this.部位チェックToolStripMenuItem,
            this.オプションOToolStripMenuItem});
            this.ツールTToolStripMenuItem.Name = "ツールTToolStripMenuItem";
            this.ツールTToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.ツールTToolStripMenuItem.Text = "ツール(&T)";
            // 
            // スキャンデータ管理ToolStripMenuItem
            // 
            this.スキャンデータ管理ToolStripMenuItem.Name = "スキャンデータ管理ToolStripMenuItem";
            this.スキャンデータ管理ToolStripMenuItem.Size = new System.Drawing.Size(184, 24);
            this.スキャンデータ管理ToolStripMenuItem.Text = "スキャンデータ管理";
            this.スキャンデータ管理ToolStripMenuItem.Click += new System.EventHandler(this.スキャンデータ管理ToolStripMenuItem_Click);
            // 
            // 入力情報検索ToolStripMenuItem
            // 
            this.入力情報検索ToolStripMenuItem.Name = "入力情報検索ToolStripMenuItem";
            this.入力情報検索ToolStripMenuItem.Size = new System.Drawing.Size(184, 24);
            this.入力情報検索ToolStripMenuItem.Text = "入力情報検索";
            this.入力情報検索ToolStripMenuItem.Click += new System.EventHandler(this.入力情報検索ToolStripMenuItem_Click);
            // 
            // ユーザー情報ToolStripMenuItem
            // 
            this.ユーザー情報ToolStripMenuItem.Name = "ユーザー情報ToolStripMenuItem";
            this.ユーザー情報ToolStripMenuItem.Size = new System.Drawing.Size(184, 24);
            this.ユーザー情報ToolStripMenuItem.Text = "ユーザー情報";
            this.ユーザー情報ToolStripMenuItem.Click += new System.EventHandler(this.ユーザー情報ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(181, 6);
            // 
            // 漏れチェックToolStripMenuItem
            // 
            this.漏れチェックToolStripMenuItem.Name = "漏れチェックToolStripMenuItem";
            this.漏れチェックToolStripMenuItem.Size = new System.Drawing.Size(184, 24);
            this.漏れチェックToolStripMenuItem.Text = "漏れチェック";
            this.漏れチェックToolStripMenuItem.Click += new System.EventHandler(this.漏れチェックToolStripMenuItem_Click);
            // 
            // 部位チェックToolStripMenuItem
            // 
            this.部位チェックToolStripMenuItem.Name = "部位チェックToolStripMenuItem";
            this.部位チェックToolStripMenuItem.Size = new System.Drawing.Size(184, 24);
            this.部位チェックToolStripMenuItem.Text = "部位チェック";
            this.部位チェックToolStripMenuItem.Click += new System.EventHandler(this.部位チェックToolStripMenuItem_Click);
            // 
            // オプションOToolStripMenuItem
            // 
            this.オプションOToolStripMenuItem.Name = "オプションOToolStripMenuItem";
            this.オプションOToolStripMenuItem.Size = new System.Drawing.Size(184, 24);
            this.オプションOToolStripMenuItem.Text = "オプション(&O)";
            this.オプションOToolStripMenuItem.Click += new System.EventHandler(this.オプションOToolStripMenuItem_Click);
            // 
            // モードToolStripMenuItem
            // 
            this.モードToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.通常入力ToolStripMenuItem,
            this.toolStripSeparator2,
            this.部位入力1ToolStripMenuItem,
            this.部位入力2ToolStripMenuItem,
            this.toolStripSeparator3,
            this.日付入力１回目FToolStripMenuItem,
            this.日付入力２回目SToolStripMenuItem});
            this.モードToolStripMenuItem.Name = "モードToolStripMenuItem";
            this.モードToolStripMenuItem.Size = new System.Drawing.Size(76, 24);
            this.モードToolStripMenuItem.Text = "モード(&M)";
            // 
            // 通常入力ToolStripMenuItem
            // 
            this.通常入力ToolStripMenuItem.Checked = true;
            this.通常入力ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.通常入力ToolStripMenuItem.Name = "通常入力ToolStripMenuItem";
            this.通常入力ToolStripMenuItem.Size = new System.Drawing.Size(201, 24);
            this.通常入力ToolStripMenuItem.Text = "通常入力(&N)";
            this.通常入力ToolStripMenuItem.Click += new System.EventHandler(this.通常入力ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(198, 6);
            // 
            // 部位入力1ToolStripMenuItem
            // 
            this.部位入力1ToolStripMenuItem.Name = "部位入力1ToolStripMenuItem";
            this.部位入力1ToolStripMenuItem.Size = new System.Drawing.Size(201, 24);
            this.部位入力1ToolStripMenuItem.Text = "部位入力１回目(&1)";
            this.部位入力1ToolStripMenuItem.Click += new System.EventHandler(this.部位入力1ToolStripMenuItem_Click);
            // 
            // 部位入力2ToolStripMenuItem
            // 
            this.部位入力2ToolStripMenuItem.Name = "部位入力2ToolStripMenuItem";
            this.部位入力2ToolStripMenuItem.Size = new System.Drawing.Size(201, 24);
            this.部位入力2ToolStripMenuItem.Text = "部位入力２回目(&2)";
            this.部位入力2ToolStripMenuItem.Click += new System.EventHandler(this.部位入力2ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(198, 6);
            // 
            // 日付入力１回目FToolStripMenuItem
            // 
            this.日付入力１回目FToolStripMenuItem.Name = "日付入力１回目FToolStripMenuItem";
            this.日付入力１回目FToolStripMenuItem.Size = new System.Drawing.Size(201, 24);
            this.日付入力１回目FToolStripMenuItem.Text = "日付入力１回目(&F)";
            this.日付入力１回目FToolStripMenuItem.Click += new System.EventHandler(this.日付入力１回目FToolStripMenuItem_Click);
            // 
            // 日付入力２回目SToolStripMenuItem
            // 
            this.日付入力２回目SToolStripMenuItem.Name = "日付入力２回目SToolStripMenuItem";
            this.日付入力２回目SToolStripMenuItem.Size = new System.Drawing.Size(201, 24);
            this.日付入力２回目SToolStripMenuItem.Text = "日付入力２回目(&S)";
            this.日付入力２回目SToolStripMenuItem.Click += new System.EventHandler(this.日付入力２回目SToolStripMenuItem_Click);
            // 
            // ヘルプHToolStripMenuItem
            // 
            this.ヘルプHToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.バージョン情報AToolStripMenuItem});
            this.ヘルプHToolStripMenuItem.Name = "ヘルプHToolStripMenuItem";
            this.ヘルプHToolStripMenuItem.Size = new System.Drawing.Size(77, 24);
            this.ヘルプHToolStripMenuItem.Text = "ヘルプ(&H)";
            // 
            // バージョン情報AToolStripMenuItem
            // 
            this.バージョン情報AToolStripMenuItem.Name = "バージョン情報AToolStripMenuItem";
            this.バージョン情報AToolStripMenuItem.Size = new System.Drawing.Size(191, 24);
            this.バージョン情報AToolStripMenuItem.Text = "バージョン情報(&A)...";
            this.バージョン情報AToolStripMenuItem.Click += new System.EventHandler(this.バージョン情報AToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 845);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "柔整入力システム";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.TextBox txtHihoNum;
        private System.Windows.Forms.TextBox txtSinryoYM;
        private System.Windows.Forms.TextBox txtSum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnUserChange;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ファイルFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 終了XToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ツールTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem オプションOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ヘルプHToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem バージョン情報AToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem スキャンデータ管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Button btnGroupChange;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnNext50;
        private System.Windows.Forms.Button btnNext10;
        private System.Windows.Forms.Button btnPrev10;
        private System.Windows.Forms.Button btnPrev50;
        private System.Windows.Forms.ToolStripMenuItem ユーザー情報ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 入力情報検索ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 漏れチェックToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.ToolStripMenuItem モードToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 通常入力ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 部位入力1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 部位入力2ToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ToolStripMenuItem 部位チェックToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 日付入力１回目FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 日付入力２回目SToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem データ出力OToolStripMenuItem;
    }
}

