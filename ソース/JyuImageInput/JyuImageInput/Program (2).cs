﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ImageInputSystem
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Mutexクラスの作成
            //"MyName"の部分を適当な文字列に変えてください
            System.Threading.Mutex mutex = new System.Threading.Mutex(false, "ReceiptScanInput");
            //ミューテックスの所有権を要求する
            if (mutex.WaitOne(0, false) == false)
            {
                //すでに起動していると判断して終了
                MessageBox.Show("すでに起動しています",
                    "多重起動禁止",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
