﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StrPair = System.Collections.Generic.KeyValuePair<string, string>;

namespace JyuImageInput
{
    public partial class ScanDataForm : Form
    {
        /// <summary> 読込元ファイル一覧の取得 </summary>
        /// <param name="dir">読込元ディレクトリ</param>
        /// <returns>ファイル一覧</returns>
        List<string> GetFileList(string dir)
        {
            var fileList = System.IO.Directory.GetFiles(dir, "*.jpg").ToList();
            fileList.AddRange(System.IO.Directory.GetFiles(dir, "*.jpeg"));
            fileList.AddRange(System.IO.Directory.GetFiles(dir, "*.tif"));
            fileList.AddRange(System.IO.Directory.GetFiles(dir, "*.tiff"));
            fileList.AddRange(System.IO.Directory.GetFiles(dir, "*.png"));
            fileList.AddRange(System.IO.Directory.GetFiles(dir, "*.gif"));
            fileList.AddRange(System.IO.Directory.GetFiles(dir, "*.bmp"));
            fileList.Sort();
            return fileList;
        }


        public ScanDataForm()
        {
            InitializeComponent();
            dataGridView1.Rows.Add(4);
            dataGridView1[0, 0].Value = "ファイル(画像)数";
            dataGridView1[0, 1].Value = "グループ分割数";
            dataGridView1[0, 2].Value = "スキャン日(1枚目)";
            dataGridView1[0, 3].Value = "";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog f = new FolderBrowserDialog())
            {
                f.ShowNewFolderButton = false;
                var res = f.ShowDialog();
                if (res != System.Windows.Forms.DialogResult.OK) return;
                textBox1.Text = f.SelectedPath;
            }

            //情報収集
            var fileList = GetFileList(textBox1.Text);

            if (fileList.Count > 0)
            {
                int groupCount = fileList.Count / 200;
                if (fileList.Count % 200 > 0) groupCount++;
                dataGridView1[1, 0].Value = fileList.Count.ToString();
                dataGridView1[1, 1].Value = groupCount.ToString();
                dataGridView1[1, 2].Value = System.IO.File.GetLastWriteTime(fileList[0]).ToString("yyyy/MM/dd");
                button2.Enabled = true;
            }
            else
            {
                dataGridView1[1, 0].Value = 0;
                dataGridView1[1, 1].Value = 0;
                dataGridView1[1, 2].Value = "";
                button2.Enabled = false;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary> スキャン実行関数の引数構造体クラス　</summary>
        class ScanParams
        {
            public int billmonth;
            public string scanDir;
        }


        /// <summary> 取込開始 </summary>
        private void button2_Click(object sender, EventArgs e)
        {
            //審査月確認
            int byear;
            int bmonth;
            int.TryParse(textBox2.Text, out byear);
            int.TryParse(textBox3.Text, out bmonth);

            if (byear <= 0 || bmonth <= 0 || byear > 80 || bmonth > 12)
            {
                MessageBox.Show("年、または月の指定を確認してください",
                    "審査月指定エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            var res = MessageBox.Show("取り込みを開始します。審査月の設定は間違いないかもう一度確認してください。" +
                "\r\n\r\n指定審査月:　" + byear.ToString() + "年" + bmonth.ToString() + "月",
                "取り込み確認",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Information);

            if (res != System.Windows.Forms.DialogResult.OK) return;

            button2.Enabled = false;
            button3.Enabled = false;
            
            var p = new ScanParams();
            p.billmonth = byear * 100 + bmonth;            
            p.scanDir = textBox1.Text.Trim();

            backgroundWorker1.RunWorkerAsync(p);
        }


     

        /// <summary> 取り込み実行 （バックグラウンド側で実行する）</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            backgroundWorker1.ReportProgress(0, "");
            e.Result = "";
            var p = e.Argument as ScanParams;
            if (p == null)
            {
                e.Result = "パラメーターエラー１";
                return;
            }

     
            //取り込みファイル確認
            var fileList = GetFileList(p.scanDir);
            fileList.Sort();

            //各種データ
            int scandate = DateTimeEx.DateTimeToInt(System.IO.File.GetLastWriteTime(fileList[0]));
            int groupCount = fileList.Count / 200;
            if (fileList.Count % 200 > 0) groupCount++;
            
            // スキャン情報データベース登録
            // sid（シリアル値）　・　scandate（タイムスタンプ）　・　uid（ユーザID）　・　scancount（登録ファイル数：取り敢えずゼロ）　・　billmonth（審査月)
            int sid;
            using (var cmd = SQLClass.CreateCommand("INSERT INTO scans (scandate, uid, billmonth) VALUES (:scandate, :uid, :billmonth) RETURNING sid"))
            {
                cmd.Parameters.Add(":scandate", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add(":uid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add(":billmonth", NpgsqlTypes.NpgsqlDbType.Integer);

                int i = 0;
                cmd.Parameters[i++].Value = scandate;
                cmd.Parameters[i++].Value = Settings.InputUserID;
                cmd.Parameters[i++].Value = p.billmonth;

                using (var r = cmd.ExecuteReader())
                {
                    r.Read();
                    sid = Convert.ToInt32(r["sid"]);
                }
            }

            

            // 前回のグループID （同じ審査月）を取得          （初期値 ： billmonth * 10000）
            int gid;
            using (var cmd = SQLClass.CreateCommand("SELECT MAX(gid) FROM groups WHERE billmonth = :billmonth"))
            {
                cmd.Parameters.Add("billmonth", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = p.billmonth;
                var resmax = cmd.ExecuteScalar();
                gid = resmax == DBNull.Value ? p.billmonth * 10000 : (int)resmax;
            }
            
            //レセプトファイルナンバーの開始IDを取得  （初期値 ： billmonth * 1000000 + 連番　・　重複しないもの(前回の最大＋１)を探す！）
            long rid;       // レセプトファイルナンバー（ファイル名）
            using (var cmd = SQLClass.CreateCommand("SELECT MAX(rid) FROM receipts WHERE rid >= :min AND rid < :max"))
            {
                cmd.Parameters.Add("min", NpgsqlTypes.NpgsqlDbType.Bigint);
                cmd.Parameters.Add("max", NpgsqlTypes.NpgsqlDbType.Bigint);
                cmd.Parameters[0].Value = (long)p.billmonth * 1000000L;
                cmd.Parameters[1].Value = ((long)p.billmonth + 1L) * 1000000L;
                var resno = cmd.ExecuteScalar();
                rid = resno == DBNull.Value ? 　(long)p.billmonth * 1000000L : (long)resno + 1L;
            }

            
            // 画像のコピー・ファイル登録
            // gcm  ：画像グループ新規登録コマンド　　groups　　　gid, scandate：タイムスタンプ、imagecount：画像数・取り敢えずゼロ、 sid：スキャンID、　billmonth：審査月
            // rcmd ：画像登録コマンド                receipts　　rid,  gid
            //                                        scans       scancount++
            //                                        groups      imagecount+;
            using (var gcmd = SQLClass.CreateCommand("INSERT INTO groups (gid, scandate, sid, billmonth) VALUES(:gid, :scandate, :sid, :billmonth)"))
            using (var rcmd = SQLClass.CreateCommand("INSERT INTO receipts (rid, gid) VALUES (:rid, :gid);" +
                                                     "UPDATE scans  SET scancount = scancount + 1 WHERE sid = :sid;" +
                                                     "UPDATE groups SET imagecount = imagecount + 1 WHERE gid = :gid;"))
            {
                // コピー先のディレクトリ
                // rid : yyMM + 連番６桁、　　ディレクトリ：yyMM + 連番２桁　なので、　ディレクトリは rid / 10000 となる！
                string destDir = "";               

                gcmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);
                gcmd.Parameters.Add("scandate", NpgsqlTypes.NpgsqlDbType.Integer);
                gcmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                gcmd.Parameters.Add("billmonth", NpgsqlTypes.NpgsqlDbType.Integer);

                rcmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);
                rcmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);
                rcmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);

                gcmd.Parameters["scandate"].Value = scandate;
                gcmd.Parameters["sid"].Value = sid;
                gcmd.Parameters["billmonth"].Value = p.billmonth;
                rcmd.Parameters["sid"].Value = sid;


                for (int fileCounter = 0; fileCounter < fileList.Count; fileCounter++)
                {
                    // 初期状態 または、　10000ごとの時にディレクトリ作成を行う！
                    if ((rid % 10000) == 0 || destDir == "")
                    {
                        destDir = Settings.imagePath + "\\" + ((long)(rid / 10000L)).ToString();
                        try
                        {
                            System.IO.Directory.CreateDirectory(destDir);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                    if (fileCounter % 200 == 0)
                    {
                        // グループ登録 (200枚毎に登録する。)
                        gid++;
                        gcmd.Parameters["gid"].Value = gid;
                        rcmd.Parameters["gid"].Value = gid;
                        gcmd.ExecuteNonQuery();
                    }


                    //ファイル変換
                    if (!Settings.ImageChange(fileList[fileCounter], destDir + "\\" + rid.ToString() + ".gif"))
                    {
                        e.Result = string.Format("変換エラーが発生しました\r\n{0}\r\n変換成功{1}件、失敗{2}件",
                                                System.IO.Path.GetFileName(fileList[fileCounter]),
                                                fileCounter,
                                                fileList.Count - fileCounter);
                        return;
                    }
                    rcmd.Parameters["rid"].Value = rid;
                    rcmd.ExecuteNonQuery();

                    System.IO.File.Delete(fileList[fileCounter]);

                    rid++;

                    //プログレスバーを調整
                    backgroundWorker1.ReportProgress(100 * (fileCounter + 1) / fileList.Count,                            // Progress (Max：100%)
                                                     string.Format("{0} / {1}", fileCounter + 1, fileList.Count));     // テキスト表示
                }
            }
            
            e.Result = "変換成功";
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                progressBar1.Value = e.ProgressPercentage;
                label7.Text = e.UserState.ToString();
            }
            catch (Exception)
            {
            }
        }


        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(e.Result.ToString());
            Close();
        }

        private void ScanDataForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = backgroundWorker1.IsBusy;
        }
    }
}
