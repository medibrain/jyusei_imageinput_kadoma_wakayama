﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class OmissionCheckForm : Form
    {
        public OmissionCheckForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //チェック年月を取得
            int year;
            if(!int.TryParse(textBox1.Text,out year) || year<=0||year>100)
            {
                MessageBox.Show("指定年が間違っています");
                return;
            }
            year = year * 100;

            int month;
            if (!int.TryParse(textBox2.Text, out month) || month <= 0 || month > 12)
            {
                MessageBox.Show("指定月が間違っています");
                return;
            }
            year = year + month;
            year =DateTimeEx.YearMonthJtoAD_H(year);

            //チェック対象を確認
            string typeCode = "";
            if (checkBox1.Checked) typeCode = "9";

            if (checkBox2.Checked)
            {
                if (typeCode != "") typeCode += ", ";
                typeCode += "7 ,8";
            }

            if (typeCode == "")
            {
                MessageBox.Show("チェック対象となるレセプトの種類が選択されていません");
                return;
            }

            DataTable dt = new DataTable();
            //SQLを発行
            //SELECT tdata.* FROM tdata LEFT JOIN receipts ON tdata.id = receipts.outdata WHERE tdata.billmonth = 201110 AND typecode in (9) AND receipts.outdata IS NULL
            using (var cmd = SQLClass.CreateCommand("SELECT tdata.* FROM tdata LEFT JOIN receipts ON tdata.id = " +
                "receipts.outdata WHERE tdata.billmonth = :billmonth AND typecode in (" + typeCode + ") AND receipts.outdata IS NULL"))
            {
                cmd.Parameters.Add("billmonth", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["billmonth"].Value = year;

                using(var da =SQLClass.CreateDataAdapter(cmd))
                {
                    da.Fill(dt);
                }
            }

            dataGridView1.DataSource= dt;

            label4.Text = "漏れ件数:" + dt.Rows.Count.ToString();

            dataGridView1.Columns[0].HeaderText = "電算番号";
            dataGridView1.Columns[1].HeaderText = "性別";
            dataGridView1.Columns[2].HeaderText = "生年月日";
            dataGridView1.Columns[3].HeaderText = "被保険者番号";
            dataGridView1.Columns[4].HeaderText = "施術師コード";
            dataGridView1.Columns[5].HeaderText = "診療月";
            dataGridView1.Columns[6].HeaderText = "審査月";
            dataGridView1.Columns[7].HeaderText = "レセタイプ";
            dataGridView1.Columns[8].HeaderText = "請求額";
            dataGridView1.Columns[9].HeaderText = "状態コード";


            dataGridView1.Columns[0].Width = 180;
            dataGridView1.Columns[1].Width = 60;
            dataGridView1.Columns[2].Width = 70;
            dataGridView1.Columns[5].Width = 70;
            dataGridView1.Columns[6].Width = 70;
            dataGridView1.Columns[7].Width = 70;
            dataGridView1.Columns[8].Width = 70;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0)
            {
                MessageBox.Show("出力するデータがありません",
                    "出力エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            string fileName = "未スキャン一覧" + textBox1.Text.PadLeft(2, '0') + textBox2.Text.PadLeft(2, '0');
            if (checkBox1.Checked) fileName += "柔";
            if (checkBox2.Checked) fileName += "鍼灸";
            fileName += ".csv";

            //ファイル名指定
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                sfd.FileName = fileName;
                sfd.Filter = "CSVファイル|*.csv";

                var res = sfd.ShowDialog();
                if (res != System.Windows.Forms.DialogResult.OK) return;

                fileName = sfd.FileName;

            }

            //データ作成
            StringBuilder sb = new StringBuilder();
            sb.Append("電算番号,性別,生年月日,被保険者番号,施術師コード,診療月,審査月,レセタイプ,請求額,状態コード\r\n");
            var dt = (DataTable)dataGridView1.DataSource;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j <= 9; j++)
                {
                    sb.Append(dt.Rows[i][j].ToString() + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                sb.Append("\r\n");
            }


            //セーブ
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fileName, false, Encoding.GetEncoding("Shift_JIS")))
            {
                try
                {
                    sw.Write(sb.ToString());
                }
                catch(SyntaxErrorException er)
                {
                    MessageBox.Show("データの保存に失敗しました\r\n" + er.Message);
                    return;
                }
            }

            MessageBox.Show("出力が完了しました");

            //
        }
    }
}
