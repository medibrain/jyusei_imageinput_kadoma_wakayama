﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class InputLogForm : Form
    {
        public InputLogForm()
        {
            InitializeComponent();

            //一覧取得表示
            userListUpdate();
        }

        private void userListUpdate()
        {
            //一覧取得表示
            dataGridView1.DataSource = null;
            DataTable dt = new DataTable();

            using (var da = SQLClass.CreateDataAdapter("SELECT uid, uname, redate, valid FROM users"))
            {
                da.Fill(dt);
            }

            dataGridView1.DataSource = dt;

            dataGridView1.Columns[0].Width = 50;
            dataGridView1.Columns[1].Width = 100;
            dataGridView1.Columns[2].Width = 70;
            dataGridView1.Columns[3].Width = 40;
            dataGridView1.Columns[0].HeaderText = "ID";
            dataGridView1.Columns[1].HeaderText = "氏名";
            dataGridView1.Columns[2].HeaderText = "登録日";
            dataGridView1.Columns[3].HeaderText = "有効";
        }

        private void monthUpdate()
        {
            //入力月一覧を表示
            dataGridView1.DataSource = null;
            Dictionary<int, long> dic = new Dictionary<int, long>();

            using (var cmd = SQLClass.CreateCommand("SELECT inputdate, count(inputdate) FROM receipts  WHERE inputdate <> 0 GROUP BY inputdate ORDER BY inputdate"))
            using (var da = cmd.ExecuteReader())
            {
                while (da.Read())
                {
                    int m = da.GetInt32(0) / 100;
                    if (!dic.ContainsKey(m)) dic.Add(m, 0);
                    dic[m] += da.GetInt64(1);
                }
            }
            var lst = dic.ToList();
            dataGridView1.DataSource = lst;


            dataGridView1.Columns[0].Width = 100;
            dataGridView1.Columns[1].Width = 100;
            dataGridView1.Columns[0].HeaderText = "入力年月";
            dataGridView1.Columns[0].DefaultCellStyle.Format = "0000/00";
            dataGridView1.Columns[1].HeaderText = "入力総件数";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked) selectUserInfo();
            else if (radioButton2.Checked) selectMothInfo();
        }

        private void selectMothInfo()
        {
            if (dataGridView1.CurrentCell == null) return;
            if (dataGridView1.CurrentRow == null) return;
            int month = (int)dataGridView1[0, dataGridView1.CurrentRow.Index].Value;
            //month = month / 100;
            dataGridView2.DataSource = null;
            dataGridView2.Rows.Clear();
            dataGridView2.Columns.Clear();

            long normalCount = 0;
            long subCount = 0;
            long errorCount = 0;


            using (var cmd = SQLClass.CreateCommand("SELECT medimonth, count(inputdate) FROM receipts WHERE inputdate > :inputdatemin AND inputdate < :inputdatemax GROUP BY medimonth"))
            {
                cmd.Parameters.Add("inputdatemin", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add("inputdatemax", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = month * 100;
                cmd.Parameters[1].Value = (month + 1) * 100;

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        int m = dr.GetInt32(0);
                        if (m == 99) subCount += dr.GetInt64(1);
                        else if (m == 999) errorCount += dr.GetInt64(1);
                        else normalCount += dr.GetInt64(1);
                    }
                }
            }

            dataGridView2.Columns.Add("情報", "情報");
            dataGridView2.Columns.Add("入力数", "入力数");

            dataGridView2.Rows.Add(4);
            dataGridView2[0, 0].Value = "全入力数";
            dataGridView2[0, 1].Value = "レセプト数";
            dataGridView2[0, 2].Value = "続紙枚数";
            dataGridView2[0, 3].Value = "エラー枚数";
            dataGridView2[1, 0].Value = normalCount + subCount + errorCount; ;
            dataGridView2[1, 1].Value = normalCount;
            dataGridView2[1, 2].Value = subCount;
            dataGridView2[1, 3].Value = errorCount;
        }

        private void selectUserInfo()
        {
            if (dataGridView1.CurrentCell == null) return;
            dataGridView2.DataSource = null;
            dataGridView2.Rows.Clear();
            dataGridView2.Columns.Clear();

            int uid = (int)dataGridView1[0, dataGridView1.CurrentRow.Index].Value;

            DataTable dt = new DataTable();

            using (var cmd = SQLClass.CreateCommand("SELECT inputdate, COUNT (uid) FROM receipts WHERE uid =:uid GROUP BY inputdate"))
            {
                cmd.Parameters.Add("uid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = uid;
                using (var da = SQLClass.CreateDataAdapter(cmd))
                {
                    da.Fill(dt);
                }
            }
            dataGridView2.DataSource = dt;

            dataGridView2.Columns[0].HeaderText = "日付";
            dataGridView2.Columns[1].HeaderText = "入力数";
            dataGridView2.Columns[0].DefaultCellStyle.Format = "0000/00/00";
        }

        private void modeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!((RadioButton)sender).Checked) return;

            if (radioButton1.Checked) userListUpdate();
            else if (radioButton2.Checked) monthUpdate();
        }
    }
}
