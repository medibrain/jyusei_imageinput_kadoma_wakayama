﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class UserForm : Form
    {
        public UserForm()
        {
            InitializeComponent();

            if (!SQLClass.ConState())
            {
                MessageBox.Show("サーバーに接続されていません。設定を確認の上、プログラムを起動しなおしてください。");
                return;
            }

            //一覧取得表示
            userListUpdate();

            dataGridView1.Columns[0].Width = 50;

            //20190612134143 furukawa st ////////////////////////
            //フォントサイズ大きくしたので幅変更
            dataGridView1.Columns[1].Width = 150;
            dataGridView1.Columns[2].Width = 150;

            //dataGridView1.Columns[2].Width = 70;
            //20190612134143 furukawa ed ////////////////////////

            dataGridView1.Columns[3].Width = 40;

            dataGridView1.Columns[0].HeaderText = "ID";
            dataGridView1.Columns[1].HeaderText = "氏名";
            dataGridView1.Columns[2].HeaderText = "登録日";
            dataGridView1.Columns[3].HeaderText = "有効";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "")
            {
                MessageBox.Show("氏名を確認してください", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            using (var cmd = SQLClass.CreateCommand("INSERT INTO users ( uname, redate ) VALUES ( :name, :date )"))
            {
                cmd.Parameters.Add("name", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters.Add("date", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = textBox1.Text;
                cmd.Parameters[1].Value = DateTimeEx.NowDateInt();
                cmd.ExecuteNonQuery();
            }

            DataTable dt = new DataTable();
            using (var da = SQLClass.CreateDataAdapter("SELECT * FROM users"))
            {
                da.Fill(dt);
            }

            dataGridView1.DataSource = dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var dt = dataGridView1;

            if (dataGridView1.CurrentCell == null) return;

            int userID = (int)dt[0, dt.CurrentRow.Index].Value;
            string userName = (string)dt[1, dt.CurrentRow.Index].Value;

            var res = MessageBox.Show("ID: [" + userID.ToString() + "]   入力者名: [" + userName + "] でよろしいですか？",
                "入力者確認",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (res != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            //変わった場合、入力枚数をリセット
            //if (Settings.InputUserID != userID)
            //{
            //    Settings.TodayInputCount = 0;
            //}

            //当日入力枚数を取得
            using (var cmd = SQLClass.CreateCommand("SELECT COUNT (uid) FROM receipts WHERE uid = :uid AND inputdate = :inputdate"))
            {
                cmd.Parameters.Add("uid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add("inputdate", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = userID;
                cmd.Parameters[1].Value = DateTimeEx.NowDateInt();

                Settings.TodayInputCount = (int)(long)cmd.ExecuteScalar();
            }

            Settings.InputUserID = userID;
            Settings.InputUserName = userName;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            userListUpdate();
        }

        private void userListUpdate()
        {
            //一覧取得表示
            DataTable dt = new DataTable();
            if (checkBox1.Checked)
            {
                using (var da = SQLClass.CreateDataAdapter("SELECT uid, uname, redate, valid FROM users"))
                {
                    da.Fill(dt);
                }
            }
            else
            {
                using (var da = SQLClass.CreateDataAdapter("SELECT uid, uname, redate, valid FROM users WHERE valid = TRUE"))
                {
                    da.Fill(dt);
                }
            }
            dataGridView1.DataSource = dt;
        }

        private void 無効化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dt = dataGridView1;
            if (dt.CurrentCell == null) return;
            int uid = (int)dt[0, dt.CurrentRow.Index].Value;
            string userName = (string)dt[1, dt.CurrentRow.Index].Value;

            var res = MessageBox.Show("ID: [" + uid.ToString() + "]   入力者名: [" + userName + "] を無効化します。よろしいですか？",
                "入力者確認",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (res != System.Windows.Forms.DialogResult.Yes) return;

            using (var cmd = SQLClass.CreateCommand("UPDATE users SET valid = FALSE WHERE uid = :uid"))
            {
                cmd.Parameters.Add("uid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = uid;
                cmd.ExecuteNonQuery();
            }

            userListUpdate();
        }

        private void 有効化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dt = dataGridView1;
            if (dt.CurrentCell == null) return;
            int uid = (int)dt[0, dt.CurrentRow.Index].Value;
            string userName = (string)dt[1, dt.CurrentRow.Index].Value;

            var res = MessageBox.Show("ID: [" + uid.ToString() + "]   入力者名: [" + userName + "] を有効化します。よろしいですか？",
                "入力者確認",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (res != System.Windows.Forms.DialogResult.Yes) return;

            using (var cmd = SQLClass.CreateCommand("UPDATE users SET valid = TRUE WHERE uid = :uid"))
            {
                cmd.Parameters.Add("uid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = uid;
                cmd.ExecuteNonQuery();
            }

            userListUpdate();


        }
    }
}
