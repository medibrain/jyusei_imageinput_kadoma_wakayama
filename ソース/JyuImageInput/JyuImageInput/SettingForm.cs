﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class SettingForm : Form
    {
        public SettingForm()
        {
            InitializeComponent();
            Settings.load();
            if (Settings.numberLength <= 0) Settings.numberLength = 1;
            textBox1.Text = Settings.DBName;
            textBox2.Text = Settings.DBuserID;
            textBox3.Text = Settings.DBuserPassword;
            textBox4.Text = Settings.imagePath;
            numericUpDown1.Value = Settings.numberLength;

            //20190624213854 furukawa st ////////////////////////
            //gifフラグ、gifサイズ追加
            
            chkGif.Checked = Settings.flgConvGif;
            nmSize.Value = Settings.intGifConvSize <= 200 ? 200 : Settings.intGifConvSize;
            //20190624213854 furukawa ed ////////////////////////
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPath_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog f = new FolderBrowserDialog())
            {
                //f.SelectedPath = Settings.imagePath;
                var res = f.ShowDialog();
                if (res != System.Windows.Forms.DialogResult.OK) return;

                textBox4.Text = f.SelectedPath;
            }
        }

        private void btnEnable_Click(object sender, EventArgs e)
        {
            if (textBox1.ReadOnly)
            {
                var res = MessageBox.Show("通常、変更の必要はありません。編集しますか？",
                    "編集確認",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Asterisk);
                if (res != System.Windows.Forms.DialogResult.Yes) return;

                textBox1.ReadOnly = false;
                textBox2.ReadOnly = false;
                textBox3.ReadOnly = false;
                textBox4.ReadOnly = false;
                numericUpDown1.ReadOnly = false;
                numericUpDown1.Increment = 1;               
                btnPath.Enabled = true;

                btnCancel.Enabled = true;

                //20190624213110 furukawa st ////////////////////////
                //gifチェックボックス使用可
                gbGif.Enabled = true;
                chkGif.Enabled = true;


                //20191021135942 furukawa st ////////////////////////
                //圧縮サイズの微調整のため増減値を10単位にした
                
                //nmSize.Increment = 100;
                nmSize.Increment = 10;
                //20191021135942 furukawa ed ////////////////////////


                //20190624213110 furukawa ed ////////////////////////


                btnEnable.Text = "修正確定";
            }
            else
            {
                var res = MessageBox.Show("設定後、アプリケーションが直ちに終了します。また正常に機能しなくなる恐れがありますが、よろしいですか？",
                    "編集確認",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Asterisk);
                if (res != System.Windows.Forms.DialogResult.Yes) return;

                res = MessageBox.Show("設定後、アプリケーションが直ちに終了します。また正常に機能しなくなる恐れがあります。本当に編集を確定しますか？",
                    "編集確認",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Asterisk);
                if (res != System.Windows.Forms.DialogResult.Yes) return;

                Settings.DBName = textBox1.Text;
                Settings.DBuserID = textBox2.Text;
                Settings.DBuserPassword = textBox3.Text;
                Settings.imagePath = textBox4.Text;
                Settings.numberLength = (int)numericUpDown1.Value;

                //20190624213246 furukawa st ////////////////////////
                //gifチェックボックス、gifサイズ保存
                
                Settings.flgConvGif = chkGif.Checked;
                Settings.intGifConvSize = (int)nmSize.Value;
                //20190624213246 furukawa ed ////////////////////////


                Settings.save();
                MessageBox.Show("設定を更新しました。アプリケーションを終了します。");
                Application.Restart();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            textBox1.Text = Settings.DBName;
            textBox2.Text = Settings.DBuserID;
            textBox3.Text = Settings.DBuserPassword;
            textBox4.Text = Settings.imagePath;
            numericUpDown1.Value = Settings.numberLength;
            textBox1.ReadOnly = true;
            textBox2.ReadOnly = true;
            textBox3.ReadOnly = true;
            textBox4.ReadOnly = true;
            numericUpDown1.ReadOnly = true;
            numericUpDown1.Increment = 0;
            btnPath.Enabled = false;

            //20190624213550 furukawa st ////////////////////////
            //gifチェックボックス、gifサイズ参照
            
            chkGif.Checked = Settings.flgConvGif;
            nmSize.Value = Settings.intGifConvSize <=200 ? 200: Settings.intGifConvSize;
            chkGif.Enabled = false;
            nmSize.ReadOnly = true;
            nmSize.Increment = 0;
            //20190624213550 furukawa ed ////////////////////////



            btnEnable.Text = "修正";
            btnEnable.Enabled = false;
            btnCancel.Enabled = false;
        }

        //20190624213616 furukawa st ////////////////////////
        //gifチェックボックスイベント
        
        private void chkGif_CheckedChanged(object sender, EventArgs e)
        {
            nmSize.Enabled = chkGif.Checked;
        }
        //20190624213616 furukawa ed ////////////////////////

    }

}
