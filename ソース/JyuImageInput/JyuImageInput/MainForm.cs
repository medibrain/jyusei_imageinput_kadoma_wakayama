﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class MainForm : Form
    {
        #region 宣言
        private enum INPUT_MODE { 通常, 部位1, 部位2, 日付1, 日付2 }

        INPUT_MODE inputMode = INPUT_MODE.通常;

        /// <summary>
        /// 入力に変更があったか
        /// </summary>
        bool changeFlag = false;
        /// <summary>
        /// 現在のレセプトが、未入力だったか
        /// </summary>
        bool nullReceipt;

        Npgsql.NpgsqlCommand upCmd;
        Npgsql.NpgsqlCommand seCmd;

        /// <summary>
        /// グループの入力済み枚数を記入するコマンド
        /// </summary>
        Npgsql.NpgsqlCommand groupCountUpdateCmd;

        /// <summary>
        /// グループの開始日を登録するコマンド
        /// </summary>
        Npgsql.NpgsqlCommand groupStartUpdateCmd;

        /// <summary>
        /// グループの終了日を登録するコマンド
        /// </summary>
        Npgsql.NpgsqlCommand groupEndUpdateCmd;

        /// <summary>
        /// 入力済み件数取得コマンド
        /// </summary>
        Npgsql.NpgsqlCommand groupCountSelectCmd;

        /// <summary>
        /// 部位取得コマンド
        /// </summary>
        Npgsql.NpgsqlCommand partsSelectCmd;

        /// <summary>
        /// 部位インサートコマンド
        /// </summary>
        Npgsql.NpgsqlCommand partsInsertCmd;

        /// <summary>
        /// 部位アップデートコマンド
        /// </summary>
        Npgsql.NpgsqlCommand partsUpdateCmd;

        /// <summary>
        /// 部位アップデートコマンド2
        /// </summary>
        Npgsql.NpgsqlCommand partsUpdateCmd2;
        #endregion


        #region コンストラクタ
        public MainForm()
        {
            InitializeComponent();

            
#if WAKA_KOIKI  
            Text += " (和歌山広域連合用)";
#elif KADOMA_CITY
            Text += " (門真市役所用)";
#endif

            Settings.load();

            if (!SQLClass.ConState())
            {
                MessageBox.Show("サーバーとの接続で問題が発生しました。サーバーの状態、もしくは設定を確認してください。");
                return;
            }

            upCmd = SQLClass.CreateCommand("UPDATE receipts SET medimonth = :medimonth, insuredno = :insuredno, totalpoint = :totalpoint, inputdate = :inputdate, uid = :uid WHERE rid = :rid");
            upCmd.Parameters.Add("medimonth", NpgsqlTypes.NpgsqlDbType.Integer);
            upCmd.Parameters.Add("insuredno", NpgsqlTypes.NpgsqlDbType.Text);
            upCmd.Parameters.Add("totalpoint", NpgsqlTypes.NpgsqlDbType.Integer);
            upCmd.Parameters.Add("inputdate", NpgsqlTypes.NpgsqlDbType.Integer);
            upCmd.Parameters.Add("uid", NpgsqlTypes.NpgsqlDbType.Integer);
            upCmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);

            seCmd = SQLClass.CreateCommand("SELECT medimonth, insuredno, totalpoint, inputdate FROM receipts WHERE rid = :rid");
            seCmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);

            groupCountUpdateCmd = SQLClass.CreateCommand("UPDATE groups SET inputcount = :inputcount WHERE gid = :gid");
            groupCountUpdateCmd.Parameters.Add("inputcount", NpgsqlTypes.NpgsqlDbType.Integer);
            groupCountUpdateCmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);

            groupStartUpdateCmd = SQLClass.CreateCommand("UPDATE groups SET startdate = :startdate WHERE gid = :gid");
            groupStartUpdateCmd.Parameters.Add("startdate", NpgsqlTypes.NpgsqlDbType.Integer);
            groupStartUpdateCmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);

            groupEndUpdateCmd = SQLClass.CreateCommand("UPDATE groups SET enddate = :enddate WHERE gid = :gid");
            groupEndUpdateCmd.Parameters.Add("enddate", NpgsqlTypes.NpgsqlDbType.Integer);
            groupEndUpdateCmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);

            groupCountSelectCmd = SQLClass.CreateCommand("SELECT COUNT (gid) FROM receipts WHERE gid = :gid AND inputdate > 0");
            groupCountSelectCmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);

            partsSelectCmd = SQLClass.CreateCommand("SELECT rid, name1, name2, name3, name4, name5, name6, name7, name8, name9, name10, s1, s2, e1, e2 FROM parts WHERE rid = :rid");
            partsSelectCmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);

            partsUpdateCmd = SQLClass.CreateCommand("UPDATE parts SET name1 = :name1, name2 = :name2, name3 = :name3, name4 = :name4, name5 = :name5, s1 = :s1, e1 = :e1 WHERE rid =:rid");
            partsUpdateCmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);
            partsUpdateCmd.Parameters.Add("name1", NpgsqlTypes.NpgsqlDbType.Text);
            partsUpdateCmd.Parameters.Add("name2", NpgsqlTypes.NpgsqlDbType.Text);
            partsUpdateCmd.Parameters.Add("name3", NpgsqlTypes.NpgsqlDbType.Text);
            partsUpdateCmd.Parameters.Add("name4", NpgsqlTypes.NpgsqlDbType.Text);
            partsUpdateCmd.Parameters.Add("name5", NpgsqlTypes.NpgsqlDbType.Text);
            partsUpdateCmd.Parameters.Add("s1", NpgsqlTypes.NpgsqlDbType.Integer);
            partsUpdateCmd.Parameters.Add("e1", NpgsqlTypes.NpgsqlDbType.Integer);

            partsInsertCmd = SQLClass.CreateCommand("INSERT INTO parts (rid, name1, name2, name3, name4, name5, name6, name7, name8, name9, name10, s1, s2, e1, e2) VALUES (:rid, :name1, :name2, :name3, :name4, :name5, :name6, :name7, :name8, :name9, :name10, :s1, :s2, :e1, :e2)");
            partsInsertCmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);
            partsInsertCmd.Parameters.Add("name1", NpgsqlTypes.NpgsqlDbType.Text);
            partsInsertCmd.Parameters.Add("name2", NpgsqlTypes.NpgsqlDbType.Text);
            partsInsertCmd.Parameters.Add("name3", NpgsqlTypes.NpgsqlDbType.Text);
            partsInsertCmd.Parameters.Add("name4", NpgsqlTypes.NpgsqlDbType.Text);
            partsInsertCmd.Parameters.Add("name5", NpgsqlTypes.NpgsqlDbType.Text);
            partsInsertCmd.Parameters.Add("name6", NpgsqlTypes.NpgsqlDbType.Text);
            partsInsertCmd.Parameters.Add("name7", NpgsqlTypes.NpgsqlDbType.Text);
            partsInsertCmd.Parameters.Add("name8", NpgsqlTypes.NpgsqlDbType.Text);
            partsInsertCmd.Parameters.Add("name9", NpgsqlTypes.NpgsqlDbType.Text);
            partsInsertCmd.Parameters.Add("name10", NpgsqlTypes.NpgsqlDbType.Text);
            partsInsertCmd.Parameters.Add("s1", NpgsqlTypes.NpgsqlDbType.Integer);
            partsInsertCmd.Parameters.Add("s2", NpgsqlTypes.NpgsqlDbType.Integer);
            partsInsertCmd.Parameters.Add("e1", NpgsqlTypes.NpgsqlDbType.Integer);
            partsInsertCmd.Parameters.Add("e2", NpgsqlTypes.NpgsqlDbType.Integer);


            partsUpdateCmd2 = SQLClass.CreateCommand("UPDATE parts SET name6 = :name6, name7 = :name7, name8 = :name8, name9 = :name9, name10 = :name10, s2 = :s2, e2 = :e2 WHERE rid =:rid");
            partsUpdateCmd2.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);
            partsUpdateCmd2.Parameters.Add("name6", NpgsqlTypes.NpgsqlDbType.Text);
            partsUpdateCmd2.Parameters.Add("name7", NpgsqlTypes.NpgsqlDbType.Text);
            partsUpdateCmd2.Parameters.Add("name8", NpgsqlTypes.NpgsqlDbType.Text);
            partsUpdateCmd2.Parameters.Add("name9", NpgsqlTypes.NpgsqlDbType.Text);
            partsUpdateCmd2.Parameters.Add("name10", NpgsqlTypes.NpgsqlDbType.Text);
            partsUpdateCmd2.Parameters.Add("s2", NpgsqlTypes.NpgsqlDbType.Integer);
            partsUpdateCmd2.Parameters.Add("e2", NpgsqlTypes.NpgsqlDbType.Integer);
        }
        #endregion

        private bool userSelect()
        {
            using (UserForm f = new UserForm())
            {
                f.ShowDialog();
            }

            if (Settings.InputUserID == -1)
            {
                MessageBox.Show("入力者が設定されていないため、入力できません。",
                    "入力者エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);

                txtHihoNum.ReadOnly = true;
                txtSinryoYM.ReadOnly = true;
                txtSum.ReadOnly = true;

                return false;
            }
            else
            {
                textBox4.Text = "ID:" + Settings.InputUserID.ToString() + "   " + Settings.InputUserName;
                textBox5.Text = Settings.TodayInputCount.ToString();
                return true;
            }
        }

        private void panel2_SizeChanged(object sender, EventArgs e)
        {
            ImageReset();
        }

        /// <summary>
        /// 表示モード
        /// </summary>
        private enum ImageMode { WidthHeightBase, OriginalBase, PanelBase, FreeRatio };
        private ImageMode imageMode = ImageMode.PanelBase;

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ImageShowSettingChange();
            ImageReset();
        }

        #region 画像関連
        private void ImageReset()
        {
            if (pictureBox1.Image == null) return;
            if (imageMode == ImageMode.WidthHeightBase)
            {
                //幅いっぱい、もしくは高さいっぱいにする
                float wf = (float)(panel2.Width - SystemInformation.HorizontalScrollBarHeight) / (float)pictureBox1.Image.Width;
                float wh = (float)(panel2.Height - SystemInformation.VerticalScrollBarWidth) / (float)pictureBox1.Image.Height;

                float ratio;
                ratio = wf < wh ? wh : wf;

                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox1.Dock = DockStyle.None;
                pictureBox1.Height = (int)((float)pictureBox1.Image.Height * ratio);
                pictureBox1.Width = (int)((float)pictureBox1.Image.Width * ratio);
            }
            else if (imageMode == ImageMode.PanelBase)
            {
                //全面表示にする
                pictureBox1.Dock = DockStyle.None;
                pictureBox1.Location = new Point(0, 0);
                pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;

                imageMode = ImageMode.PanelBase;
            }
            else if (imageMode == ImageMode.OriginalBase)
            {
                //等倍表示にする
                pictureBox1.Dock = DockStyle.Fill;
                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            }
            else
            {
                //任意の倍率

            }
        }

        private void ImageShowSettingChange()
        {
            if (imageMode == ImageMode.OriginalBase)
            {
                //幅いっぱい、もしくは高さいっぱいにする
                imageMode = ImageMode.WidthHeightBase;
            }
            else if (imageMode == ImageMode.WidthHeightBase)
            {
                //全面表示にする
                imageMode = ImageMode.PanelBase;
            }
            else
            {
                //等倍表示にする
                imageMode = ImageMode.OriginalBase;
            }
        }
        #endregion
        
        #region フォームイベント
        private void MainForm_Shown(object sender, EventArgs e)
        {
            if (!SQLClass.ConState()) return;

            if (!userSelect()) return;

            using (SelectGroupForm f = new SelectGroupForm())
            {
                f.ShowDialog();
            }

            label7.Text = "グループID：　" + Settings.GroupID.ToString();
            label8.Text = "枚数：　" + Settings.RIDListIndex.ToString() + "/" + Settings.RIDList.Count.ToString();

            if (Settings.RIDList.Count == 0) return;
            receiptChange(Settings.RIDList[Settings.RIDListIndex]);
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            groupUpdate();
        }

        #endregion



        private bool inputEnabled { get; set; }

        private void inputEnabledTrue()
        {
            inputEnabled = true;
            txtHihoNum.ReadOnly = false;
            txtSinryoYM.ReadOnly = false;
            txtSum.ReadOnly = false;
        }
        private void inputEnabledFalse()
        {
            inputEnabled = false;
            txtHihoNum.ReadOnly = true;
            txtSinryoYM.ReadOnly = true;
            txtSum.ReadOnly = true;
        }

        private void inputTextBox_TextChanged(object sender, EventArgs e)
        {
            changeFlag = true;
        }

        /// <summary>
        /// 表示レセプトの変更
        /// </summary>
        /// <param name="receiptID"></param>
        private void receiptChange(long receiptID)
        {
            if (Settings.RIDList.Count == 0) return;

            seCmd.Parameters[0].Value = receiptID;
            using (var dr = seCmd.ExecuteReader())
            {
                dr.Read();
                object[] obj = new object[dr.FieldCount];
                dr.GetValues(obj);
                txtSinryoYM.Text = (int)dr[0] == 0 ? "" : dr[0].ToString();
                txtHihoNum.Text = dr[1].ToString();
                txtSum.Text = (int)dr[2] == 0 ? "" : dr[2].ToString();
                textBox7.Text = (int)dr[3] == 0 ? "" : ((int)dr[3]).ToString("0000/00/00");

                nullReceipt = (int)dr[3] == 0 ? true : false;
            }

            //続紙等の場合 code99
            if (txtSinryoYM.Text == "99") txtSinryoYM.Text = "---";

            //不明、エラー等 code999
            if (txtSinryoYM.Text == "999") txtSinryoYM.Text = "*****";

            //画像表示
            //20190613114836 furukawa st ////////////////////////
            //billstrの桁数を変更（令和1年は前ゼロなくなるので）
            
            string billstr = (receiptID / 1000000).ToString();
                        //string billstr = (receiptID / 1000000).ToString("0000");
            //20190613114836 furukawa ed ////////////////////////

            string floderNo = ((receiptID / 10000) % 100).ToString("00");
            string ridStr = (receiptID % 1000000).ToString("000000");

            string imgstr = Settings.imagePath + "\\" + billstr + floderNo + "\\" + receiptID.ToString();

            //画像があるか確認 gif jpg 両対応
            if (System.IO.File.Exists(imgstr + ".gif")) imgstr += ".gif";
            else if (System.IO.File.Exists(imgstr + ".jpg")) imgstr += ".jpg";
            else imgstr = "def.gif";

            pictureBox1.Image = Image.FromFile(imgstr);

            //部位表示
            partsSelect(receiptID);

            Settings.ReceiptID = receiptID;
            textBox6.Text = receiptID.ToString();
            label8.Text = "枚数：　" + (Settings.RIDListIndex + 1).ToString() + "/" + Settings.RIDList.Count.ToString();


            changeFlag = false;
        }

        /// <summary>
        /// 現在表示されているレセプトを書き込みます
        /// </summary>
        /// <returns>レセプトを移行していいか</returns>
        private bool receptWrite()
        {
            if (Settings.RIDList.Count == 0) return false;

            string mediMonthStr = txtSinryoYM.Text.Trim();
            string insuredNoStr = txtHihoNum.Text.Trim();
            string totalPointStr = txtSum.Text.Trim();
            int mediMonth = 0;
            int totalPoint = 0;
            bool errorFlag = false;
            string errorText = "";

            //値の検証
            //20190614142002 furukawa st ////////////////////////
            //診療月長さチェック変更（令和1年は前ゼロなくなるので）
            
            if (mediMonthStr.Length <= 2 || mediMonthStr.Length >= 5 || !int.TryParse(mediMonthStr, out mediMonth) || (mediMonth % 100) > 12)
                    //if (mediMonthStr.Length != 4 || !int.TryParse(mediMonthStr, out mediMonth) || (mediMonth % 100) > 12)
            //20190614142002 furukawa ed ////////////////////////
            {
                errorFlag = true;
                errorText += "診療月 ";
            }
            if (insuredNoStr.Length != Settings.numberLength)
            {
                errorFlag = true;
                errorText += "被保番 ";
            }
            if (totalPointStr.Length < 2 || !int.TryParse(totalPointStr, out totalPoint))
            {
                errorFlag = true;
                errorText += "合計金額 ";
            }
            if (mediMonthStr == "---")
            {
                mediMonth = 99;
                errorFlag = false;
            }
            if (mediMonthStr == "*****")
            {
                mediMonth = 999;
                errorFlag = false;
            }

            //開始日、終了日のチェック
            if (inputMode == INPUT_MODE.日付1 || inputMode == INPUT_MODE.日付2)
            {
                int dayTemp;
                if (textBox14.Text.Trim() == string.Empty || int.TryParse(textBox14.Text.Trim(), out dayTemp))
                {
                    errorFlag = true;
                    errorText += "開始日 ";
                }
                if (textBox15.Text.Trim() == string.Empty || int.TryParse(textBox15.Text.Trim(), out dayTemp))
                {
                    errorFlag = true;
                    errorText += "終了日 ";
                }
            }

            if (errorFlag)
            {
                var res = MessageBox.Show(errorText + "の入力に誤りがあります。入力は記録されませんが、よろしいですか？",
                    "入力エラー", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (res == System.Windows.Forms.DialogResult.No)
                {
                    txtSinryoYM.Focus();
                    return false;
                }
                else
                {
                    txtSinryoYM.Focus();
                    return true;
                }
            }

            //基本情報
            if (inputMode == INPUT_MODE.通常 || inputMode == INPUT_MODE.部位1)
            {
                upCmd.Parameters[0].Value = mediMonth;
                upCmd.Parameters[1].Value = insuredNoStr;
                upCmd.Parameters[2].Value = totalPoint;
                upCmd.Parameters[3].Value = DateTimeEx.NowDateInt();
                upCmd.Parameters[4].Value = Settings.InputUserID;
                upCmd.Parameters[5].Value = Settings.ReceiptID;

                try
                {
                    upCmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    MessageBox.Show("更新できませんでした" + e.Message);
                    return false;
                }

                if (nullReceipt)
                {
                    Settings.GroupInputCount++;
                    Settings.TodayInputCount++;
                    textBox5.Text = Settings.TodayInputCount.ToString();
                }

                //開始日が記入されていない場合
                if (Settings.GroupStartDate == 0)
                {
                    Settings.GroupStartDate = DateTimeEx.NowDateInt();
                    groupStartUpdateCmd.Parameters[0].Value = DateTimeEx.NowDateInt();
                    groupStartUpdateCmd.Parameters[1].Value = Settings.GroupID;
                    try
                    {
                        groupStartUpdateCmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("正常に更新できませんでした" + e.Message);
                    }
                }
            }

            //部位処理
            if (inputMode != INPUT_MODE.通常) partsWrite();

            txtSinryoYM.Focus();
            return true;

        }



        #region ボタンイベント

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Settings.RIDList.Count == 0) return;

            //現在の情報を書き込む
            if (changeFlag)
            {
                if (!receptWrite()) return;
            }

            //インデックスを1つ進める
            if (Settings.RIDListIndex < Settings.RIDList.Count - 1) Settings.RIDListIndex++;
            long rid = Settings.RIDList[Settings.RIDListIndex];

            //画像表示
            receiptChange(rid);

            if (inputMode == INPUT_MODE.部位2) textBox8.Focus();
            else if (inputMode == INPUT_MODE.日付1 || inputMode == INPUT_MODE.日付2) textBox14.Focus();


            //20190612132441 furukawa st ////////////////////////
            //MainForm_KeyUpでやるので外す

                    else txtSinryoYM.Focus();
            //20190612132441 furukawa ed ////////////////////////
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            if (Settings.RIDList.Count == 0) return;

            //現在の情報を書き込む
            if (changeFlag)
            {
                if (!receptWrite()) return;
            }

            //インデックスを1つ進める
            if (Settings.RIDListIndex != 0) Settings.RIDListIndex--;
            long rid = Settings.RIDList[Settings.RIDListIndex];

            //画像表示
            receiptChange(rid);

            txtSinryoYM.Focus();
        }


        private void button4_Click(object sender, EventArgs e)
        {
            userSelect();
        }


        private void button3_Click(object sender, EventArgs e)
        {
            groupUpdate();

            using (SelectGroupForm f = new SelectGroupForm())
            {
                f.ShowDialog();
            }
            if (Settings.RIDList.Count == 0) return;
            receiptChange(Settings.RIDList[Settings.RIDListIndex]);

            label7.Text = "グループID：　" + Settings.GroupID.ToString();
            label8.Text = "枚数：　" + Settings.RIDListIndex.ToString() + "/" + Settings.RIDList.Count.ToString();
        }



        private void button6_Click(object sender, EventArgs e)
        {
            if (Settings.RIDList.Count == 0) return;

            //現在の情報を書き込む
            if (changeFlag)
            {
                if (!receptWrite()) return;
            }

            //インデックス調整
            if (Settings.RIDListIndex - 10 > 0)
            {
                Settings.RIDListIndex -= 10;
            }
            else
            {
                Settings.RIDListIndex = 0;
            }
            long rid = Settings.RIDList[Settings.RIDListIndex];

            //画像表示
            receiptChange(rid);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (Settings.RIDList.Count == 0) return;

            //現在の情報を書き込む
            if (changeFlag)
            {
                if (!receptWrite()) return;
            }

            //インデックス調整
            if (Settings.RIDListIndex + 10 < Settings.RIDList.Count)
            {
                Settings.RIDListIndex += 10;
            }
            else
            {
                Settings.RIDListIndex = Settings.RIDList.Count - 1;
            }
            long rid = Settings.RIDList[Settings.RIDListIndex];

            //画像表示
            receiptChange(rid);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (Settings.RIDList.Count == 0) return;

            //現在の情報を書き込む
            if (changeFlag)
            {
                if (!receptWrite()) return;
            }

            //インデックス調整
            if (Settings.RIDListIndex + 50 < Settings.RIDList.Count)
            {
                Settings.RIDListIndex += 50;
            }
            else
            {
                Settings.RIDListIndex = Settings.RIDList.Count - 1;
            }
            long rid = Settings.RIDList[Settings.RIDListIndex];

            //画像表示
            receiptChange(rid);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (Settings.RIDList.Count == 0) return;

            //現在の情報を書き込む
            if (changeFlag)
            {
                if (!receptWrite()) return;
            }

            //インデックス調整
            if (Settings.RIDListIndex - 50 > 0)
            {
                Settings.RIDListIndex -= 50;
            }
            else
            {
                Settings.RIDListIndex = 0;
            }
            long rid = Settings.RIDList[Settings.RIDListIndex];

            //画像表示
            receiptChange(rid);
        }

        #endregion

        /// <summary>
        /// グループ情報を更新します
        /// </summary>
        private void groupUpdate()
        {
            groupCountSelectCmd.Parameters[0].Value = Settings.GroupID;

            int gCount = (int)(long)groupCountSelectCmd.ExecuteScalar();
            groupCountUpdateCmd.Parameters[0].Value = gCount;
            groupCountUpdateCmd.Parameters[1].Value = Settings.GroupID;
            groupCountUpdateCmd.ExecuteNonQuery();

            if (gCount == Settings.RIDList.Count)
            {
                groupEndUpdateCmd.Parameters[0].Value = DateTimeEx.NowDateInt();
                groupEndUpdateCmd.Parameters[1].Value = Settings.GroupID;
                groupEndUpdateCmd.ExecuteNonQuery();
            }

            //部位情報更新
            DataTable dt = new DataTable();

            int p1 = 0;
            int p2 = 0;
            int d1 = 0;
            int d2 = 0;

            if (Settings.GroupID == 0) return;

            //部位の入力数,開始終了日付入力数を確認
            using (var cmd = SQLClass.CreateCommand("SELECT name1, name6, s1, s2 FROM receipts LEFT JOIN parts ON parts.rid = receipts.rid WHERE receipts.gid = :gid AND parts.rid IS NOT NULL"))
            {
                cmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["gid"].Value = Settings.GroupID;

                using (var da = SQLClass.CreateDataAdapter(cmd))
                {
                    da.Fill(dt);
                }
            }

            //テーブルから検索
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //１回目入力
                if ((string)dt.Rows[i][0] != string.Empty) p1++;
                if ((string)dt.Rows[i][1] != string.Empty) p2++;
                if ((int)dt.Rows[i][2] != 0) d1++;
                if ((int)dt.Rows[i][3] != 0) d2++;
            }

            //部位管理情報をグループデータに書き込み
            using (var cmd = SQLClass.CreateCommand("UPDATE groups SET parts1 = :parts1, parts2 = :parts2, days1 = :days1, days2 = :days2 WHERE gid = :gid"))
            {
                cmd.Parameters.Add("parts1", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add("parts2", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add("days1", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add("days2", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);

                cmd.Parameters["parts1"].Value = p1;
                cmd.Parameters["parts2"].Value = p2;
                cmd.Parameters["days1"].Value = d1;
                cmd.Parameters["days2"].Value = d2;
                cmd.Parameters["gid"].Value = Settings.GroupID;

                try
                {
                    var c = cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    MessageBox.Show("更新できませんでした\r\n", e.Message);
                }
            }
        }

        private void inputTextBox_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

      
  

        #region 部位情報を登録します
        /// <summary>
        /// 部位情報を登録します
        /// </summary>
        private void partsWrite()
        {
            bool newPartsInput = false;
            long rid = Settings.ReceiptID;
            bool first = (inputMode == INPUT_MODE.部位1 || inputMode == INPUT_MODE.日付1);

            partsSelectCmd.Parameters["rid"].Value = rid;

            //部位の順番をチェック　上から順番に入力してもらう
            bool emptyError = false;
            if (textBox8.Text.Trim() == string.Empty && textBox9.Text.Trim() != string.Empty) emptyError = true;
            if (textBox9.Text.Trim() == string.Empty && textBox10.Text.Trim() != string.Empty) emptyError = true;
            if (textBox10.Text.Trim() == string.Empty && textBox11.Text.Trim() != string.Empty) emptyError = true;
            if (textBox11.Text.Trim() == string.Empty && textBox12.Text.Trim() != string.Empty) emptyError = true;
            if (emptyError)
            {
                MessageBox.Show("部位は上から順番に埋めてください");
                return;
            }


            int sDate;
            int eDate;

            if (textBox14.Text == string.Empty) sDate = 0;
            else if (!int.TryParse(textBox14.Text, out sDate))
            {
                MessageBox.Show("開始日を指定してください");
                return;
            }
            if (textBox15.Text == string.Empty) eDate = 0;
            else if (!int.TryParse(textBox15.Text, out eDate))
            {
                MessageBox.Show("終了日を指定してください");
                return;
            }
            if (sDate > 31)
            {
                MessageBox.Show("開始日に誤りがあります");
                return;
            }
            if (eDate > 31)
            {
                MessageBox.Show("終了日に誤りがあります");
                return;
            }


            //すべての部位が空白の場合、delete
            bool deleteSwitch = false;
            if (textBox8.Text.Trim() == string.Empty)
            {
                //１回目２回目ともに未入力か確認
                using (var cmd = SQLClass.CreateCommand("SELECT name1, name6 FROM parts WHERE rid = :rid"))
                {
                    cmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);
                    cmd.Parameters["rid"].Value = rid;
                    try
                    {
                        using (var rd = cmd.ExecuteReader())
                        {
                            if (rd.HasRows)
                            {
                                rd.Read();
                                if (!first && rd.GetString(0) == string.Empty) deleteSwitch = true;
                                else if (first && rd.GetString(1) == string.Empty) deleteSwitch = true;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("部位データの更新に失敗しました\r\n" + e.Message);
                    }
                }

                using (var cmd = SQLClass.CreateCommand("DELETE FROM parts WHERE rid = :rid"))
                {
                    if (deleteSwitch)
                    {
                        cmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);
                        cmd.Parameters["rid"].Value = rid;
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("部位データの更新に失敗しました\r\n" + e.Message);
                        }
                        return;
                    }
                }
            }

            try
            {
                var res = partsSelectCmd.ExecuteScalar();
                if (res == null) newPartsInput = true;
            }
            catch (Exception e)
            {
                MessageBox.Show("部位の情報が操作できません", e.Message);
                return;
            }

            if (newPartsInput)
            {
                partsInsertCmd.Parameters["rid"].Value = rid;
                if (first)
                {
                    partsInsertCmd.Parameters["name1"].Value = partsNameAdjust(textBox8.Text);
                    partsInsertCmd.Parameters["name2"].Value = partsNameAdjust(textBox9.Text);
                    partsInsertCmd.Parameters["name3"].Value = partsNameAdjust(textBox10.Text);
                    partsInsertCmd.Parameters["name4"].Value = partsNameAdjust(textBox11.Text);
                    partsInsertCmd.Parameters["name5"].Value = partsNameAdjust(textBox12.Text);
                    partsInsertCmd.Parameters["name6"].Value = string.Empty;
                    partsInsertCmd.Parameters["name7"].Value = string.Empty;
                    partsInsertCmd.Parameters["name8"].Value = string.Empty;
                    partsInsertCmd.Parameters["name9"].Value = string.Empty;
                    partsInsertCmd.Parameters["name10"].Value = string.Empty;
                    partsInsertCmd.Parameters["s1"].Value = sDate;
                    partsInsertCmd.Parameters["s2"].Value = 0;
                    partsInsertCmd.Parameters["e1"].Value = eDate;
                    partsInsertCmd.Parameters["e2"].Value = 0;

                }
                else
                {
                    partsInsertCmd.Parameters["name1"].Value = string.Empty;
                    partsInsertCmd.Parameters["name2"].Value = string.Empty;
                    partsInsertCmd.Parameters["name3"].Value = string.Empty;
                    partsInsertCmd.Parameters["name4"].Value = string.Empty;
                    partsInsertCmd.Parameters["name5"].Value = string.Empty;
                    partsInsertCmd.Parameters["name6"].Value = partsNameAdjust(textBox8.Text);
                    partsInsertCmd.Parameters["name7"].Value = partsNameAdjust(textBox9.Text);
                    partsInsertCmd.Parameters["name8"].Value = partsNameAdjust(textBox10.Text);
                    partsInsertCmd.Parameters["name9"].Value = partsNameAdjust(textBox11.Text);
                    partsInsertCmd.Parameters["name10"].Value = partsNameAdjust(textBox12.Text);
                    partsInsertCmd.Parameters["s1"].Value = 0;
                    partsInsertCmd.Parameters["s2"].Value = sDate;
                    partsInsertCmd.Parameters["e1"].Value = 0;
                    partsInsertCmd.Parameters["e2"].Value = eDate;
                }
                try
                {
                    partsInsertCmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    MessageBox.Show("部位の登録に失敗しました" + e.Message);
                }
            }
            else
            {
                if (first)
                {
                    partsUpdateCmd.Parameters["rid"].Value = Settings.ReceiptID;
                    partsUpdateCmd.Parameters["name1"].Value = partsNameAdjust(textBox8.Text);
                    partsUpdateCmd.Parameters["name2"].Value = partsNameAdjust(textBox9.Text);
                    partsUpdateCmd.Parameters["name3"].Value = partsNameAdjust(textBox10.Text);
                    partsUpdateCmd.Parameters["name4"].Value = partsNameAdjust(textBox11.Text);
                    partsUpdateCmd.Parameters["name5"].Value = partsNameAdjust(textBox12.Text);
                    partsUpdateCmd.Parameters["s1"].Value = sDate;
                    partsUpdateCmd.Parameters["e1"].Value = eDate;
                    try
                    {
                        partsUpdateCmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("部位の更新に失敗しました\r\n" + e.Message);
                    }
                }
                else
                {
                    partsUpdateCmd2.Parameters["rid"].Value = Settings.ReceiptID;
                    partsUpdateCmd2.Parameters["name6"].Value = partsNameAdjust(textBox8.Text);
                    partsUpdateCmd2.Parameters["name7"].Value = partsNameAdjust(textBox9.Text);
                    partsUpdateCmd2.Parameters["name8"].Value = partsNameAdjust(textBox10.Text);
                    partsUpdateCmd2.Parameters["name9"].Value = partsNameAdjust(textBox11.Text);
                    partsUpdateCmd2.Parameters["name10"].Value = partsNameAdjust(textBox12.Text);
                    partsUpdateCmd2.Parameters["s2"].Value = sDate;
                    partsUpdateCmd2.Parameters["e2"].Value = eDate;
                    try
                    {
                        partsUpdateCmd2.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("部位の更新に失敗しました" + e.Message);
                    }
                }
            }
        }

        #endregion

        #region 部位情報の読み込み
        /// <summary>
        /// 部位情報の読み込み
        /// </summary>
        private void partsSelect(long receiptID)
        {
            partsSelectCmd.Parameters["rid"].Value = receiptID;

            using (var dr = partsSelectCmd.ExecuteReader())
            {
                if (!dr.HasRows)
                {
                    textBox8.Clear();
                    textBox9.Clear();
                    textBox10.Clear();
                    textBox11.Clear();
                    textBox12.Clear();
                    textBox14.Clear();
                    textBox15.Clear();
                    return;
                }
                dr.Read();
                var os = new object[dr.FieldCount];
                dr.GetValues(os);

                if (inputMode != INPUT_MODE.部位2 && inputMode != INPUT_MODE.日付2)
                {
                    textBox8.Text = (string)os[1];
                    textBox9.Text = (string)os[2];
                    textBox10.Text = (string)os[3];
                    textBox11.Text = (string)os[4];
                    textBox12.Text = (string)os[5];
                    textBox14.Text = (int)os[11] == 0 ? string.Empty : os[11].ToString();
                    textBox15.Text = (int)os[13] == 0 ? string.Empty : os[13].ToString();
                }
                else
                {
                    textBox8.Text = (string)os[6];
                    textBox9.Text = (string)os[7];
                    textBox10.Text = (string)os[8];
                    textBox11.Text = (string)os[9];
                    textBox12.Text = (string)os[10];
                    textBox14.Text = (int)os[12] == 0 ? string.Empty : os[12].ToString();
                    textBox15.Text = (int)os[14] == 0 ? string.Empty : os[14].ToString();
                }
            }
        }
        #endregion



        private string partsNameAdjust(string str)
        {
            string returnStr;
            returnStr = str.Trim().Replace('(', '（').Replace(')', '）');
            returnStr = returnStr.Replace('頸', '頚');
            returnStr = returnStr.Replace('1', '１').Replace('2', '２').Replace('3', '３').Replace('4', '４').Replace('5', '５');
            return returnStr;
        }

        #region メニューイベント


        private void 終了XToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void オプションOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SettingForm f = new SettingForm())
            {
                f.ShowDialog();
            }
        }

        private void バージョン情報AToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (VersionForm f = new VersionForm())
            {
                f.ShowDialog();
            }
        }

        private void スキャンデータ管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (ScansForm f = new ScansForm())
            {
                f.ShowDialog();
            }
        }

        private void ユーザー情報ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (InputLogForm f = new InputLogForm())
            {
                f.ShowDialog();
            }
        }
               
        private void 入力情報検索ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SearchForm f = new SearchForm())
            {
                f.ShowDialog();
            }
        }

        private void 漏れチェックToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OmissionCheckForm f = new OmissionCheckForm())
            {
                f.ShowDialog();
            }
        }

        private void 通常入力ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox4.Enabled = true;
            groupBox5.Enabled = false;
            groupBox6.Enabled = false;
            inputMode = INPUT_MODE.通常;

            通常入力ToolStripMenuItem.Checked = true;
            部位入力1ToolStripMenuItem.Checked = false;
            部位入力2ToolStripMenuItem.Checked = false;
            日付入力１回目FToolStripMenuItem.Checked = false;
            日付入力２回目SToolStripMenuItem.Checked = false;
            textBox13.Text = "通常入力";

            receiptChange(Settings.ReceiptID);
        }

        private void 部位入力1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox4.Enabled = true;
            groupBox5.Enabled = true;
            groupBox6.Enabled = true;
            inputMode = INPUT_MODE.部位1;

            通常入力ToolStripMenuItem.Checked = false;
            部位入力1ToolStripMenuItem.Checked = true;
            部位入力2ToolStripMenuItem.Checked = false;
            日付入力１回目FToolStripMenuItem.Checked = false;
            日付入力２回目SToolStripMenuItem.Checked = false;
            textBox13.Text = "部位入力１回目";

            receiptChange(Settings.ReceiptID);
        }

        private void 部位入力2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox4.Enabled = false;
            groupBox5.Enabled = true;
            groupBox6.Enabled = true;
            inputMode = INPUT_MODE.部位2;

            通常入力ToolStripMenuItem.Checked = false;
            部位入力1ToolStripMenuItem.Checked = false;
            部位入力2ToolStripMenuItem.Checked = true;
            日付入力１回目FToolStripMenuItem.Checked = false;
            日付入力２回目SToolStripMenuItem.Checked = false;
            textBox13.Text = "部位入力２回目";

            receiptChange(Settings.ReceiptID);
        }

        private void 部位チェックToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new PartsCheckForm()) f.ShowDialog();
        }

        private void 部位表現統一ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show("通常この作業は行わないでください。データベースに対して部位情報の統一を行ないますか？", "部位情報統一", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (res != System.Windows.Forms.DialogResult.OK) return;


            DataTable dt = new DataTable();
            using (var cmd = SQLClass.CreateCommand("SELECT * FROM parts"))
            using (var da = SQLClass.CreateDataAdapter(cmd))
            {
                da.Fill(dt);
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i][1] = partsNameAdjust(((string)dt.Rows[i][1]));
                dt.Rows[i][2] = partsNameAdjust(((string)dt.Rows[i][2]));
                dt.Rows[i][3] = partsNameAdjust(((string)dt.Rows[i][3]));
                dt.Rows[i][4] = partsNameAdjust(((string)dt.Rows[i][4]));
                dt.Rows[i][5] = partsNameAdjust(((string)dt.Rows[i][5]));
                dt.Rows[i][6] = partsNameAdjust(((string)dt.Rows[i][6]));
                dt.Rows[i][7] = partsNameAdjust(((string)dt.Rows[i][7]));
                dt.Rows[i][8] = partsNameAdjust(((string)dt.Rows[i][8]));
                dt.Rows[i][9] = partsNameAdjust(((string)dt.Rows[i][9]));
                dt.Rows[i][10] = partsNameAdjust(((string)dt.Rows[i][10]));
            }

            using (var upcmd = SQLClass.CreateCommand("UPDATE parts SET name1 = :name1, name2 = :name2, name3 = :name3, name4 = :name4, name5 = :name5, name6 = :name6, name7 = :name7, name8 = :name8, name9 = :name9, name10 = :name10 WHERE rid =:rid"))
            {
                upcmd.Parameters.Add("name1", NpgsqlTypes.NpgsqlDbType.Text);
                upcmd.Parameters.Add("name2", NpgsqlTypes.NpgsqlDbType.Text);
                upcmd.Parameters.Add("name3", NpgsqlTypes.NpgsqlDbType.Text);
                upcmd.Parameters.Add("name4", NpgsqlTypes.NpgsqlDbType.Text);
                upcmd.Parameters.Add("name5", NpgsqlTypes.NpgsqlDbType.Text);
                upcmd.Parameters.Add("name6", NpgsqlTypes.NpgsqlDbType.Text);
                upcmd.Parameters.Add("name7", NpgsqlTypes.NpgsqlDbType.Text);
                upcmd.Parameters.Add("name8", NpgsqlTypes.NpgsqlDbType.Text);
                upcmd.Parameters.Add("name9", NpgsqlTypes.NpgsqlDbType.Text);
                upcmd.Parameters.Add("name10", NpgsqlTypes.NpgsqlDbType.Text);
                upcmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    upcmd.Parameters["name1"].Value = dt.Rows[i][1];
                    upcmd.Parameters["name2"].Value = dt.Rows[i][2];
                    upcmd.Parameters["name3"].Value = dt.Rows[i][3];
                    upcmd.Parameters["name4"].Value = dt.Rows[i][4];
                    upcmd.Parameters["name5"].Value = dt.Rows[i][5];
                    upcmd.Parameters["name6"].Value = dt.Rows[i][6];
                    upcmd.Parameters["name7"].Value = dt.Rows[i][7];
                    upcmd.Parameters["name8"].Value = dt.Rows[i][8];
                    upcmd.Parameters["name9"].Value = dt.Rows[i][9];
                    upcmd.Parameters["name10"].Value = dt.Rows[i][10];
                    upcmd.Parameters["rid"].Value = dt.Rows[i][0];
                    upcmd.ExecuteNonQuery();
                }
            }
        }

        private void 日付入力１回目FToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox4.Enabled = false;
            groupBox5.Enabled = false;
            groupBox6.Enabled = true;
            inputMode = INPUT_MODE.日付1;

            通常入力ToolStripMenuItem.Checked = false;
            部位入力1ToolStripMenuItem.Checked = false;
            部位入力2ToolStripMenuItem.Checked = false;
            日付入力１回目FToolStripMenuItem.Checked = true;
            日付入力２回目SToolStripMenuItem.Checked = false;
            textBox13.Text = "日付入力１回目";

            receiptChange(Settings.ReceiptID);
        }

        private void 日付入力２回目SToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox4.Enabled = false;
            groupBox5.Enabled = false;
            groupBox6.Enabled = true;
            inputMode = INPUT_MODE.日付2;

            通常入力ToolStripMenuItem.Checked = false;
            部位入力1ToolStripMenuItem.Checked = false;
            部位入力2ToolStripMenuItem.Checked = false;
            日付入力１回目FToolStripMenuItem.Checked = false;
            日付入力２回目SToolStripMenuItem.Checked = true;
            textBox13.Text = "日付入力２回目";

            receiptChange(Settings.ReceiptID);
        }

        private void データ出力OToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new OutputForm()) f.ShowDialog();
        }
        #endregion


        
        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {

            //20190614133849 furukawa st ////////////////////////
            //復活
            
                    //20190612112822 furukawa st ////////////////////////
                    //不要

            if (e.KeyCode == Keys.Enter)
            {
                bool forward = e.Modifiers != Keys.Shift;
                this.SelectNextControl(this.ActiveControl, forward, true, true, true);
                e.Handled = true;
            }
                    //20190612112822 furukawa ed ////////////////////////
            //20190614133849 furukawa ed ////////////////////////
        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            //20190614133940 furukawa st ////////////////////////
            //復活
            
                    //20190612112746 furukawa st ////////////////////////
                    //不要

            if (e.KeyChar == (char)Keys.Enter) e.Handled = true;
                    //20190612112746 furukawa ed ////////////////////////
            //20190614133940 furukawa ed ////////////////////////
        }




        //20190612112844 furukawa KeyUpイベント
        /// <summary>
        ///  KeyUpイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_KeyUp(object sender, KeyEventArgs e)
        {
            //20190612133532 furukawa st ////////////////////////
            //左右カーソル、ページアップダウン追加

            switch (e.KeyCode)
            {
                case Keys.Enter:
                    bool forward = e.Modifiers != Keys.Shift;
                    //this.SelectNextControl(this.ActiveControl, forward, true, true, true);
                    e.Handled = true;
                    break;

                //20190614133556 furukawa st ////////////////////////
                //左右カーソルはテキストボックス内操作でもページ移動するので中止

                    //case Keys.Right:
                    //    changeFlag = false;
                    //    btnNext_Click(sender, e);
                    //    txtSinryoYM.Focus();
                    //    break;
                //20190614133556 furukawa ed ////////////////////////


                case Keys.PageUp:
                    btnNext_Click(sender, e);
                    txtSinryoYM.Focus();
                    break;


                //20190614133628 furukawa st ////////////////////////
                //左右カーソルはテキストボックス内操作でもページ移動するので中止

                //case Keys.Left:
                //20190614133628 furukawa ed ////////////////////////

                case Keys.PageDown:
                    btnPrevious_Click(sender, e);
                    txtSinryoYM.Focus();
                    break;

            }



                        //if (e.KeyCode == Keys.Enter)
                        //{                
                        //    bool forward = e.Modifiers != Keys.Shift;
                        //    this.SelectNextControl(this.ActiveControl, forward, true, true, true);
                        //    e.Handled = true;
                        //}
            //20190612133532 furukawa ed ////////////////////////

        }


        //20190612132651 furukawa ボタン上でのキーイベント調節
        /// <summary>
        /// ボタン上でのキーイベント調節
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNext_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

            //20190614133721 furukawa st ////////////////////////
            //シフト＋enterは煩雑になるので中止

                    //if (e.Modifiers == Keys.Shift)
                    //{
                    //    btnNext.Click -= new EventHandler(btnNext_Click);
                    //    KeyEventArgs ke=new KeyEventArgs(Keys.Shift);                
                    //    MainForm_KeyUp(sender, ke);
                    //}
                    //else
                    //{
                    //    btnNext.Click += new EventHandler(btnNext_Click);
                    //}
            //20190614133721 furukawa ed ////////////////////////
        }

    }
}
