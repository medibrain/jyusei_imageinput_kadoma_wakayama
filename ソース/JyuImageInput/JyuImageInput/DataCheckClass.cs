﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace JyuImageInput
{
    class DataCheckClass
    {
        /// <summary>
        /// エラーリスト
        /// </summary>
        public static List<ErrorData> ErrorList = new List<ErrorData>();
        public class ErrorData
        {
            public long rid { get; set; }
            public int errorCode { get; set; }
            public string errorText { get; set; }

            public ErrorData(long rid, Settings.ERROR_CODE errorCode)
            {
                this.rid = rid;
                this.errorCode = (int)errorCode;
                this.errorText = Settings.errorMessage[this.errorCode];
            }
        }

        //20190505213228 furukawa st ////////////////////////
        //令和対応
        static int[] e_2_yyyy = { 0, 1867, 1911, 1925, 1988 , 2018};
        //static int[] e_2_yyyy = { 0, 1867, 1911, 1925, 1988 };
        //20190505213228 furukawa ed ////////////////////////

        /// <summary> 診療年月・請求年月計算用 </summary>
        /// <param name="eyymm">和暦５桁</param>
        /// <returns>年月（yyyyMM） 異常時は-1</returns>
        static int JMonth_2_yyyymm(string eyymm)
        {
            try
            {
                eyymm = eyymm.Trim();
                if (eyymm.Length != 5)  return -1;

                int e = int.Parse(eyymm.Substring(0, 1));
                int y = int.Parse(eyymm.Substring(1, 2));
                int m = int.Parse(eyymm.Substring(3, 2));
                return (e_2_yyyy[e] + y) * 100 + m;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }


        /// <summary>
        /// チェック用データ一時クラス
        /// </summary>
        public static List<BillRecord> lst = new List<BillRecord>();
        public class BillRecord
        {
            /// <summary> ★必須★ 電算管理番号 （主キー）</summary>
            public string id { get; set; }

            /// <summary> ★必須★ 被保険者番号 </summary>
            public string insuredNo { get; set; }

            /// <summary> ★必須★ 診療年月 西暦６桁 YYYYMM</summary>
            public int mediDate { get; set; }

            /// <summary> 請求年月 西暦６桁 YYYYMM</summary>
            public int billDate { get; set; }

            /// <summary> 生年月日 </summary>
            public int jBirth { get; set; }

            /// <summary> 施術師コード 10桁 </summary>
            public long hosCode { get; set; }

            /// <summary> 区分（7:鍼灸、8：あんま、9：柔整）</summary>
            public int typeCode { get; set; }

            /// <summary> 性別 </summary>
            public int sex { get; set; }

            /// <summary> ★必須★ 金額（円）門真は保健決定点数</summary>
            public int totalCost { get; set; }

            /// <summary> 状態区分 </summary>
            public int stateCode { get; set; }
        }

        /// <summary>
        /// チェック用CSVからデータを読み込みます
        /// </summary>
        /// <param name="CSVFileName"></param>
        public static void CSVReader(string CSVFileName)
        {
            int tempInt;
            long tempLong;
            using (System.IO.StreamReader sr = new System.IO.StreamReader(CSVFileName))
            {
                while (sr.Peek() > 0)
                {
                    var str = sr.ReadLine().Replace("\"", "");
                    var strs = str.Split(',');

                    var rcd = new BillRecord();
#if WAKA_KOIKI  
                    // ★ 和歌山広域連合向けのCSV取込 ★
                    if (strs.Length < 68) continue;
                    if (!int.TryParse(strs[0], out tempInt)) continue;      // 請求年月
                    rcd.billDate = tempInt;
                    rcd.insuredNo = strs[1].Trim();
                    if (int.TryParse(strs[2], out tempInt)) rcd.typeCode = tempInt;
                    if (int.TryParse(strs[4], out tempInt)) rcd.mediDate = tempInt;
                    if (int.TryParse(strs[11], out tempInt)) rcd.sex = tempInt;
                    if (int.TryParse(strs[12], out tempInt)) rcd.jBirth = tempInt;
                    if (int.TryParse(strs[45], out tempInt)) rcd.totalCost = tempInt;
                    if (int.TryParse(strs[65], out tempInt)) rcd.stateCode = tempInt;
                    rcd.id = strs[66].Trim();

                    tempLong = 0;
                    if (int.TryParse(strs[5], out tempInt)) tempLong = (long)tempInt * 100000000L;
                    tempLong += rcd.typeCode * 10000000L;
                    if (int.TryParse(strs[6], out tempInt)) tempLong += (long)tempInt * 100000L;
                    if (int.TryParse(strs[7], out tempInt)) tempLong += tempInt;
                    rcd.hosCode = tempLong;
#elif KADOMA_CITY
                    // ★ 門真市役所向けのCSV取込 ★
                    if (strs.Length < 60) continue;
                    rcd.billDate = JMonth_2_yyyymm(strs[0]);                            // 請求年月
                    rcd.id = strs[1].Trim();                                            // 電算管理番号 (レセプト全国共通キー)
                    rcd.insuredNo = strs[4].Trim();                                     // 被保険者番号
                    if (rcd.insuredNo.Length > Settings.numberLength) rcd.insuredNo = rcd.insuredNo.Substring(rcd.insuredNo.Length - Settings.numberLength);
                    rcd.mediDate = JMonth_2_yyyymm(strs[13]);                           // 診療年月
                    if (int.TryParse(strs[5], out tempInt)) rcd.jBirth = tempInt;       // 生年月日
                    if (long.TryParse(strs[2], out tempLong)) rcd.hosCode = tempLong;   // 施術師コード （医科・歯科・調剤も取り込む）
                    if (int.TryParse(strs[6], out tempInt)) rcd.sex = tempInt;          // 性別
                    rcd.typeCode = (int)(rcd.hosCode / 10000000) % 10;                  // 区分
                    if (rcd.typeCode != 0 && rcd.typeCode != 7 && rcd.typeCode != 8 && rcd.typeCode != 9) continue;
                    if (int.TryParse(strs[59], out tempInt)) rcd.totalCost = tempInt;   // 金額 （国保決定点数）
                    rcd.stateCode = 0;                                                  // 状態区分 （ゼロとする）

                    if (rcd.mediDate < 0) continue;
                    if (rcd.id == "") continue;
#else
#error　コンパイルエラー！ コンパイルスイッチにて、役所名を指定してください
#endif
                    lst.Add(rcd);
                }
            }
        }

    }
}
