﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class ScansForm : Form
    {
        public ScansForm()
        {
            InitializeComponent();

            listUpdate();

            dataGridView1.Columns[0].Width = 50;
            dataGridView1.Columns[0].HeaderText = "ID";
            dataGridView1.Columns[1].HeaderText = "スキャン日";
            dataGridView1.Columns[2].HeaderText = "審査月";
            dataGridView1.Columns[3].HeaderText = "枚数";
            dataGridView1.Columns[1].DefaultCellStyle.Format = "0000-00-00";

            dataGridView1.Columns[4].Visible = false;
            dataGridView1.Columns[5].Visible = false;

            dataGridView1.Columns[6].HeaderText = "処理終了日";
            dataGridView1.Columns[6].DefaultCellStyle.Format = "0000-00-00";
            dataGridView1.Columns[7].HeaderText = "チェック日";
            dataGridView1.Columns[7].DefaultCellStyle.Format = "0000-00-00";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (ScanDataForm f = new ScanDataForm())
            {
                f.ShowDialog();
            }

            listUpdate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            listUpdate();
        }

        private void listUpdate()
        {
            DataTable dt = new DataTable();
            if (checkBox1.Checked)
            {
                using (var da = SQLClass.CreateDataAdapter("SELECT sid, scandate, billmonth, scancount, enddate, checkdate FROM scans ORDER BY sid"))
                {
                    da.Fill(dt);
                }
            }
            else
            {
                using (var da = SQLClass.CreateDataAdapter("SELECT sid, scandate, billmonth, scancount, enddate, checkdate FROM scans WHERE enddate <= 0 ORDER BY sid"))
                {
                    da.Fill(dt);
                }
            }

            dt.Columns.Add("EndDate2");
            dt.Columns.Add("CheckDate2");


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if ((int)dt.Rows[i][4] != 0)
                {
                    dt.Rows[i][6] = ((int)dt.Rows[i][4]).ToString("0000/00/00");
                }
                else
                {
                    dt.Rows[i][6] = string.Empty;
                }

                if ((int)dt.Rows[i][5] != 0)
                {
                    dt.Rows[i][7] = ((int)dt.Rows[i][5]).ToString("0000/00/00");
                }
                else
                {
                    dt.Rows[i][7] = string.Empty;
                }
            }

            dataGridView1.DataSource = dt;

        }

        private void チェックToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //チェックを開始
            if (dataGridView1.CurrentCell == null) return;
            int sid = (int)dataGridView1[0, dataGridView1.CurrentRow.Index].Value;

            using (var f = new CheckArrangeForm(sid))
            {
                f.ShowDialog();
            }

            listUpdate();
        }

        private void チェック取り消しToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //チェックを行ったものをなかったことにする
            if (dataGridView1.CurrentCell == null) return;
            int rid = dataGridView1.CurrentRow.Index;
            int sid = (int)dataGridView1[0, rid].Value;

            if ((string)dataGridView1[7, rid].Value == string.Empty)
            {
                MessageBox.Show("チェックが終了していません", "チェック取り消し", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            var res = MessageBox.Show("この作業を行うと、チェック結果がクリアされ、もとには戻せません\r\nチェックを取り消しますか？", "チェック取り消し確認",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            if (res != System.Windows.Forms.DialogResult.OK) return;

            res = MessageBox.Show("チェック結果がクリアされます\r\n戻せませんが本当にチェックを取り消しますか？", "チェック取り消し再確認",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (res != System.Windows.Forms.DialogResult.OK) return;

            //チェック日付,終了日付をクリア
            using (var cmd = SQLClass.CreateCommand("UPDATE scans SET checkdate = 0, enddate = 0 WHERE sid = :sid"))
            {
                cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["sid"].Value = sid;
                cmd.ExecuteNonQuery();
            }

            //対象レセプトのデータを削除
            using (var cmd = SQLClass.CreateCommand("UPDATE receipts SET outdata = '', errorcode = 0 FROM groups WHERE receipts.gid = groups.gid AND groups.sid = :sid"))
            {
                cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["sid"].Value = sid;
                var co = cmd.ExecuteNonQuery();
            }

            MessageBox.Show("対象のチェック結果を削除しました");
            listUpdate();
        }

        private void スキャンデータ削除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //スキャンデータをすべて削除する
            if (dataGridView1.CurrentCell == null) return;
            int rid = dataGridView1.CurrentRow.Index;
            int sid = (int)dataGridView1[0, rid].Value;
            int billmonth = (int)dataGridView1[2, rid].Value;

            var res = MessageBox.Show("スキャンID:[" + sid.ToString() + "]\r\nこのスキャンデータを画像も含め、すべて削除します\r\nよろしいですか？", "スキャン削除確認",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button2);
            if (res != System.Windows.Forms.DialogResult.OK) return;


            //再確認
            res = MessageBox.Show("スキャンID:[" + sid.ToString() + "]\r\n画像も含め、すべて削除されます\r\nもとには戻せませんが、本当によろしいですか？", "スキャン削除再確認",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            if (res != System.Windows.Forms.DialogResult.OK) return;

            List<long> delRidList = new List<long>();
            List<string> missLog = new List<string>();

            //対象ID一覧取得
            using (var cmd = SQLClass.CreateCommand("SELECT rid FROM receipts WHERE gid IN (SELECT gid FROM groups WHERE sid = :sid)"))
            {
                cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["sid"].Value = sid;
                try
                {
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            delRidList.Add(dr.GetInt64(0));
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("データの削除に失敗しました\r\n" + ex.Message);
                    return;
                }
            }

            //画像削除
            string fileName;
            foreach (var item in delRidList)
            {
                for (int i = 0; i < 10; i++)
                {
                    fileName = Settings.imagePath + "\\" + billmonth.ToString() + i.ToString("00") + "\\" + item.ToString() + ".gif";
                    if (System.IO.File.Exists(fileName))
                    {
                        try
                        {
                            System.IO.File.Delete(fileName);
                        }
                        catch (Exception er)
                        {
                            //削除できなかった場合、ログを残す
                            missLog.Add(item.ToString() + "," + er.Message);
                        }
                        break;
                    }
                    if (i == 9)
                    {
                        //フォルダ番号が9まで行ってファイルがなかった場合
                        missLog.Add(item.ToString() + ",,対象ファイルが見つかりませんでした");
                    }
                }
            }


            //レセプトデータベース削除
            using (var cmd = SQLClass.CreateCommand("DELETE FROM receipts WHERE gid IN (SELECT gid FROM groups WHERE sid = :sid)"))
            {
                cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["sid"].Value = sid;

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch(Exception er)
                {
                    missLog.Add("0," + er.Message + ",レセプトデータベースを削除できませんでした");
                }
            }


            //グループ削除
            using (var cmd = SQLClass.CreateCommand("DELETE FROM groups WHERE sid = :sid"))
            {
                cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["sid"].Value = sid;

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch(Exception er)
                {
                    missLog.Add("0," + er.Message + ",グループデータベースを削除できませんでした");
                }
            }


            //スキャン情報削除
            using (var cmd = SQLClass.CreateCommand("DELETE FROM scans WHERE sid = :sid"))
            {
                cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["sid"].Value = sid;

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch(Exception er)
                {
                    missLog.Add("0," + er.Message + ",スキャンデータベースデータを削除できませんでした");
                }
            }

            if (missLog.Count == 0)
            {
                MessageBox.Show("対象のスキャンデータを削除しました");
                listUpdate();
            }
            else
            {
                //ログファイルを保存
                string logName = Application.StartupPath + "\\errorlog" + DateTime.Now.ToString("yyMMddhhmmss") + ".log";
                using(System.IO.StreamWriter sw=new System.IO.StreamWriter(logName,true,Encoding.GetEncoding("shift_jis")))
                {
                    foreach (var item in missLog)
                    {
                        sw.WriteLine(item);
                    }
                }

                MessageBox.Show("対象のスキャンデータを削除しましたが、いくつかミスが発生しています。\r\nログファイルを確認してください。");
                listUpdate();
            }
        }
    }
}
