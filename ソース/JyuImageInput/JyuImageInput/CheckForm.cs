﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class CheckArrangeForm : Form
    {
        int targetSID = -1;

        public CheckArrangeForm(int targetSID)
        {
            InitializeComponent();
            this.targetSID = targetSID;
            this.Text = "データチェック　対象スキャンID [ " + targetSID.ToString() + " ]";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string fileName;
            using (OpenFileDialog f = new OpenFileDialog())
            {
                var res = f.ShowDialog();
                if (res != System.Windows.Forms.DialogResult.OK) return;
                fileName = f.FileName;
            }

            DataCheckClass.CSVReader(fileName);

            var hs = new HashSet<string>();

            using(var cmd = SQLClass.CreateCommand("SELECT id FROM tdata"))
            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    hs.Add(dr.GetString(0));
                }
            }

            using (var inCmd = SQLClass.CreateCommand("INSERT INTO tdata (id, sex, birth, insuredno, hoscode, medimonth, billmonth, typecode, totalcost, statecode) VALUES (:id, :sex, :birth, :insuredno, :hoscode, :medimonth, :billmonth, :typecode, :totalcost, :statecode)"))
            using (var upCmd = SQLClass.CreateCommand("UPDATE tdata SET sex = :sex, birth = :birth, insuredno = :insuredno, hoscode = :hoscode, medimonth = :medimonth, billmonth = :billmonth, typecode = :typecode, totalcost = :totalcost , statecode = :statecode WHERE id = :id"))
            {
                inCmd.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Text);
                inCmd.Parameters.Add("sex", NpgsqlTypes.NpgsqlDbType.Integer);
                inCmd.Parameters.Add("birth", NpgsqlTypes.NpgsqlDbType.Integer);
                inCmd.Parameters.Add("insuredno", NpgsqlTypes.NpgsqlDbType.Text);
                inCmd.Parameters.Add("hoscode", NpgsqlTypes.NpgsqlDbType.Bigint);
                inCmd.Parameters.Add("medimonth", NpgsqlTypes.NpgsqlDbType.Integer);
                inCmd.Parameters.Add("billmonth", NpgsqlTypes.NpgsqlDbType.Integer);
                inCmd.Parameters.Add("typecode", NpgsqlTypes.NpgsqlDbType.Integer);
                inCmd.Parameters.Add("totalcost", NpgsqlTypes.NpgsqlDbType.Integer);
                inCmd.Parameters.Add("statecode", NpgsqlTypes.NpgsqlDbType.Integer);

                upCmd.Parameters.Add("sex", NpgsqlTypes.NpgsqlDbType.Integer);
                upCmd.Parameters.Add("birth", NpgsqlTypes.NpgsqlDbType.Integer);
                upCmd.Parameters.Add("insuredno", NpgsqlTypes.NpgsqlDbType.Text);
                upCmd.Parameters.Add("hoscode", NpgsqlTypes.NpgsqlDbType.Bigint);
                upCmd.Parameters.Add("medimonth", NpgsqlTypes.NpgsqlDbType.Integer);
                upCmd.Parameters.Add("billmonth", NpgsqlTypes.NpgsqlDbType.Integer);
                upCmd.Parameters.Add("typecode", NpgsqlTypes.NpgsqlDbType.Integer);
                upCmd.Parameters.Add("totalcost", NpgsqlTypes.NpgsqlDbType.Integer);
                upCmd.Parameters.Add("statecode", NpgsqlTypes.NpgsqlDbType.Integer);
                upCmd.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Text);


                progressBar1.Maximum = DataCheckClass.lst.Count;
                progressBar1.Value = 0;

                var t = System.Threading.Tasks.Task.Factory.StartNew(() =>
                    {
                        foreach (var item in DataCheckClass.lst)
                        {
                            Invoke(new Action(() =>
                            {
                                progressBar1.Value++;
                            }));

                            if (item.id == "") continue;
                            if (hs.Contains(item.id))
                            {
                                //登録済みの場合、更新
                                upCmd.Parameters[0].Value = item.sex;
                                upCmd.Parameters[1].Value = item.jBirth;
                                upCmd.Parameters[2].Value = item.insuredNo;
                                upCmd.Parameters[3].Value = item.hosCode;
                                upCmd.Parameters[4].Value = item.mediDate;
                                upCmd.Parameters[5].Value = item.billDate;
                                upCmd.Parameters[6].Value = item.typeCode;
                                upCmd.Parameters[7].Value = item.totalCost;
                                upCmd.Parameters[8].Value = item.stateCode;
                                upCmd.Parameters[9].Value = item.id;
                                upCmd.ExecuteNonQuery();
                            }
                            else
                            {
                                //登録のない場合、新規登録
                                inCmd.Parameters[0].Value = item.id;
                                inCmd.Parameters[1].Value = item.sex;
                                inCmd.Parameters[2].Value = item.jBirth;
                                inCmd.Parameters[3].Value = item.insuredNo;
                                inCmd.Parameters[4].Value = item.hosCode;
                                inCmd.Parameters[5].Value = item.mediDate;
                                inCmd.Parameters[6].Value = item.billDate;
                                inCmd.Parameters[7].Value = item.typeCode;
                                inCmd.Parameters[8].Value = item.totalCost;
                                inCmd.Parameters[9].Value = item.stateCode;
                                inCmd.ExecuteNonQuery();
                                hs.Add(item.id);
                            }
                        }
                    });

                t.ContinueWith(item =>
                {
                    MessageBox.Show("取り込みが終了しました");
                });
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //チェック日を取得　チェックの形跡がなければエラーを返す
            using (var checkDateCmd = SQLClass.CreateCommand("SELECT checkdate FROM scans WHERE sid = :sid"))
            {
                checkDateCmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                checkDateCmd.Parameters[0].Value = targetSID;
                int checkDate = (int)checkDateCmd.ExecuteScalar();
                if (checkDate == 0)
                {
                    MessageBox.Show("チェックが行われていません。", "チェックエラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            //チェック日を取得　チェックの形跡がなければエラーを返す
            using (var endDateCheckCmd = SQLClass.CreateCommand("SELECT enddate FROM scans WHERE sid = :sid"))
            {
                endDateCheckCmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                endDateCheckCmd.Parameters[0].Value = targetSID;
                int checkDate = (int)endDateCheckCmd.ExecuteScalar();
                if (checkDate != 0)
                {
                    var endDateCheckRes = MessageBox.Show("最終確認済みですが、もう一度確認しますか？",
                        "チェック確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (endDateCheckRes != System.Windows.Forms.DialogResult.Yes) return;
                }
            }


            //チェック結果を再点検
            //現在のエラーリストをクリア
            DataCheckClass.ErrorList.Clear();

            long rid;
            int errorCode;

            //過去チェック時のエラーリストを読み込む
            using (var cmd = SQLClass.CreateCommand("SELECT receipts.rid, receipts.errorcode FROM receipts, groups WHERE groups.gid = receipts.gid AND groups.sid = :sid AND receipts.errorcode <> 0"))
            {
                cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = targetSID;

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        rid = dr.GetInt64(0);
                        errorCode = dr.GetInt32(1);
                        DataCheckClass.ErrorList.Add(new DataCheckClass.ErrorData(rid, (Settings.ERROR_CODE)errorCode));
                    }
                }
            }

            //表示
            DataCheckClass.ErrorList.Sort((x, y) => x.rid == y.rid ? 0 : x.rid > y.rid ? 1 : -1);
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = DataCheckClass.ErrorList;
            dataGridView1.Columns[1].Width = 100;
            dataGridView1.Columns[2].Width = 500;
            dataGridView1.Columns[0].HeaderText = "ID";
            dataGridView1.Columns[1].HeaderText = "エラーコード";
            dataGridView1.Columns[2].HeaderText = "エラー内容";

            label5.Text = "エラー件数：  " + DataCheckClass.ErrorList.Count.ToString();

            int errorCount = DataCheckClass.ErrorList.Count(item => item.errorCode % 10 == 1);
            if (errorCount > 0)
            {
                MessageBox.Show("処理の方法が指定されていないエラーが [ "+ errorCount.ToString() + " ] 件あります。チェックをやり直してください。",
                    "チェックエラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int voidErrorCount = DataCheckClass.ErrorList.Count(item => item.errorCode % 10 == 2);
            if (voidErrorCount > 0)
            {
                var voidres = MessageBox.Show("無視に指定されているエラーが [ " + voidErrorCount.ToString() + " ] 件あります。よろしいですか？",
                    "チェック確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (voidres != System.Windows.Forms.DialogResult.Yes) return;
            }
            
            int beforeErrorCount = DataCheckClass.ErrorList.Count(item => item.errorCode % 10 == 3);
            if (beforeErrorCount > 0)
            {
                var befres = MessageBox.Show("保留に指定されているエラーが [ " + beforeErrorCount.ToString() + " ] 件あります。よろしいですか？",
                    "チェック確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (befres != System.Windows.Forms.DialogResult.Yes) return;
            }

            //todo: 続紙に対する番号を振る  "続紙"
            //続紙に対して、直前の画像の電算管理番号を割り当て
            string outData = string.Empty;
            int zokushiCount = 1;
            using (var cmd = SQLClass.CreateCommand("SELECT "+
                "receipts.rid, receipts.outdata, receipts.errorcode " +
                "FROM receipts, groups WHERE groups.gid = receipts.gid AND groups.sid = :sid "+
                "ORDER BY receipts.rid"))
            using(var cmd2=SQLClass.CreateCommand("UPDATE receipts SET outdata=:outdata "+
                "WHERE rid=:rid"))
            using(var ecmd = SQLClass.CreateCommand("UPDATE receipts SET errorcode=:errorcode "+
                "WHERE rid=:rid"))
            {
                cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters["sid"].Value = targetSID;

                cmd2.Parameters.Add("outdata", NpgsqlTypes.NpgsqlDbType.Text);
                cmd2.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);

                ecmd.Parameters.Add("errorcode", NpgsqlTypes.NpgsqlDbType.Integer);
                ecmd.Parameters["errorcode"].Value = (int)Settings.ERROR_CODE.エラーレセ続紙;
                ecmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var er = dr.GetInt32(2);
                        var s = dr.GetString(1);

                        if (er != 0)
                        {
                            //0以外出力しない
                            zokushiCount = 1;
                            outData = string.Empty;
                        }
                        else if (s == "続紙")
                        {
                            var l = dr.GetInt64(0);
                            if (outData == string.Empty)
                            {
                                ecmd.Parameters["rid"].Value = l;
                                ecmd.ExecuteNonQuery();
                            }
                            else
                            {
                                zokushiCount++;
                                cmd2.Parameters["rid"].Value = l;
                                cmd2.Parameters["outdata"].Value = outData + "-" + zokushiCount.ToString();
                                cmd2.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            outData = s;
                            zokushiCount = 1;
                        }
                    }
                }
            }

            using (var cmd = SQLClass.CreateCommand("SELECT receipts.rid, receipts.errorcode FROM receipts, groups WHERE groups.gid = receipts.gid AND groups.sid = :sid AND receipts.errorcode <> 0"))
            {
                cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = targetSID;

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        rid = dr.GetInt64(0);
                        errorCode = dr.GetInt32(1);
                        DataCheckClass.ErrorList.Add(new DataCheckClass.ErrorData(rid, (Settings.ERROR_CODE)errorCode));
                    }
                }
            }




            var res = MessageBox.Show("最終チェックが終わりました。入力/チェックが終了したスキャンとして扱います。よろしいですか？",
                                    "チェック確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res != System.Windows.Forms.DialogResult.Yes) return;

            using (var endCmd = SQLClass.CreateCommand("UPDATE scans SET enddate = :enddate WHERE sid = :sid"))
            {
                endCmd.Parameters.Add("enddate", NpgsqlTypes.NpgsqlDbType.Integer);
                endCmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                endCmd.Parameters[0].Value = DateTimeEx.NowDateInt();
                endCmd.Parameters[1].Value = targetSID;
                endCmd.ExecuteNonQuery();
            }

            MessageBox.Show("一連のチェックが終わりました。このスキャンを出力対象に指定できます");

        }

        private void button5_Click(object sender, EventArgs e)
        {
            using (OutputForm f = new OutputForm())
            {
                f.ShowDialog();
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            bool fullCheckMode = checkBox1.Checked;

            //チェック日を取得　初めてならチェックボックスにかかわらずフルチェック
            using (var checkDateCmd = SQLClass.CreateCommand("SELECT checkdate FROM scans WHERE sid = :sid"))
            {
                checkDateCmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                checkDateCmd.Parameters[0].Value = targetSID;
                int checkDate = (int)checkDateCmd.ExecuteScalar();
                if (checkDate == 0) fullCheckMode = true;
            }

            //チェック日を書き込む
            using (var checkDateCmd = SQLClass.CreateCommand("UPDATE scans SET checkdate = :checkdate WHERE sid = :sid"))
            {
                checkDateCmd.Parameters.Add("checkdate", NpgsqlTypes.NpgsqlDbType.Integer);
                checkDateCmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                checkDateCmd.Parameters[0].Value = DateTimeEx.NowDateInt();
                checkDateCmd.Parameters[1].Value = targetSID;
                checkDateCmd.ExecuteNonQuery();
            }

            //フルチェックモードでない場合　リチェックモードへ
            if (!fullCheckMode)
            {
                reCheck();
                return;
            }

            //フルチェックモードでチェックした場合、処理終了指定を解除
            using (var endCmd = SQLClass.CreateCommand("UPDATE scans SET enddate = 0 WHERE sid = :sid"))
            {
                endCmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                endCmd.Parameters[0].Value = targetSID;
                endCmd.ExecuteNonQuery();
            }


            using (var errorCmd = SQLClass.CreateCommand("UPDATE receipts SET outdata = 'error', errorcode = :errorcode WHERE rid = :rid"))
            {
                //エラーコードを書き込むためのコマンド
                errorCmd.Parameters.Add("errorcode", NpgsqlTypes.NpgsqlDbType.Integer);
                errorCmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);

                //現在のエラーリストをクリア
                DataCheckClass.ErrorList.Clear();

                //現在のエラーコードをクリア
                using (var cmd = SQLClass.CreateCommand("UPDATE receipts SET errorcode = 0 WHERE groups.gid = receipts.gid AND groups.sid = :sid"))
                {
                    cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                    cmd.Parameters[0].Value = targetSID;
                }


                //未入力検査
                using (var cmd = SQLClass.CreateCommand("SELECT receipts.rid, outdata FROM receipts, groups WHERE medimonth = 0 AND groups.gid = receipts.gid AND groups.sid = :sid"))
                {
                    cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                    cmd.Parameters[0].Value = targetSID;

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            long rid = dr.GetInt64(0);
                            string errorCodeStr = dr.GetString(1);
                            DataCheckClass.ErrorList.Add(new DataCheckClass.ErrorData(rid, Settings.ERROR_CODE.未入力));
                            errorCmd.Parameters[0].Value = (int)Settings.ERROR_CODE.未入力;
                            errorCmd.Parameters[1].Value = rid;
                            errorCmd.ExecuteNonQuery();
                        }
                    }
                }



                //エラーコード999
                using (var cmd = SQLClass.CreateCommand("SELECT receipts.rid FROM receipts, groups WHERE medimonth = 999 AND groups.gid = receipts.gid AND groups.sid = :sid"))
                {
                    cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                    cmd.Parameters[0].Value = targetSID;

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            long rid = dr.GetInt64(0);
                            DataCheckClass.ErrorList.Add(new DataCheckClass.ErrorData(rid, Settings.ERROR_CODE.エラー指定));
                            errorCmd.Parameters[0].Value = (int)Settings.ERROR_CODE.エラー指定;
                            errorCmd.Parameters[1].Value = rid;
                            errorCmd.ExecuteNonQuery();
                        }
                    }
                }


                //各グループの最初に位置する、続紙等確認
                var lst = new List<long>();

                ////取り出した続紙等コード対象のレセプトデータで、各グループ最初にあるものかどうか確認
                using (var cmd = SQLClass.CreateCommand("SELECT MIN(receipts.rid) FROM receipts, groups WHERE groups.gid = receipts.gid AND groups.sid = :sid GROUP BY receipts.gid"))
                using (var da = SQLClass.CreateDataAdapter(cmd))
                {
                    cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                    cmd.Parameters[0].Value = targetSID;
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lst.Add(dr.GetInt64(0));
                        }
                    }
                }

                StringBuilder sb = new StringBuilder();
                foreach (var item in lst)
                {
                    sb.Append(item.ToString() + ", ");
                }
                sb.Remove(sb.Length - 2, 2);

                using (var cmd = SQLClass.CreateCommand("SELECT rid FROM receipts WHERE rid IN (" + sb.ToString() + ") AND medimonth = 99"))
                {
                    lst.Clear();

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            long rid = dr.GetInt64(0);
                            DataCheckClass.ErrorList.Add(new DataCheckClass.ErrorData(rid, Settings.ERROR_CODE.先頭続紙));
                            errorCmd.Parameters[0].Value = (int)Settings.ERROR_CODE.先頭続紙;
                            errorCmd.Parameters[1].Value = rid;
                            errorCmd.ExecuteNonQuery();
                        }
                    }
                }


                //入力エラー確認
                DataTable dt = new DataTable();
                using (var cmd = SQLClass.CreateCommand("SELECT receipts.rid, receipts.medimonth, receipts.insuredno, receipts.totalpoint" +
                    " FROM receipts, groups WHERE groups.gid = receipts.gid AND groups.sid = :sid ORDER BY receipts.rid"))
                using (var da = SQLClass.CreateDataAdapter(cmd))
                {
                    cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                    cmd.Parameters[0].Value = targetSID;
                    da.Fill(dt);
                }

                //プログレスバーの準備
                int dataCount = dt.Rows.Count;
                progressBar1.Maximum = dataCount;
                progressBar1.Value = 0;

                //エラーレセプトのIDを記録
                List<string> targetLst = new List<string>();

                //スレッドを別にして検査開始
                var t = System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    using (var cmd = SQLClass.CreateCommand("SELECT id FROM tdata " +
                        "WHERE medimonth = :medimonth AND insuredno = :insuredno AND totalcost = :totalcost"))
                    using (var upcmd = SQLClass.CreateCommand("UPDATE receipts SET outdata = :outdata WHERE rid = :rid"))
                    {
                        cmd.Parameters.Add("medimonth", NpgsqlTypes.NpgsqlDbType.Integer);
                        cmd.Parameters.Add("insuredno", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("totalcost", NpgsqlTypes.NpgsqlDbType.Integer);

                        upcmd.Parameters.Add("outdata", NpgsqlTypes.NpgsqlDbType.Text);
                        upcmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);

                        string receiptSerchIDStr = "";
                        int receiptCount = 0;
                        int mediMonth = 0;

                        for (int i = 0; i < dataCount; i++)
                        {
                            Invoke(new Action(() => progressBar1.Value++));

                            //999の場合、一致するデータは存在しない 0は未入力
                            if ((int)dt.Rows[i][1] == 999 || (int)dt.Rows[i][1] == 0) continue;

                            //99の場合、前のレセの続紙 グループ先頭を考慮していないため、調整の必要あり
                            if ((int)dt.Rows[i][1] == 99)
                            {
                                receiptCount++;
                                upcmd.Parameters[0].Value = "続紙";
                                upcmd.Parameters[1].Value = dt.Rows[i][0];
                                upcmd.ExecuteNonQuery();
                                continue;
                            }

                            mediMonth = DateTimeEx.YearMonthJtoAD_H((int)dt.Rows[i][1]);

                            cmd.Parameters[0].Value = mediMonth;
                            cmd.Parameters[1].Value = dt.Rows[i][2];
                            cmd.Parameters[2].Value = dt.Rows[i][3];

                            targetLst.Clear();

                            //2つ以上、もしくは1つもない場合はエラーリストへ書き込み
                            using (var dr = cmd.ExecuteReader())
                            {
                                while (dr.Read()) targetLst.Add(dr.GetString(0));
                                if (targetLst.Count == 0)
                                {
                                    long rid =(long)dt.Rows[i][0];
                                    DataCheckClass.ErrorList.Add(new DataCheckClass.ErrorData(rid, Settings.ERROR_CODE.一致データなし));
                                    errorCmd.Parameters[0].Value = (int)Settings.ERROR_CODE.一致データなし;
                                    errorCmd.Parameters[1].Value = rid;
                                    errorCmd.ExecuteNonQuery();
                                }
                                else if (targetLst.Count > 1)
                                {
                                    long rid = (long)dt.Rows[i][0];
                                    DataCheckClass.ErrorList.Add(new DataCheckClass.ErrorData(rid, Settings.ERROR_CODE.同一複数データ));
                                    errorCmd.Parameters[0].Value = (int)Settings.ERROR_CODE.同一複数データ;
                                    errorCmd.Parameters[1].Value = rid;
                                    errorCmd.ExecuteNonQuery();
                                }
                                else
                                {
                                    //1つのみであればOK 検索番号を書き込み
                                    receiptCount = 1;
                                    receiptSerchIDStr = targetLst[0].ToString();
                                    upcmd.Parameters[0].Value = receiptSerchIDStr;
                                    upcmd.Parameters[1].Value = dt.Rows[i][0];
                                    upcmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                });

                t.ContinueWith(item =>
                {

                    Invoke(new Action(() =>
                        {
                            checkBox1.Checked = false;
                            label5.Text = "エラー件数：  " + DataCheckClass.ErrorList.Count.ToString();
                        }));
                    if (DataCheckClass.ErrorList.Count == 0)
                    {
                        MessageBox.Show("検査が終わりました.入力ミスはありませんでした。");
                    }
                    else
                    {
                        DataCheckClass.ErrorList.Sort((x, y) => x.rid == y.rid ? 0 : x.rid > y.rid ? 1 : -1);
                        Invoke(new Action(() =>
                            {
                                dataGridView1.DataSource = null;
                                dataGridView1.DataSource = DataCheckClass.ErrorList;
                                dataGridView1.Columns[1].Width = 100;
                                dataGridView1.Columns[2].Width = 500;
                                dataGridView1.Columns[0].HeaderText = "ID";
                                dataGridView1.Columns[1].HeaderText = "エラーコード";
                                dataGridView1.Columns[2].HeaderText = "エラー内容";

                            }));

                        var res = MessageBox.Show("入力ミス、またはデータの不備が[ " + DataCheckClass.ErrorList.Count.ToString() + " ]件見つかりました。詳細を表示しますか？",
                            "入力エラー確認",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question);

                        if (res != System.Windows.Forms.DialogResult.Yes) return;

                        Invoke(new Action(() =>
                            {
                                using (ImageCheckForm f = new ImageCheckForm())
                                {
                                    f.ShowDialog();
                                }
                            }));
                    }
                });
            }
        }

        /// <summary>
        /// 2度目以降のチェックを行ないます
        /// </summary>
        private void reCheck()
        {
            //現在のエラーリストをクリア
            DataCheckClass.ErrorList.Clear();

            long rid;
            int errorcode;

            //過去チェック時のエラーリストを読み込む
            using (var cmd = SQLClass.CreateCommand("SELECT receipts.rid, receipts.errorcode FROM receipts, groups WHERE groups.gid = receipts.gid AND groups.sid = :sid AND receipts.errorcode <> 0"))
            {
                cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                cmd.Parameters[0].Value = targetSID;

                using (var dr = cmd.ExecuteReader())
                {

                    while (dr.Read())
                    {
                        rid = dr.GetInt64(0);
                        errorcode = dr.GetInt32(1);
                        DataCheckClass.ErrorList.Add(new DataCheckClass.ErrorData(rid, (Settings.ERROR_CODE)errorcode));
                    }
                }
            }

            //表示
            DataCheckClass.ErrorList.Sort((x, y) => x.rid == y.rid ? 0 : x.rid > y.rid ? 1 : -1);
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = DataCheckClass.ErrorList;
            dataGridView1.Columns[1].Width = 100;
            dataGridView1.Columns[2].Width = 500;
            dataGridView1.Columns[0].HeaderText = "ID";
            dataGridView1.Columns[1].HeaderText = "エラーコード";
            dataGridView1.Columns[2].HeaderText = "エラー内容";


            label5.Text = "エラー件数：  " + DataCheckClass.ErrorList.Count.ToString();

            var res = MessageBox.Show("入力ミス、またはデータの不備が[ " + DataCheckClass.ErrorList.Count.ToString() + " ]件見つかりました。詳細を表示しますか？",
                "入力エラー確認",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (res != System.Windows.Forms.DialogResult.Yes) return;

            using (ImageCheckForm f = new ImageCheckForm())
            {
                f.ShowDialog();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.CurrentCell == null) return;
            long rid = (long)dataGridView1[0, e.RowIndex].Value;

            using (ImageCheckForm f = new ImageCheckForm(rid))
            {
                f.ShowDialog();
            }

        }
    }
}
