﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class SearchForm : Form
    {
        public SearchForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int billMonth;
            int mediMonth;
            int totalPoint;
            int scanID;
            int groupID;
            long rid;

            dataGridView1.DataSource = null;
            DataTable dt = new DataTable();

            using (var cmd = SQLClass.CreateCommand("SELECT groups.sid, groups.gid, receipts.rid, groups.billmonth, receipts.medimonth, receipts.insuredno, receipts.totalpoint, receipts.outdata, receipts.errorcode FROM groups, receipts WHERE groups.gid = receipts.gid"))
            {
                //診療年月
                if (int.TryParse(textBox1.Text, out mediMonth))
                {
                    cmd.CommandText += " AND receipts.medimonth = :medimonth";
                    cmd.Parameters.Add("medimonth", NpgsqlTypes.NpgsqlDbType.Integer);
                    cmd.Parameters["medimonth"].Value = mediMonth;
                }

                //被保番
                if (textBox2.Text.Trim() != "")
                {
                    cmd.CommandText += " AND receipts.insuredno = :insuredno";
                    cmd.Parameters.Add("insuredno", NpgsqlTypes.NpgsqlDbType.Text);
                    cmd.Parameters["insuredno"].Value = textBox2.Text.Trim();
                }

                //合計額
                if (int.TryParse(textBox3.Text, out totalPoint))
                {
                    cmd.CommandText += " AND receipts.totalpoint = :totalpoint";
                    cmd.Parameters.Add("totalpoint", NpgsqlTypes.NpgsqlDbType.Integer);
                    cmd.Parameters["totalpoint"].Value = totalPoint;
                }

                //請求年月
                if (int.TryParse(textBox6.Text, out billMonth))
                {
                    cmd.CommandText += " AND groups.billmonth = :billmonth";
                    cmd.Parameters.Add("billmonth", NpgsqlTypes.NpgsqlDbType.Integer);
                    cmd.Parameters["billmonth"].Value = billMonth;
                }

                //スキャンID
                if (int.TryParse(textBox4.Text, out scanID))
                {
                    cmd.CommandText += " AND groups.sid = :sid";
                    cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer);
                    cmd.Parameters["sid"].Value = scanID;
                }

                //グループID
                if (int.TryParse(textBox5.Text, out groupID))
                {
                    cmd.CommandText += " AND groups.gid = :gid";
                    cmd.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);
                    cmd.Parameters["gid"].Value = groupID;
                }

                //電算番号
                if (textBox7.Text.Trim() != "")
                {
                    cmd.CommandText += " AND receipts.outdata = :outdata";
                    cmd.Parameters.Add("outdata", NpgsqlTypes.NpgsqlDbType.Text);
                    cmd.Parameters["outdata"].Value = textBox7.Text.Trim();
                }

                //グループID
                if (long.TryParse(textBox8.Text, out rid))
                {
                    cmd.CommandText += " AND receipts.rid = :rid";
                    cmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);
                    cmd.Parameters["rid"].Value = rid;
                }


                if (cmd.Parameters.Count == 0)
                {
                    MessageBox.Show("検索条件を指定してください");
                    return;
                }

                //内部レセプト番号順に並べる
                cmd.CommandText += " ORDER BY receipts.rid";

                using (var da = SQLClass.CreateDataAdapter(cmd))
                {
                    da.Fill(dt);
                }

                dataGridView1.DataSource = dt;

                //groups.sid, groups.gid, receipts.rid, groups.billmonth, receipts.medimonth, receipts.insuredno, receipts.totalpoint receipts.outdata
                dataGridView1.Columns[0].HeaderText = "スキャンID";
                dataGridView1.Columns[1].HeaderText = "グループID";
                dataGridView1.Columns[2].HeaderText = "レセプトID";
                dataGridView1.Columns[3].HeaderText = "請求年月";
                dataGridView1.Columns[4].HeaderText = "診療年月";
                dataGridView1.Columns[5].HeaderText = "被保番";
                dataGridView1.Columns[6].HeaderText = "合計額";
                dataGridView1.Columns[7].HeaderText = "対象電算番号";
                dataGridView1.Columns[8].HeaderText = "エラーコード";

                dataGridView1.Columns[0].Width = 80;
                dataGridView1.Columns[1].Width = 80;
                dataGridView1.Columns[2].Width = 80;
                dataGridView1.Columns[3].Width = 80;
                dataGridView1.Columns[4].Width = 80;
                dataGridView1.Columns[5].Width = 80;
                dataGridView1.Columns[6].Width = 80;
                dataGridView1.Columns[7].Width = 120;
                dataGridView1.Columns[8].Width = 80;

                label8.Text = "件数: " + dt.Rows.Count;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0)
            {
                MessageBox.Show("対象データがありません。");
                return;
            }

            if (dataGridView1.RowCount > 500)
            {
                MessageBox.Show("対象データが500件以上あります。さらに絞り込んでください。");
                return;
            }

            DataCheckClass.ErrorList.Clear();
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                var er = new DataCheckClass.ErrorData((long)dataGridView1[2, i].Value, (Settings.ERROR_CODE)(int)dataGridView1[8, i].Value);
                DataCheckClass.ErrorList.Add(er);
            }

            using (ImageCheckForm f = new ImageCheckForm())
            {
                f.ShowDialog();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.CurrentCell == null) return;
            int cr = dataGridView1.CurrentRow.Index;

            DataCheckClass.ErrorList.Clear();

            //ダブルクリックした対象データのみ表示
            var er = new DataCheckClass.ErrorData((long)dataGridView1[2, cr].Value, (Settings.ERROR_CODE)(int)dataGridView1[8, cr].Value);
            DataCheckClass.ErrorList.Add(er);

            using (ImageCheckForm f = new ImageCheckForm())
            {
                f.ShowDialog();
            }
        }
    }
}
