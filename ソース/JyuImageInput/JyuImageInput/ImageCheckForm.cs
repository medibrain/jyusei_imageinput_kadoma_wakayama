﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JyuImageInput
{
    public partial class ImageCheckForm : Form
    {
        /// <summary>
        /// 入力に変更があったか
        /// </summary>
        bool changeFlag = false;
        ///// <summary>
        ///// 現在のレセプトが、未入力だったか
        ///// </summary>
        //bool nullReceipt;

        Npgsql.NpgsqlCommand upCmd;
        Npgsql.NpgsqlCommand seCmd;

        int checkIndex = 0;
        long nowReceiptID;
        Settings.ERROR_CODE nowErrorCode;
        Settings.ERROR_CODE oldErrorCode;
        bool readOnlyReceipt = false;
        string nowImageName = "def.gif";

        public ImageCheckForm()
        {
            InitializeComponent();
            label7.Text = "";
            createCheckCmd();
        }
       

        private void createCheckCmd()
        {
            upCmd = SQLClass.CreateCommand("UPDATE receipts SET medimonth = :medimonth, insuredno = :insuredno, totalpoint = :totalpoint, outdata = :outdata, errorcode = :errorcode WHERE rid = :rid");
            upCmd.Parameters.Add("medimonth", NpgsqlTypes.NpgsqlDbType.Integer);
            upCmd.Parameters.Add("insuredno", NpgsqlTypes.NpgsqlDbType.Text);
            upCmd.Parameters.Add("totalpoint", NpgsqlTypes.NpgsqlDbType.Integer);
            upCmd.Parameters.Add("outdata", NpgsqlTypes.NpgsqlDbType.Text);
            upCmd.Parameters.Add("errorcode", NpgsqlTypes.NpgsqlDbType.Integer);
            upCmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);

            seCmd = SQLClass.CreateCommand("SELECT receipts.medimonth, receipts.insuredno, receipts.totalpoint, receipts.outdata, receipts.errorcode, receipts.gid, groups.sid FROM receipts, groups WHERE groups.gid = receipts.gid AND rid = :rid");
            seCmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);
        }


        public ImageCheckForm(long rid)
        {
            InitializeComponent();

            label7.Text = "";
            createCheckCmd();
            
            //対象ridを探し出す
            checkIndex = DataCheckClass.ErrorList.FindIndex(item => item.rid == rid);
            receptChenge(DataCheckClass.ErrorList[checkIndex]);
        }


        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                bool forward = e.Modifiers != Keys.Shift;
                //this.ProcessTabKey(forward);
                this.SelectNextControl(this.ActiveControl, forward, true, true, true);
                e.Handled = true;
            }
        }

        private void btnAfterCheck_Click(object sender, EventArgs e)
        {
            textBoxOutdata.ReadOnly = true;
            if (DataCheckClass.ErrorList.Count == 0) return;

            //現在の情報を書き込む
            if (changeFlag)
            {
                if (!receiptWrite()) return;
            }

            //インデックスを1つ進める
            if (checkIndex < DataCheckClass.ErrorList.Count - 1) checkIndex++;
            long rid = DataCheckClass.ErrorList[checkIndex].rid;

            //画像表示
            receptChenge(DataCheckClass.ErrorList[checkIndex]);

            textBoxMediMonth.Focus();
        }

        private void btnBeforeCheck_Click(object sender, EventArgs e)
        {
            textBoxOutdata.ReadOnly = true;
            if (DataCheckClass.ErrorList.Count == 0) return;

            //現在の情報を書き込む
            if (changeFlag)
            {
                if (!receiptWrite()) return;
            }

            //インデックスを1つ進める
            if (checkIndex != 0) checkIndex--;
            long rid = DataCheckClass.ErrorList[checkIndex].rid;

            //画像表示
            receptChenge(DataCheckClass.ErrorList[checkIndex]);

            textBoxMediMonth.Focus();
        }

        //private List<string> loadAndOcr(String filename)
        //{
        //    List<string> lst = new List<string>();
        //    pictureBox1.Image = Image.FromFile(filename);
        //    try
        //    {
        //        MODI.Document doc = new MODI.Document();
        //        doc.Create(filename);
        //        doc.OCR(MODI.MiLANGUAGES.miLANG_ENGLISH, false, false);

        //        foreach (MODI.Image img in doc.Images)
        //        {
        //            MODI.Layout layout = img.Layout;
        //            lst.Add(layout.Text);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.Error.WriteLine(ex.Message);
        //    }
        //    return lst;
        //}

        private void panel2_SizeChanged(object sender, EventArgs e)
        {
            ImageReset();
        }

        /// <summary>
        /// 表示モード
        /// </summary>
        private enum ImageMode { WidthHeightBase, OriginalBase, PanelBase, FreeRatio };
        private ImageMode imageMode = ImageMode.PanelBase;

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ImageShowSettingChange();
            ImageReset();
        }

        private void ImageReset()        {
            if (imageMode == ImageMode.WidthHeightBase)
            {
                //幅いっぱい、もしくは高さいっぱいにする
                float wf = (float)(panel2.Width - SystemInformation.HorizontalScrollBarHeight) / (float)pictureBox1.Image.Width;
                float wh = (float)(panel2.Height - SystemInformation.VerticalScrollBarWidth) / (float)pictureBox1.Image.Height;

                float ratio;
                ratio = wf < wh ? wh : wf;

                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox1.Dock = DockStyle.None;
                pictureBox1.Height = (int)((float)pictureBox1.Image.Height * ratio);
                pictureBox1.Width = (int)((float)pictureBox1.Image.Width * ratio);
            }
            else if (imageMode == ImageMode.PanelBase)
            {
                //全面表示にする
                pictureBox1.Dock = DockStyle.None;
                pictureBox1.Location = new Point(0, 0);
                pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;

                imageMode = ImageMode.PanelBase;
            }
            else if (imageMode == ImageMode.OriginalBase)
            {
                //等倍表示にする
                pictureBox1.Dock = DockStyle.Fill;
                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            }
            else
            {
                //任意の倍率

            }
        }

        private void ImageShowSettingChange()
        {
            if (imageMode == ImageMode.OriginalBase)
            {
                //幅いっぱい、もしくは高さいっぱいにする
                imageMode = ImageMode.WidthHeightBase;
            }
            else if (imageMode == ImageMode.WidthHeightBase)
            {
                //全面表示にする
                imageMode = ImageMode.PanelBase;
            }
            else
            {
                //等倍表示にする
                imageMode = ImageMode.OriginalBase;
            }
        }

        private bool inputEnabled { get; set; }

        private void inputEnabledTrue()
        {
            inputEnabled = true;
            textBoxInsuredNo.ReadOnly = false;
            textBoxMediMonth.ReadOnly = false;
            textBoxTotalCost.ReadOnly = false;

        }
        private void inputEnabledFalse()
        {
            inputEnabled = false;
            textBoxInsuredNo.ReadOnly = true;
            textBoxMediMonth.ReadOnly = true;
            textBoxTotalCost.ReadOnly = true;

        }

        private void inputTextBox_TextChanged(object sender, EventArgs e)
        {
            changeFlag = true;
        }

        /// <summary>
        /// 現在のリストに関係なく、次、または前のレセプトを表示します
        /// </summary>
        /// <param name="receiptID"></param>
        private void receptChangeNull(long nowReceiptID, bool nextReceipt)
        {
            textBoxOutdata.ReadOnly = true;
            long receiptID = 0;
            int sid = 0;
            int gid = 0;

            for (int i = 1; i < 6; i++)
            {
                if (i == 5)
                {
                    MessageBox.Show("次または前のレセプトはありません");
                    changeFlag = false;
                    return;
                }

                //idを5づつさかのぼって、画像データがあるか確認
                if (nextReceipt)
                {
                    receiptID = nowReceiptID + i;
                }
                else
                {
                    receiptID = nowReceiptID - i;
                }

                seCmd.Parameters[0].Value = receiptID;
                using (var dr = seCmd.ExecuteReader())
                {
                    //データがなければ、次のidを探しに行く
                    if (!dr.HasRows) continue;

                    textBoxInsuredNo.ReadOnly = true;
                    textBoxMediMonth.ReadOnly = true;
                    textBoxTotalCost.ReadOnly = true;
                    groupBox2.Enabled = false;
                    groupBox1.Enabled = false;

                    dr.Read();
                    object[] obj = new object[dr.FieldCount];
                    dr.GetValues(obj);
                    textBoxMediMonth.Text = (int)dr[0] == 0 ? "" : dr[0].ToString();
                    textBoxInsuredNo.Text = dr[1].ToString();
                    textBoxTotalCost.Text = (int)dr[2] == 0 ? "" : dr[2].ToString();
                    textBoxOutdata.Text = (string)dr[3];
                    gid = (int)dr[5];
                    sid = (int)dr[6];


                    break;
                }
            }

            //画像表示
            //20190614135727 furukawa st ////////////////////////
            //billstrの桁数を変更（令和1年は前ゼロなくなるので）
            
            string billstr = (receiptID / 1000000).ToString();
                        //string billstr = (receiptID / 1000000).ToString("0000");
            //20190614135727 furukawa ed ////////////////////////

            string floderNo = ((receiptID / 10000) % 100).ToString("00");
            string ridStr = (receiptID % 1000000).ToString("000000");

            //続紙等の場合 code99
            if (textBoxMediMonth.Text == "99") textBoxMediMonth.Text = "---";

            //不明、エラー等 code999
            if (textBoxMediMonth.Text == "999") textBoxMediMonth.Text = "*****";

            string imgstr = Settings.imagePath + "\\" + billstr + floderNo + "\\" + receiptID.ToString();

            //画像があるか確認 gif jpg 両対応
            if (System.IO.File.Exists(imgstr + ".gif")) imgstr += ".gif";
            else if (System.IO.File.Exists(imgstr + ".jpg")) imgstr += ".jpg";
            else imgstr = "def.gif";

            if (pictureBox1.Image != null)
            {
                var oldimg = pictureBox1.Image;
                pictureBox1.Image = null;
                oldimg.Dispose();
            }
            pictureBox1.Image = Image.FromFile(imgstr);

            this.nowReceiptID = receiptID;
            textBoxReceiptID.Text = receiptID.ToString();
            changeFlag = false;

            label6.Text = (checkIndex + 1).ToString() + " / " + DataCheckClass.ErrorList.Count.ToString();

            readOnlyReceipt = true;
        }


        /// <summary>
        /// 表示レセプトの変更
        /// </summary>
        /// <param name="receiptID"></param>
        private void receptChenge(DataCheckClass.ErrorData ed)
        {
            textBoxOutdata.ReadOnly = true;
            if (DataCheckClass.ErrorList.Count == 0) return;

            textBoxInsuredNo.ReadOnly = false;
            textBoxMediMonth.ReadOnly = false;
            textBoxTotalCost.ReadOnly = false;
            groupBox2.Enabled = true;
            groupBox1.Enabled = true;

            int gid = 0;
            int sid = 0;
            nowErrorCode = Settings.ERROR_CODE.エラーなし;
            oldErrorCode = Settings.ERROR_CODE.エラーなし;

            numericUpDown1.Value = 220;

            seCmd.Parameters[0].Value = ed.rid;
            using (var dr = seCmd.ExecuteReader())
            {
                dr.Read();
                object[] obj = new object[dr.FieldCount];
                dr.GetValues(obj);
                textBoxMediMonth.Text = (int)dr[0] == 0 ? "" : dr[0].ToString();
                textBoxInsuredNo.Text = dr[1].ToString();
                textBoxTotalCost.Text = (int)dr[2] == 0 ? "" : dr[2].ToString();
                textBoxOutdata.Text = (string)dr[3];
                nowErrorCode = (Settings.ERROR_CODE)(int)dr[4];
                oldErrorCode = nowErrorCode;
                gid = (int)dr[5];
                sid = (int)dr[6];
            }


            //続紙等の場合 code99
            if (textBoxMediMonth.Text == "99") textBoxMediMonth.Text = "---";

            //不明、エラー等 code999
            if (textBoxMediMonth.Text == "999") textBoxMediMonth.Text = "*****";

            //エラーコードに応じてラジオボタン調整
            errorRadioButtonSet(nowErrorCode);

            //画像表示
            
            //20190614135511 furukawa st ////////////////////////
            //billstrの桁数を変更（令和1年は前ゼロなくなるので）            
            string billstr = (ed.rid / 1000000).ToString();
                    //string billstr = (ed.rid / 1000000).ToString("0000");
            //20190614135511 furukawa ed ////////////////////////

            string floderNo = ((ed.rid / 10000) % 100).ToString("00");
            string ridStr = (ed.rid % 1000000).ToString("000000");

            nowImageName = Settings.imagePath + "\\" + billstr + floderNo + "\\" + ed.rid.ToString();

            //画像があるか確認 gif jpg 両対応
            if (System.IO.File.Exists(nowImageName + ".gif")) nowImageName += ".gif";
            else if (System.IO.File.Exists(nowImageName + ".jpg")) nowImageName += ".jpg";
            else nowImageName = "def.gif";

            if (pictureBox1.Image != null)
            {
                var oldimg = pictureBox1.Image;
                pictureBox1.Image = null;
                oldimg.Dispose();
            }
            pictureBox1.Image = Image.FromFile(nowImageName);

            nowReceiptID = ed.rid;
            textBoxReceiptID.Text = ed.rid.ToString();
            changeFlag = false;

            label6.Text = (checkIndex + 1).ToString() + " / " + DataCheckClass.ErrorList.Count.ToString();
            //label7.Text = ed.errorText;

            readOnlyReceipt = false;

            //重複情報の表示
            dataGridView2.DataSource = null;
            if (textBoxOutdata.Text != "error")overlaped(textBoxOutdata.Text);

            //todo:スキャンID　グループIDの表示
            //label8.Text = "スキャンID:" + sid.ToString() + "    グループID" + gid.ToString();
            textBoxScanID.Text = sid.ToString();
            textBoxGroupID.Text = gid.ToString();

            checkDataShow();
        }

        private bool receiptInputDataCheck()
        {
            string mediMonthStr = textBoxMediMonth.Text.Trim();
            string insuredNoStr = textBoxInsuredNo.Text.Trim();
            string totalPointStr = textBoxTotalCost.Text.Trim();
            int mediMonth = 0;
            int totalPoint = 0;
            bool errorFlag = false;
            string errorText = "";

            //値の検証
            //20190614141550 furukawa st ////////////////////////
            //診療月長さチェック変更（令和1年は前ゼロなくなるので）

            if (mediMonthStr.Length <= 2 || mediMonthStr.Length >= 5 || !int.TryParse(mediMonthStr, out mediMonth) || (mediMonth % 100) > 12)
                //if (mediMonthStr.Length != 4 || !int.TryParse(mediMonthStr, out mediMonth) || (mediMonth % 100) > 12)
            //20190614141550 furukawa ed ////////////////////////
            {
                errorFlag = true;
                errorText += "診療月 ";
            }
            if (insuredNoStr.Length != 8)
            {
                errorFlag = true;
                errorText += "被保番 ";
            }
            if (totalPointStr.Length < 2 || !int.TryParse(totalPointStr, out totalPoint))
            {
                errorFlag = true;
                errorText += "合計金額 ";
            }
            if (mediMonthStr == "---")
            {
                mediMonth = 99;
                errorFlag = false;
            }
            if (mediMonthStr == "*****")
            {
                mediMonth = 999;
                errorFlag = false;
            }

            if (errorFlag) return false;

            return true;
        }

        /// <summary>
        /// 現在表示されているレセプトを書き込みます
        /// </summary>
        /// <returns>別レセプトへ移動していいか</returns>
        private bool receiptWrite()
        {
            if (DataCheckClass.ErrorList.Count == 0) return false;
            if (readOnlyReceipt) return false;

            string mediMonthStr = textBoxMediMonth.Text.Trim();
            string insuredNoStr = textBoxInsuredNo.Text.Trim();
            string totalPointStr = textBoxTotalCost.Text.Trim();
            int mediMonth = 0;
            int totalPoint = 0;
            bool errorFlag = false;
            string errorText = "";

            //値の検証
            //20190614141440 furukawa st ////////////////////////
            //診療月長さチェック変更（令和1年は前ゼロなくなるので）
            
            if (mediMonthStr.Length <= 2 || mediMonthStr.Length >= 5 || !int.TryParse(mediMonthStr, out mediMonth) || (mediMonth % 100) > 12)
                    //if (mediMonthStr.Length != 4 || !int.TryParse(mediMonthStr, out mediMonth) || (mediMonth % 100) > 12)
            //20190614141440 furukawa ed ////////////////////////
            {
                errorFlag = true;
                errorText += "診療月 ";
            }

#if KADOMA_CITY // ★ 門真市役所向けのCSV取込 ★
            if (insuredNoStr.Length != Settings.numberLength)  // 門真市は、設定ファイルで被保険者番号の文字列長を決める！
#else
            if (insuredNoStr.Length != 8)
#endif
            {
                errorFlag = true;
                errorText += "被保番 ";
            }
            if (totalPointStr.Length < 2 || !int.TryParse(totalPointStr, out totalPoint))
            {
                errorFlag = true;
                errorText += "合計金額 ";
            }
            if (mediMonthStr == "---")
            {
                mediMonth = 99;
                errorFlag = false;
            }
            if (mediMonthStr == "*****")
            {
                mediMonth = 999;
                errorFlag = false;
            }

            if (errorFlag)
            {
                var res = MessageBox.Show(errorText + "のデータに誤りがあります。データは記録されませんが、よろしいですか？",
                    "入力エラー", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (res == System.Windows.Forms.DialogResult.No)
                {
                    textBoxMediMonth.Focus();
                    return false;
                }
                else
                {
                    textBoxMediMonth.Focus();
                    return true;
                }
            }

            upCmd.Parameters[0].Value = mediMonth;
            upCmd.Parameters[1].Value = insuredNoStr;
            upCmd.Parameters[2].Value = totalPoint;
            upCmd.Parameters[3].Value = textBoxOutdata.Text;
            upCmd.Parameters[4].Value = (int)nowErrorCode;
            upCmd.Parameters[5].Value = nowReceiptID;

            upCmd.ExecuteNonQuery();

            textBoxMediMonth.Focus();
            return true;

        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBoxOutdata.ReadOnly = true;
            if (DataCheckClass.ErrorList.Count == 0) return;

            //現在の情報を書き込む
            if (changeFlag)
            {
                if (!receiptWrite()) return;
            }

            //インデックス調整
            if (checkIndex - 10 > 0)
            {
                checkIndex -= 10;
            }
            else
            {
                checkIndex = 0;
            }
            long rid = DataCheckClass.ErrorList[checkIndex].rid;

            //画像表示
            receptChenge(DataCheckClass.ErrorList[checkIndex]);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBoxOutdata.ReadOnly = true;
            if (DataCheckClass.ErrorList.Count == 0) return;

            //現在の情報を書き込む
            if (changeFlag)
            {
                if (!receiptWrite()) return;
            }

            //インデックス調整
            if (checkIndex + 10 < DataCheckClass.ErrorList.Count)
            {
                checkIndex += 10;
            }
            else
            {
                checkIndex = DataCheckClass.ErrorList.Count - 1;
            }
            long rid = DataCheckClass.ErrorList[checkIndex].rid;

            //画像表示
            receptChenge(DataCheckClass.ErrorList[checkIndex]);
        }

        private void inputTextBox_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBoxOutdata.ReadOnly = true;
            receptChangeNull(nowReceiptID, false);
        }

        private void btnAfterReze_Click(object sender, EventArgs e)
        {
            textBoxOutdata.ReadOnly = true;
            receptChangeNull(nowReceiptID, true);
        }

        private void ImageCheckForm_Shown(object sender, EventArgs e)
        {
            if (DataCheckClass.ErrorList.Count < 1)
            {
                MessageBox.Show("チェック対象レセプトが存在しません。チェックを終了します。");
                this.Close();
                return;
            }

            receptChenge(DataCheckClass.ErrorList[checkIndex]);
            label6.Text = (checkIndex + 1).ToString() + " / " + DataCheckClass.ErrorList.Count.ToString();
        }

        /// <summary>
        /// エラーコードに応じて、表示を調整します
        /// </summary>
        /// <param name="errorCode"></param>
        private void errorRadioButtonSet(Settings.ERROR_CODE errorCode)
        {
            if (errorCode == Settings.ERROR_CODE.エラーなし)
            {
                radioButton4.Checked = true;
                label7.Text = "エラーなし";
            }

            int reasonCode = (int)errorCode / 10;
            int stateCode = (int)errorCode % 10;

            //状態コード別にラジオボタンを設定
            if (stateCode == 1) radioButton1.Checked = true;
            else if (stateCode == 2) radioButton2.Checked = true;
            else if (stateCode == 3) radioButton3.Checked = true;
        }

        private void errorRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!((RadioButton)sender).Checked) return;

            int reasonCode = (int)oldErrorCode / 10;
            int stateCode = 0;

            if (reasonCode == 5)
            {
                //画像重複データの場合
                List<Settings.ERROR_CODE> overErrorList = new List<Settings.ERROR_CODE>();
                List<long> overRidList = new List<long>();

                DataTable dt = new DataTable();
                dataGridView2.DataSource = null;
                using (var cmd = SQLClass.CreateCommand("SELECT rid, errorcode FROM receipts WHERE outdata = :outdata"))
                {
                    cmd.Parameters.Add("outdata", NpgsqlTypes.NpgsqlDbType.Text);
                    cmd.Parameters[0].Value = textBoxOutdata.Text;
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            overRidList.Add(dr.GetInt64(0));
                            if (dr.GetInt64(0) == nowReceiptID)
                            {
                                if (radioButton1.Checked) stateCode = 1;
                                else if (radioButton2.Checked) stateCode = 2;
                                else if (radioButton3.Checked) stateCode = 3;
                                int tempCode = reasonCode * 10 + stateCode;

                                if (radioButton4.Checked) tempCode = 0;
                                overErrorList.Add((Settings.ERROR_CODE)tempCode);
                            }
                            else
                            {
                                overErrorList.Add((Settings.ERROR_CODE)dr.GetInt32(1));
                            }
                        }

                        //複数のエラーなしを指定した場合
                        if (overErrorList.Count(item => item == Settings.ERROR_CODE.エラーなし) > 1)
                        {
                            MessageBox.Show("複数のレセプトが関連づけられようとしています。設定をやり直してください。");

                            StringBuilder oversb = new StringBuilder();
                            for (int i = 0; i < overRidList.Count; i++)
                            {
                                if (i != 0) oversb.Append(", ");
                                oversb.Append(overRidList[i].ToString());
                            }

                            using (var replaceCmd = SQLClass.CreateCommand("UPDATE receipts SET errorcode = 51 WHERE errorcode = 0 AND rid IN (" + oversb.ToString() + ")"))
                            {
                                replaceCmd.ExecuteScalar();
                            }

                            radioButton1.Checked = true;
                            return;
                        }

                        //todo:すべて無視・保留にした場合


                    }
                }

                overlaped(textBoxOutdata.Text);
            }

            //エラーなしに設定された場合
            if (radioButton4.Checked)
            {
                if (textBoxOutdata.Text == "error")
                {
                    MessageBox.Show("エラーが解決されていないか、電算管理番号が指定されていません。確認してください");
                    stateCode = 1;
                }
                else
                {
                    changeFlag = true;
                }
            }
            //その他の場合
            else
            {
                if (radioButton1.Checked) stateCode = 1;
                else if (radioButton2.Checked) stateCode = 2;
                else if (radioButton3.Checked) stateCode = 3;
                changeFlag = true;
            }

            nowErrorCode = (Settings.ERROR_CODE)(reasonCode * 10 + stateCode);

            if (stateCode == 0) nowErrorCode = 0;

            errorRadioButtonSet(nowErrorCode);
            label7.Text = Settings.errorMessage[(int)nowErrorCode];
        }



        private void このレセプトに関連付けるToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell == null) return;
            string selectID = (string)dataGridView1[0, dataGridView1.CurrentRow.Index].Value;
            string insuredno = (string)dataGridView1[3, dataGridView1.CurrentRow.Index].Value;
            int medimonth = (int)dataGridView1[5, dataGridView1.CurrentRow.Index].Value;
            int totalcost = (int)dataGridView1[8, dataGridView1.CurrentRow.Index].Value;

            textBoxInsuredNo.Text = insuredno;
            textBoxMediMonth.Text = DateTimeEx.YearMonthADtoJ_H(medimonth).ToString();
            textBoxTotalCost.Text = totalcost.ToString();
            textBoxOutdata.Text = selectID;
            radioButton4.Checked = true;
        }

        /// <summary> 回転ボタン </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (nowImageName == "def.gif") return;

            var img = (Image)pictureBox1.Image.Clone();
            var oldimg = pictureBox1.Image;
            pictureBox1.Image = null;
            oldimg.Dispose();

            img.RotateFlip(RotateFlipType.Rotate90FlipNone);
#if NOT_GIFCONVERT  // GIF変換をやめる
            img.Save(nowImageName, img.RawFormat);
#else
            img.Save(nowImageName, System.Drawing.Imaging.ImageFormat.Gif);
#endif

            pictureBox1.Image = img;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (nowImageName == "def.gif") return;

            string fileName;
            using (OpenFileDialog f = new OpenFileDialog())
            {
                f.Filter = "画像ファイル(*.jpg;*.gif;*.tif)|*.jpg;*.jpeg;*.gif;*.tif;*.tiff";
                var res = f.ShowDialog();
                if (res != System.Windows.Forms.DialogResult.OK) return;
                fileName = f.FileName;
            }

            var img = Image.FromFile(fileName);
            if (img.PixelFormat != System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
            {
                MessageBox.Show("グレースケールの画像ではありません。スキャンしなおす必要があります");
                return;
            }

            var res2 = MessageBox.Show("もとには戻せません　本当に画像を差し替えますか？", "差し替え確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res2 != System.Windows.Forms.DialogResult.Yes) return;

            using (var oldimg = pictureBox1.Image)
            {
                pictureBox1.Image = null;
                oldimg.Dispose();
            }

            //念のためリネームしてバックアップを取る
            int oldFileCount = 1;
            while (System.IO.File.Exists(nowImageName + ".old" + oldFileCount.ToString())) oldFileCount++;
            System.IO.File.Copy(nowImageName, nowImageName + ".old" + oldFileCount.ToString());

            Settings.ImageChange(fileName, nowImageName);
            pictureBox1.Image = Image.FromFile(nowImageName);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            int maxi = (int)numericUpDown1.Value;
            Bitmap bmp = (Bitmap)Image.FromFile(nowImageName);

            var img = pictureBox1.Image;
            pictureBox1.Image = null;
            img.Dispose();

            Settings.ImageBrightness(bmp, maxi);
            pictureBox1.Image = bmp;
        }

        private void button11_Click(object sender, EventArgs e)
        {

#if !NOT_GIFCONVERT  // GIF変換をやめるモードの時は、変更しない
            var res = MessageBox.Show("確定すると元には戻せません。よろしいですか？",
                "画像明るさ変更確認",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Asterisk);
            if (res != System.Windows.Forms.DialogResult.Yes) return;

            //念のためリネームしてバックアップを取る
            int oldFileCount = 1;
            while (System.IO.File.Exists(nowImageName + ".old" + oldFileCount.ToString())) oldFileCount++;
            System.IO.File.Copy(nowImageName, nowImageName + ".old" + oldFileCount.ToString());

            //ファイルロックを外して保存
            var bmp = new Bitmap(pictureBox1.Image);
            pictureBox1.Image.Dispose();
            pictureBox1.Image = bmp;
                        
            bmp.Save(nowImageName, System.Drawing.Imaging.ImageFormat.Gif);
#endif
        }


        private void ImageCheckForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            receiptWrite();
        }

        private void dataSelecttRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            checkDataShow();
        }

        private void checkDataShow()
        {
            dataGridView1.DataSource = null;
            DataTable dt = new DataTable();
            string selectStr;
            using (var cmd = SQLClass.CreateCommand(""))
            {

                if (radioButton5.Checked)
                {
                    selectStr = "SELECT id, sex, birth, insuredno, hoscode," +
                        " medimonth, billmonth, typecode, totalcost, statecode FROM tdata WHERE medimonth =:medimonth AND totalcost = :totalcost";
                    cmd.CommandText = selectStr;
                    cmd.Parameters.Add("medimonth", NpgsqlTypes.NpgsqlDbType.Integer);
                    cmd.Parameters.Add("totalcost", NpgsqlTypes.NpgsqlDbType.Integer);

                    int mm = 0;
                    int tc = 0;
                    int.TryParse(textBoxMediMonth.Text, out mm);
                    int.TryParse(textBoxTotalCost.Text, out tc);

                    mm = DateTimeEx.YearMonthJtoAD_H(mm);

                    cmd.Parameters[0].Value = mm;
                    cmd.Parameters[1].Value = tc;

                }
                else
                {
                    selectStr = "SELECT id, sex, birth, insuredno, hoscode, medimonth, billmonth, typecode, totalcost, statecode FROM tdata WHERE medimonth =:medimonth AND insuredno = :insuredno";
                    cmd.CommandText = selectStr;
                    cmd.Parameters.Add("medimonth", NpgsqlTypes.NpgsqlDbType.Integer);
                    cmd.Parameters.Add("insuredno", NpgsqlTypes.NpgsqlDbType.Text);

                    int mm = 0;
                    int.TryParse(textBoxMediMonth.Text, out mm);
                    mm = DateTimeEx.YearMonthJtoAD_H(mm);

                    cmd.Parameters[0].Value = mm;
                    cmd.Parameters[1].Value = textBoxInsuredNo.Text.Trim();
                }

                using (var da = SQLClass.CreateDataAdapter(cmd))
                {
                    da.Fill(dt);
                }

                dataGridView1.DataSource = dt;

                dataGridView1.Columns[0].HeaderText = "ID";
                dataGridView1.Columns[1].HeaderText = "性別";
                dataGridView1.Columns[2].HeaderText = "生年月日";
                dataGridView1.Columns[3].HeaderText = "被保番";
                dataGridView1.Columns[4].HeaderText = "施術院コード";
                dataGridView1.Columns[5].HeaderText = "診療月";
                dataGridView1.Columns[6].HeaderText = "請求月";
                dataGridView1.Columns[7].HeaderText = "レセタイプ";
                dataGridView1.Columns[8].HeaderText = "合計額";
                dataGridView1.Columns[9].HeaderText = "状態";

                dataGridView1.Columns[0].Width = 200;
                dataGridView1.Columns[1].Width = 60;
                dataGridView1.Columns[2].Width = 100;
                dataGridView1.Columns[3].Width = 120;
                dataGridView1.Columns[4].Width = 120;
                dataGridView1.Columns[5].Width = 80;
                dataGridView1.Columns[6].Width = 80;
                dataGridView1.Columns[7].Width = 80;
                dataGridView1.Columns[8].Width = 80;
                dataGridView1.Columns[9].Width = 80;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            checkDataShow();
        }

        /// <summary>
        /// 同一電算管理番号が割り当てられているもの一覧を表示します
        /// </summary>
        /// <param name="outdata"></param>
        private void overlaped(string outdata)
        {
            if (outdata == "error") return;
            DataTable dt = new DataTable();
            dataGridView2.DataSource = null;
            using (var cmd = SQLClass.CreateCommand("SELECT rid, errorcode FROM receipts WHERE outdata = :outdata"))
            {
                cmd.Parameters.Add("outdata", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters[0].Value = outdata;
                using (var da = SQLClass.CreateDataAdapter(cmd))
                {
                    da.Fill(dt);
                }
            }
            dt.Columns.Add();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i][2] = ((Settings.ERROR_CODE)(int)dt.Rows[i][1]).ToString();
                if ((long)dt.Rows[i][0] == nowReceiptID) dt.Rows[i][2] = nowErrorCode.ToString();
            }

            dataGridView2.DataSource = dt;
            dataGridView2.Columns[1].Visible = false;
            dataGridView2.Columns[0].Width = 80;
            



            //現在のレセプトを選択しておく
            for (int i = 0; i < dataGridView2.RowCount; i++)
            {
                if ((long)dataGridView2[0, i].Value == nowReceiptID)
                {
                    dataGridView2.CurrentCell = dataGridView2[0, i];
                    break;
                }
            }
        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dataGridView2.CurrentCell==null)return;

            //現在の情報を書き込む
            if (changeFlag)
            {
                if (!receiptWrite()) return;
            }
            
            long rid = (long)dataGridView2[0, dataGridView2.CurrentRow.Index].Value;

            var ed = DataCheckClass.ErrorList.First(item => item.rid == rid);
            receptChenge(ed);
        }

        private void 直前レセプトの続紙にするToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show("続紙に設定しますか？", "",
                 MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res != System.Windows.Forms.DialogResult.Yes) return;

            textBoxOutdata.Text = "続紙";
            changeFlag = true;
        }

        private void 直接入力ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show("通常は行わないでください。直接修正しますか？", "",
                 MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (res != System.Windows.Forms.DialogResult.Yes) return;

            res = MessageBox.Show("正確な入力を行なわない場合、エラーが複雑になる危険があります。直接修正しますか？", "再確認",
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (res != System.Windows.Forms.DialogResult.Yes) return;

            MessageBox.Show("細心の注意をはらって修正してください。");

            textBoxOutdata.ReadOnly = false;
            changeFlag = true;
        }

        /// <summary> 被保険者番号のテキストの文字列長チェック </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxInsuredNo_Validated(object sender, EventArgs e)
        {
#if KADOMA_CITY // ★ 門真市役所向けのCSV取込 ★
            // 文字列長を固定にする （最大文字列数に満たない時は、ゼロ埋めする。）
            if (textBoxInsuredNo.Text.Length < Settings.numberLength) textBoxInsuredNo.Text = textBoxInsuredNo.Text.PadLeft(Settings.numberLength, '0');
#endif
        }

        /// <summary> ダイアログ起動時の処理 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImageCheckForm_Load(object sender, EventArgs e)
        {
#if KADOMA_CITY // ★ 門真市役所向けのCSV取込 ★
            // 文字列長を固定にする (最大文字列数を設定する)
            textBoxInsuredNo.MaxLength = Settings.numberLength;
#endif
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                // 被保番
                e.CellStyle.BackColor = (e.Value.ToString().Trim() == textBoxInsuredNo.Text.Trim()) ? Color.White : Color.Pink;
            }
            //else if (e.ColumnIndex == 5)
            //{
            //    // 診療月
            //    e.CellStyle.BackColor = (e.Value.ToString().Substring(4) == textBoxMediMonth.Text.Trim()) ? Color.White : Color.Pink;
            //}
            else if(e.ColumnIndex == 8)
            {
                // 合計額
                e.CellStyle.BackColor = (e.Value.ToString().Trim() == textBoxTotalCost.Text.Trim()) ? Color.White : Color.Pink;
            }
        }
    }
}
