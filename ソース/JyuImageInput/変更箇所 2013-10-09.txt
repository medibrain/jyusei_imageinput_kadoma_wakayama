柔整入力システム 変更箇所

2013-10-09 (水)

門真市の対応


１. プロジェクトの設定の変更

    ・ VisualStudioの の メニュー - ビルド - 構成マネージャー で、
       Release_KADOMA、 Debug_Kadoma を追加
       
    ・ プロジェクトのビルド設定の変更
       JyuImageInput の ビルドの設定で、 条件付きコンパイルシンボルを設定
       
         Release         Debug         ->  WAKA_KOIKI     （和歌山広域用）
         Release_KADOMA  Debug_Kadoma  ->  KADOMA_CITY    （門真市役所用）
    

２. プログラムの変更
    ・MainForm.cs
       MainForm() コンストラクタ ：  タイトルを変更 （和歌山広域連合用 or 門真市役所用) と表示
       
    ・DataCheckClass.cs
       CSVReader の変更 （ ★ 門真市役所向けのCSV取込 ★ を追加）
       
以上

